<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/ingresarPinConsultarController.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Consultar Pin</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <div>
                    <img src="view/images/logo.jpg" alt="" style="width: 15%; height: 15%"/>
                    <h4 align="center">Ingresar Pin</h4>
                </div>
                <div>
                    <label>Ingrese su Pin:</label>
                    <input placeholder="Ingrese el Pin" name="pin" id="pin" type="text" autocomplete="off" maxlength="15" required/>
                </div>

                <!----------------- PANEL DE BOTONES ----------------->
                <div align="center">
                    <br/>
                    <input name="register" id="submit" type="submit" value="Consultar Formulario" class="Button2"/>
                    <!--input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2"-->
                </div>
            </form>
        </div>
    </body>
</html>