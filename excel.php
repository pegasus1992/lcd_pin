<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';
include_once './model/Foto.php';
include_once './model/Formulario.php';
include_once './model/FormularioPadres.php';
include_once './model/FormularioAdicional.php';

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename = pines.xls");

function celdaVacia($max) {
    for ($i = 0; $i < $max; $i++) {
        echo "<td align='center'></td>\n";
    }
}
?>
<html>
    <header>
        <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Mostrar Pines</title>';
        ?>
    </header>
    <body>
        <table border='1' bordercolor='#000000' style='border-collapse:collapse;' align='center'>
            <tr>
                <th bgcolor='#A4A4A4'><font face='Arial'>Formulario</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>PIN Asociado</th>

                <!-- INFORMACION DEL EDUCANDO -->
                <th bgcolor='#A4A4A4'><font face='Arial'>Primer Apellido Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Segundo Apellido Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Nombres Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Lugar Nacimiento Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Fecha Nacimiento Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Edad Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Grupo Sanguíneo</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>RH</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>EPS</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo Doc Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Número Doc Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Lugar Expedición Documento</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Nacionalidad Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Dirección Residencia Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Barrio Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estrato</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Teléfono Educando</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Grado a Ingresar</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Grado Actual</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Conocido que estudia</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Impedimento físico</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Ayuda</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Enfermedad</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Medicamento Especial</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo de Ruta</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Dirección de Ruta</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Barrio de Ruta</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Restaurante Escolar</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Mano Dominante</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Deficiencia visual</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Fracturas</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Aspectos Especiales</th>

                <!-- INFORMACION DE LA MADRE -->
                <th bgcolor='#A4A4A4'><font face='Arial'>Primer Apellido Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Segundo Apellido Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Nombres Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Lugar Nacimiento Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Fecha Nacimiento Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Edad Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo Doc Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Número Doc Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Profesión Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Empresa Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Celular Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Teléfono Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Extensión Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Cargo Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Ingresos Mensuales Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Trabajo Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Primarios Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Secundarios Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Universitarios Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Otros Estudios Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Madre reside en</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo Residencia Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Correo Madre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Intereses Personales Madre</th>

                <!-- INFORMACION DEL PADRE -->
                <th bgcolor='#A4A4A4'><font face='Arial'>Primer Apellido Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Segundo Apellido Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Nombres Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Lugar Nacimiento Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Fecha Nacimiento Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Edad Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo Doc Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Número Doc Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Profesión Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Empresa Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Celular Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Teléfono Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Extensión Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Cargo Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Ingresos Mensuales Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Trabajo Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Primarios Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Secundarios Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Estudios Universitarios Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Otros Estudios Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Padre reside en</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo Residencia Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Correo Padre</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Intereses Personales Padre</th>

                <!-- INFORMACION ADICIONAL -->
                <th bgcolor='#A4A4A4'><font face='Arial'>Tipo de Unión</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>El Educando vive con</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Edad al ser Adoptado</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Información de Emergencia</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Información de Contacto de Familiares</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Persona que Responde</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Quién Firma</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Miembro que Referencia</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Padres del Miembro</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Grado del Miembro</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Características</th>
                <th bgcolor='#A4A4A4'><font face='Arial'>Visión Educativa</th>
            </tr>
            <?php
            $sql = "SELECT * FROM Pin";
            $result = getResultSet($sql);
            if ($result->num_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr><font face='Arial' Size='3'>";

                    $pin = $row["anio"] . $row["numero"];
                    echo "<td align='center'>" . $row["numeroFormulario"] . "</td>\n";
                    echo "<td align='center'>'" . $pin . "</td>\n";

                    $utilizado = $row["utilizado"];
                    if ($utilizado == 'S') {
                        // Traer Formulario del Educando
                        $formulario = Formulario::getFormulario($pin);

                        // Informacion del Educando
                        if ($formulario != null) {
                            echo "<td align='center'>" . quitarTildes($formulario->getPrimerApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getSegundoApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNombres()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getLugarNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getFechaNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getEdadCumplida()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getGrupoSanguineo()) . "</td>\n";

                            $rh = $formulario->getRh();
                            if ($rh == "P") {
                                echo "<td align='center'>Positivo</td>\n";
                            } else {
                                echo "<td align='center'>Negativo</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($formulario->getEps()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getTid()->getNombre()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNid()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getLugarExpedicion()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNacionalidad()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getDireccionResidencia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getBarrio()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getEstrato()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getTelefono()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getGradoIngresar()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getGradoActual()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getConocidoEstudia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNombreImpedimentoFisico()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNombreAyuda()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNombreEnfermedad()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getNombreMedicamentoEspecial()) . "</td>\n";

                            $tipoRuta = $formulario->getTipoRuta();
                            if ($tipoRuta == "M") {
                                echo "<td align='center'>Media Ruta</td>\n";
                            } else {
                                echo "<td align='center'>Ruta Completa</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($formulario->getDireccionRuta()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getBarrioRuta()) . "</td>\n";

                            $restaurante = $formulario->getRestauranteEscolar();
                            if ($restaurante == "S") {
                                echo "<td align='center'>Sí</td>\n";
                            } else {
                                echo "<td align='center'>No</td>\n";
                            }

                            $mano = $formulario->getManoDominante();
                            if ($mano == "Z") {
                                echo "<td align='center'>Zurdo</td>\n";
                            } else if ($mano == "D") {
                                echo "<td align='center'>Diestro</td>\n";
                            } else {
                                echo "<td align='center'>Ambidiestro</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($formulario->getDeficienciaVisual()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getDetalleFractura()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formulario->getAspectosEspeciales()) . "</td>\n";
                        } else {
                            celdaVacia(32);
                        }

                        // Traer Formulario de los Padres
                        $formularioPadres = FormularioPadres::obtenerFormularioPadres($pin);
                        if ($formularioPadres != null) {
                            $madre = $formularioPadres->getMadre();
                            $padre = $formularioPadres->getPadre();

                            // Informacion de la Madre
                            echo "<td align='center'>" . quitarTildes($madre->getPrimerApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getSegundoApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getNombres()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getLugarNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getFechaNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEdadCumplida()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getTid()->getNombre()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getNid()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getProfesion()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEmpresa()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($madre->getCelular()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($madre->getTelefono()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($madre->getExtension()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getCargo()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getIngresosMensuales()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getTrabajo()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEstudiosPrimarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEstudiosSecundarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEstudiosUniversitarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getEstudiosOtros()) . "</td>\n";

                            $resideEnMadre = $madre->getResideEn();
                            if ($resideEnMadre == "C") {
                                echo "<td align='center'>Casa</td>\n";
                            } else {
                                echo "<td align='center'>Apartamento</td>\n";
                            }

                            $tipoResidenciaMadre = $madre->getTipoResidencia();
                            if ($tipoResidenciaMadre == "P") {
                                echo "<td align='center'>Propia</td>\n";
                            } else {
                                echo "<td align='center'>Arriendo</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($madre->getEmail()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($madre->getIntereses()) . "</td>\n";

                            // Informacion del Padre
                            echo "<td align='center'>" . quitarTildes($padre->getPrimerApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getSegundoApellido()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getNombres()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getLugarNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getFechaNacimiento()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEdadCumplida()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getTid()->getNombre()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getNid()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getProfesion()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEmpresa()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($padre->getCelular()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($padre->getTelefono()) . "</td>\n";
                            echo "<td align='center'>'" . quitarTildes($padre->getExtension()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getCargo()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getIngresosMensuales()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getTrabajo()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEstudiosPrimarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEstudiosSecundarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEstudiosUniversitarios()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getEstudiosOtros()) . "</td>\n";

                            $resideEnPadre = $padre->getResideEn();
                            if ($resideEnPadre == "C") {
                                echo "<td align='center'>Casa</td>\n";
                            } else {
                                echo "<td align='center'>Apartamento</td>\n";
                            }

                            $tipoResidenciaPadre = $padre->getTipoResidencia();
                            if ($tipoResidenciaPadre == "P") {
                                echo "<td align='center'>Propia</td>\n";
                            } else {
                                echo "<td align='center'>Arriendo</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($padre->getEmail()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($padre->getIntereses()) . "</td>\n";
                        } else {
                            celdaVacia(48);
                        }

                        // Traer Formulario Adicional
                        $formularioAdicional = FormularioAdicional::obtenerFormularioAdicional($pin);

                        // Informacion Adicional
                        if ($formularioAdicional != null) {
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getTipoUnion()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getSeparados()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getEdadAdoptado()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getDireccionTelefonoEmergencia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getDireccionTelefonoFamiliar()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getPersonaResponde()) . "</td>\n";

                            $padresFirman = $formularioAdicional->getPadresFirman();
                            if ($padresFirman == 'N') {
                                echo "<td align='center'>" . quitarTildes($formularioAdicional->getNombreFirma()) . "</td>\n";
                            } else {
                                echo "<td align='center'>Padres</td>\n";
                            }

                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getMiembroReferencia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getPadresReferencia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getGradoReferencia()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getCaracteristicas()) . "</td>\n";
                            echo "<td align='center'>" . quitarTildes($formularioAdicional->getVisionEducativa()) . "</td>\n";
                        } else {
                            celdaVacia(12);
                        }
                    } else {
                        celdaVacia(92);
                    }
                    echo "</tr>";
                }
            }
            ?>
        </table>
    </body>
</html>