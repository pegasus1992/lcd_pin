<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './model/Pin.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Imprimir Pines</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <h4 align="center">Imprimir Pines</h4>
            <br/>
            <?php
            $pines = Pin::getPines();
            if ($pines != null) {
                foreach ($pines as $pin) {
                    ?>
                    <div class="texto3">
                        <br/>
                        <b>Formulario N°</b> <?php echo $pin->getNumeroFormulario() ?>
                        <br/>
                        <b>Pin asociado:</b> <?php echo $pin->getAnio() . $pin->getNumero() ?>
                        <br/>
                        <br/>
                        <b>Es requisito indispensable completar el formulario electrónico.</b>
                        <br/>
                        <br/>
                    </div>
                    <?php
                }
            }
            ?>
            <br/>
            <br/>

            <!----------------- PANEL DE BOTONES ----------------->
            <div align="center">
                <input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/>
                <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
            </div>
            <br/>
        </div>
    </body>
</html>