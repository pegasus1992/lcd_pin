<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './model/Pin.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Certificado de Finalización</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:700px" class="texto2">
                <?php
                $pin = $_GET['pin'];
                if (strlen($pin) == LONGITUD_PIN) {
                    $anio = substr($pin, 0, 4);
                    $numero = substr($pin, 4);
                    $pin = Pin::getPin($anio, $numero);
                    if ($pin != null) {
                        if ($pin->getUtilizado() == "S") {
                            ?>
                            <h4 align="center">Certificado de Finalización</h4>
                            <?php
                            $pinAnio = $pin->getAnio();
                            $pinNumero = $pin->getNumero();
                            $pinNumeroFormulario = $pin->getNumeroFormulario();
                            echo "<p>Usted ha diligenciado satisfactoriamente el formulario de admisión N° <b>$pinNumeroFormulario</b>.</p>";
                            echo "<p>El Pin utilizado es el N° <b>$pinAnio" . "$pinNumero</b>.</p>";
                            ?>
                            <br/>
                            <p><b>Importante: </b>Adjunte este certificado al formulario físico.</p>
                            <p>La fecha límite de entrega es el <b>30 de Julio de 2016</b> a las <b>7:30 am</b>.</p>

                            <!----------------- PANEL DE BOTONES ----------------->
                            <style type="text/css" media="print">
                                @page { size: letter;/*landscape;*/ }
                            </style>
                            <div align="center">
                                <input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/>
                                <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                            </div>
                            <?php
                        } else {
                            ?>
                            <form action="" method="post" enctype="multipart/form-data" class="registro">
                                <h4 align="center">Pin no utilizado</h4>
                                <?php
                                echo "<p>El Pin <b>" . $pin->getAnio() . $pin->getNumero() . "</b> no ha sido utilizado.</p>";
                                ?>
                                <div align="center">
                                    <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                                </div>
                            </form>
                            <?php
                        }
                    } else {
                        ?>
                        <h4 align="center">Pin no encontrado</h4>
                        <?php
                        echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                        ?>
                        <div align="center">
                            <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <h4 align="center">Pin no válido</h4>
                    <?php
                    echo "<p>Se ha ingresado un Pin no válido.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </body>
</html>