<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/FormularioPadres.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Mostrar Formulario - Información de los Padres</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        if (strlen($pin) == LONGITUD_PIN) {
            $formulario = FormularioPadres::obtenerFormularioPadres($pin);
            if ($formulario != null) {
                $madre = $formulario->getMadre();
                $padre = $formulario->getPadre();
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Mostrar Formulario - Información de los Padres</h4>
                    <div>
                        <label>Pin:</label>
                        <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                    </div>

                    <!----------------- DATOS DE LA MADRE ----------------->
                    <h6 align="center">Datos de la Madre</h6>
                    <div>
                        <label>Fotografía de la Madre:</label>
                        <?php
                        $fotoMadre = $madre->getFoto();
                        if ($fotoMadre->getId() != null) {
                            echo imageField("imagenMadre", $fotoMadre->getId());
                        } else {
                            echo imageField("imagenMadre");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Madre:</label>
                        <?php echo '<input name="primerApellidoMadre" id="primerApellidoMadre" type="text" value="' . $madre->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Madre:</label>
                        <?php echo '<input name="segundoApellidoMadre" id="segundoApellidoMadre" type="text" value="' . $madre->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres de la Madre:</label>
                        <?php echo '<input name="nombresMadre" id="nombresMadre" type="text" value="' . $madre->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoMadre" id="lugarNacimientoMadre" type="text" value="' . $madre->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoMadre" id="fechaNacimientoMadre" type="date" value="' . $madre->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadMadre" id="edadMadre" type="number" value="' . $madre->getEdadCumplida() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidMadre" id="tidMadre" type="text" value="' . $madre->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidMadre" id="nidMadre" type="text" value="' . $madre->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <?php echo '<input name="profesionMadre" id="profesionMadre" type="text" value="' . $madre->getProfesion() . '" readonly/>' ?>
                    </div>

                    <?php
                    $empresaMadre = $madre->getEmpresa();
                    echo '<div>';
                    echo '<label>Empresa donde Trabaja:</label>';
                    echo '<input name="empresaMadre" id="empresaMadre" type="text" value="' . $empresaMadre . '" readonly/>';
                    echo '</div>';
                    
                    if ($empresaMadre == "Independiente") {
                        echo '<div>';
                        echo '<label>Trabajo que Realiza:</label>';
                        echo '<input name="trabajoMadre" id="trabajoMadre" type="text" value="' . $madre->getTrabajo() . '" readonly/>';
                        echo '</div>';
                    } else {

                        echo '<div>';
                        echo '<label>Cargo que Desempeña:</label>';
                        echo '<input name="cargoMadre" id="cargoMadre" type="text" value="' . $madre->getCargo() . '" readonly/>';
                        echo '</div>';
                    }
                    
                    $celularMadre = $madre->getCelular();
                    $telefonoMadre = $madre->getTelefono();
                    $extensionMadre = $madre->getExtension();
                    if ($celularMadre != null) {
                            echo '<div>';
                            echo '<label>Celular:</label>';
                            echo '<input name="celularMadre" id="celularMadre" type="text" value="' . $celularMadre . '" readonly/>';
                            echo '</div>';
                    }
                    if ($telefonoMadre != null) {
                            echo '<div>';
                            echo '<label>Telefono:</label>';
                            echo '<input name="telefonoMadre" id="telefonoMadre" type="text" value="' . $telefonoMadre . '" readonly/>';
                            echo '</div>';
                    }
                        if ($extensionMadre != null) {
                            echo '<div>';
                            echo '<label>Extensión:</label>';
                            echo '<input name="extensionMadre" id="extensionMadre" type="text" value="' . $extensionMadre . '" readonly/>';
                            echo '</div>';
                    }
                    
                    echo '<div>';
                    echo '<label>Ingresos Mensuales:</label>';
                    echo '<input name="ingresosMadre" id="ingresosMadre" type="text" value="' . $madre->getIngresosMensuales() . '" readonly/>';
                    echo '</div>';
                    ?>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <?php
                    $estudiosPrimariosMadre = $madre->getEstudiosPrimarios();
                    $estudiosSecundariosMadre = $madre->getEstudiosSecundarios();
                    $estudiosUniversitariosMadre = $madre->getEstudiosUniversitarios();
                    $estudiosOtrosMadre = $madre->getEstudiosOtros();
                    if ($estudiosPrimariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Primarios:</label>';
                        echo '<input name="estudiosPrimariosMadre" id="estudiosPrimariosMadre" type="text" value="' . $estudiosPrimariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosSecundariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Secundarios:</label>';
                        echo '<input name="estudiosSecundariosMadre" id="estudiosSecundariosMadre" type="text" value="' . $estudiosSecundariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosUniversitariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Universitarios:</label>';
                        echo '<input name="estudiosUniversitariosMadre" id="estudiosUniversitariosMadre" type="text" value="' . $estudiosUniversitariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosOtrosMadre != null) {
                        echo '<div>';
                        echo '<label>* Otros Estudios:</label>';
                        echo '<input name="estudiosOtrosMadre" id="estudiosOtrosMadre" type="text" value="' . $estudiosOtrosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $resideEnMadre = $madre->getResideEn();
                    $tipoResidenciaMadre = $madre->gettipoResidencia();
                    echo '<div>';
                    echo '<label>Reside en:</label>';
                    echo '<radio>';
                    if ($resideEnMadre == "C") {
                        echo '<input name="resideEnRadioMadre" id="resideEnRadioMadre" type="text" value="Casa" readonly/>';
                    } else {
                        echo '<input name="resideEnRadioMadre" id="resideEnRadioMadre" type="text" value="Apartamento" readonly/>';
                    }
                    echo '<br/><br/>';
                    if ($tipoResidenciaMadre == "P") {
                        echo '<input name="tipoResidenciaRadioMadre" id="tipoResidenciaRadioMadre" type="text" value="Propia" readonly/>';
                    } else {
                        echo '<input name="tipoResidenciaRadioMadre" id="tipoResidenciaRadioMadre" type="text" value="Arriendo" readonly/>';
                    }
                    echo '</radio></div>';
                    ?>

                    <div>
                        <label>E-mail:</label>
                        <?php echo '<input name="correoMadre" id="correoMadre" type="email" value="' . $madre->getEmail() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <?php echo '<textarea cols="40" rows="5" name="interesesMadre" id="interesesMadre" type="text" readonly>' . $madre->getIntereses() . '</textarea>' ?>
                    </div>

                    <!----------------- DATOS DEL PADRE ----------------->
                    <h6 align="center">Datos del Padre</h6>
                    <div>
                        <label>Fotografía de la Padre:</label>
                        <?php
                        $fotoPadre = $padre->getFoto();
                        if ($fotoPadre->getId() != null) {
                            echo imageField("imagenPadre", $fotoPadre->getId());
                        } else {
                            echo imageField("imagenPadre");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Padre:</label>
                        <?php echo '<input name="primerApellidoPadre" id="primerApellidoPadre" type="text" value="' . $padre->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Padre:</label>
                        <?php echo '<input name="segundoApellidoPadre" id="segundoApellidoPadre" type="text" value="' . $padre->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres de la Padre:</label>
                        <?php echo '<input name="nombresPadre" id="nombresPadre" type="text" value="' . $padre->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoPadre" id="lugarNacimientoPadre" type="text" value="' . $padre->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoPadre" id="fechaNacimientoPadre" type="date" value="' . $padre->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadPadre" id="edadPadre" type="number" value="' . $padre->getEdadCumplida() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidPadre" id="tidPadre" type="text" value="' . $padre->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidPadre" id="nidPadre" type="text" value="' . $padre->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <?php echo '<input name="profesionPadre" id="profesionPadre" type="text" value="' . $padre->getProfesion() . '" readonly/>' ?>
                    </div>

                    <?php
                    $empresaPadre = $padre->getEmpresa();
                    echo '<div>';
                    echo '<label>Empresa donde Trabaja:</label>';
                    echo '<input name="empresaPadre" id="empresaPadre" type="text" value="' . $empresaPadre . '" readonly/>';
                    echo '</div>';
                    
                    if ($empresaPadre == "Independiente") {
                        echo '<div>';
                        echo '<label>Trabajo que Realiza:</label>';
                        echo '<input name="trabajoPadre" id="trabajoPadre" type="text" value="' . $padre->getTrabajo() . '" readonly/>';
                        echo '</div>';
                    } else {
                        echo '<div>';
                        echo '<label>Cargo que Desempeña:</label>';
                        echo '<input name="cargoPadre" id="cargoPadre" type="text" value="' . $padre->getCargo() . '" readonly/>';
                        echo '</div>';
                    }
                    
                    $celularPadre = $padre->getCelular();
                    $telefonoPadre = $padre->getTelefono();
                    $extensionPadre = $padre->getExtension();
                    if ($celularPadre != null) {
                        echo '<div>';
                        echo '<label>Celular:</label>';
                        echo '<input name="celularPadre" id="celularPadre" type="text" value="' . $celularPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($telefonoPadre != null) {
                        echo '<div>';
                        echo '<label>Telefono:</label>';
                        echo '<input name="telefonoPadre" id="telefonoPadre" type="text" value="' . $telefonoPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($extensionPadre != null) {
                        echo '<div>';
                        echo '<label>Extensión:</label>';
                        echo '<input name="extensionPadre" id="extensionPadre" type="text" value="' . $extensionPadre . '" readonly/>';
                        echo '</div>';
                    }
                    
                    echo '<div>';
                    echo '<label>Ingresos Mensuales:</label>';
                    echo '<input name="ingresosPadre" id="ingresosPadre" type="text" value="' . $padre->getIngresosMensuales() . '" readonly/>';
                    echo '</div>';
                    ?>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <?php
                    $estudiosPrimariosPadre = $padre->getEstudiosPrimarios();
                    $estudiosSecundariosPadre = $padre->getEstudiosSecundarios();
                    $estudiosUniversitariosPadre = $padre->getEstudiosUniversitarios();
                    $estudiosOtrosPadre = $padre->getEstudiosOtros();
                    if ($estudiosPrimariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Primarios:</label>';
                        echo '<input name="estudiosPrimariosPadre" id="estudiosPrimariosPadre" type="text" value="' . $estudiosPrimariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosSecundariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Secundarios:</label>';
                        echo '<input name="estudiosSecundariosPadre" id="estudiosSecundariosPadre" type="text" value="' . $estudiosSecundariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosUniversitariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Universitarios:</label>';
                        echo '<input name="estudiosUniversitariosPadre" id="estudiosUniversitariosPadre" type="text" value="' . $estudiosUniversitariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosOtrosPadre != null) {
                        echo '<div>';
                        echo '<label>* Otros Estudios:</label>';
                        echo '<input name="estudiosOtrosPadre" id="estudiosOtrosPadre" type="text" value="' . $estudiosOtrosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $resideEnPadre = $padre->getResideEn();
                    $tipoResidenciaPadre = $padre->gettipoResidencia();
                    echo '<div>';
                    echo '<label>Reside en:</label>';
                    echo '<radio>';
                    if ($resideEnPadre == "C") {
                        echo '<input name="resideEnRadioPadre" id="resideEnRadioPadre" type="text" value="Casa" readonly/>';
                    } else {
                        echo '<input name="resideEnRadioPadre" id="resideEnRadioPadre" type="text" value="Apartamento" readonly/>';
                    }
                    echo '<br/><br/>';
                    if ($tipoResidenciaPadre == "P") {
                        echo '<input name="tipoResidenciaRadioPadre" id="tipoResidenciaRadioPadre" type="text" value="Propia" readonly/>';
                    } else {
                        echo '<input name="tipoResidenciaRadioPadre" id="tipoResidenciaRadioPadre" type="text" value="Arriendo" readonly/>';
                    }
                    echo '</radio></div>';
                    ?>

                    <div>
                        <label>E-mail:</label>
                        <?php echo '<input name="correoPadre" id="correoPadre" type="email" value="' . $padre->getEmail() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <?php echo '<textarea cols="40" rows="5" name="interesesPadre" id="interesesPadre" type="text" readonly>' . $padre->getIntereses() . '</textarea>' ?>
                    </div>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <style type="text/css" media="print">
                        @page { size: letter; }
                    </style>
                    <div align="center">
                        <!--input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/-->
                        <?php echo '<input name="formulario" type="button" value="Ver Información del Educando" onclick="window.location = \'mostrarFormulario.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <?php echo '<input name="formularioAdicional" type="button" value="Ver Información Adicional" onclick="window.location = \'mostrarFormularioAdicional.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin no encontrado</h4>
                    <?php
                    echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no válido</h4>
                <?php
                echo "<p>Se ha ingresado un Pin no válido.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
    </body>
</html>