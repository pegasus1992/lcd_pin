<?php

include_once 'persistance/database.php';
include_once 'model/Archivo.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $archivo = Archivo::getArchivo($id);
    $contenido = $archivo->getBinario();
    header("Content-type:" . $archivo->getTipo());
    header("Content-Disposition: attachment; filename=" . $archivo->getNombre());
    print $contenido;
}