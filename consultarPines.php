<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/consultarPinesController.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Consultar Pines</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:450px" class="texto2">
                <h4 align="center">Consultar Pines</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>Número de Pin</th>
                        <th>Formulario</th>
                        <th>Utilizado</th>
                    </tr>
                    <?php
                    $pines = Pin::getPines();
                    if ($pines != null) {
                        foreach ($pines as $pin) {
                            echo '<tr><form action="" method="get">'
                            . '<td align="center"><input type="text" name="pin" style="width:100px" value="' . $pin->getAnio() . $pin->getNumero() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="numero" style="width:30px" value="' . $pin->getNumeroFormulario() . '" readonly></td>';
                            if ($pin->getUtilizado() == "S") {
                                echo '<td align="center"><input type="text" name="utilizado" style="width:15px" value="Sí" readonly></td>';
                                echo '<td align="center"><input name="verFormulario" type="submit" value="Ver Formulario" class="Button"></td>';
                            } else {
                                echo '<td align="center"><input type="text" name="utilizado" style="width:15px" value="No" readonly></td>';
                            }
                            echo '</form></tr>';
                        }
                    }
                    ?>
                </table>
                <br/>

                <!----------------- PANEL DE BOTONES ----------------->
                <div align="center">
                    <input name="return" type="button" value="Exportar a Excel" onclick="window.location = 'excel.php'" class="Button2">
                    <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>