document.getElementById("adicionarHermano").onclick = function () {
    adicionarFilaHermanos();
};

document.getElementById("adicionarInstitucion").onclick = function () {
    adicionarFilaInstituciones();
};

function adicionarFilaHermanos() {
    var table = document.getElementById("hermanosTabla");
    var row = table.insertRow(table.rows.length);
    var nombres = row.insertCell(0);
    var apellidos = row.insertCell(1);
    var edad = row.insertCell(2);
    var colegio = row.insertCell(3);
    var grado = row.insertCell(4);
    //var quitar = row.insertCell(5);
    nombres.innerHTML = "<input id=nombresHermano type=text name=hermanos[] autocomplete=\"on\" maxlength=\"50\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    apellidos.innerHTML = "<input id=apellidosHermano type=text name=hermanos[] autocomplete=\"on\" maxlength=\"50\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    edad.innerHTML = "<input id=edadHermano type=number name=hermanos[] autocomplete=\"on\" min=\"0\" max=\"999\"/>";
    colegio.innerHTML = "<input id=colegioHermano type=text name=hermanos[] autocomplete=\"on\" maxlength=\"50\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    grado.innerHTML = "<input id=gradoHermano type=text name=hermanos[] autocomplete=\"on\" maxlength=\"15\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    //quitar.innerHTML = "<input name=quitarHermano id=quitarHermano type=button value=\"Quitar Hermano\" class=Button onclick=\"remove(this);\">";
}

function remove(t) {
    var td = t.parentNode;
    var tr = td.parentNode;
    var table = tr.parentNode;
    table.removeChild(tr);
}

function adicionarFilaInstituciones() {
    var table = document.getElementById("institucionesTabla");
    var row = table.insertRow(table.rows.length);
    var nombre = row.insertCell(0);
    var grado = row.insertCell(1);
    var anio = row.insertCell(2);
    var telefono = row.insertCell(3);
    nombre.innerHTML = "<input id=nombreInstitucion type=text name=instituciones[] autocomplete=\"on\" maxlength=\"100\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    grado.innerHTML = "<input id=gradoInstitucion type=text name=instituciones[] autocomplete=\"on\" maxlength=\"15\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
    anio.innerHTML = "<input id=anioInstitucion type=number name=instituciones[] autocomplete=\"on\" min=\"1990\" max=\"3000\"/>";
    telefono.innerHTML = "<input id=telefonoInstitucion type=number name=instituciones[] autocomplete=\"on\" min=\"1000000\" max=\"999999999999999\" onkeyup=\"this.value = this.value.toUpperCase();\"/>";
}

function disableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = true;
    }
}

function enableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = false;
    }
}

document.getElementById("casadosOption").onchange = function () {
    disableInput("casadosOption", "estadoCivil");
    disableInput("casadosOption", "separados");
};

document.getElementById("separadosOption").onchange = function () {
    disableInput("separadosOption", "estadoCivil");
    enableInput("separadosOption", "separados");
};

document.getElementById("otroOption").onchange = function () {
    enableInput("otroOption", "estadoCivil");
    disableInput("otroOption", "separados");
};

document.getElementById("adoptadoSi").onchange = function () {
    enableInput("adoptadoSi", "edadAdoptado");
};

document.getElementById("adoptadoNo").onchange = function () {
    disableInput("adoptadoNo", "edadAdoptado");
};

document.getElementById("mamaResponde").onchange = function () {
    disableInput("mamaResponde", "personaResponde");
};

document.getElementById("papaResponde").onchange = function () {
    disableInput("papaResponde", "personaResponde");
};

document.getElementById("ambosResponde").onchange = function () {
    disableInput("ambosResponde", "personaResponde");
};

document.getElementById("otroResponde").onchange = function () {
    enableInput("otroResponde", "personaResponde");
};

document.getElementById("padresFirmanSi").onchange = function () {
    disableInput("padresFirmanSi", "nombreFirma");
};

document.getElementById("padresFirmanNo").onchange = function () {
    enableInput("padresFirmanNo", "nombreFirma");
};

document.getElementById("autorizaSi").onchange = function () {
    disableInput("autorizaSi", "autorizaExplique");
};

document.getElementById("autorizaNo").onchange = function () {
    enableInput("autorizaNo", "autorizaExplique");
};

document.getElementById("miembroReferencia").onkeyup = function () {
    this.value = this.value.toUpperCase();
    var miembroReferencia = document.getElementById("miembroReferencia").value;
    if(miembroReferencia != null && miembroReferencia != "") {
        document.getElementById("padresReferencia").disabled = false;
        document.getElementById("gradoReferencia").disabled = false;
    } else {
        document.getElementById("padresReferencia").disabled = true;
        document.getElementById("gradoReferencia").disabled = true;
    }
};

document.getElementById("escolaridadSi").onchange = function () {
    enableInput("escolaridadSi", "pazSalvo");
    enableInput("escolaridadSi", "ultimoBoletin");
};

document.getElementById("escolaridadNo").onchange = function () {
    disableInput("escolaridadNo", "pazSalvo");
    disableInput("escolaridadNo", "ultimoBoletin");
};