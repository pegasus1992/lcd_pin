function edad(date) {
    var fecha = new Date(date);
    var hoy = new Date("2017-03-31");
    var ed = parseInt((hoy - fecha) / 365 / 24 / 60 / 60 / 1000);
    /*if (ed >= 3)*/ {
        document.getElementById('edadNino').value = ed;
        document.getElementById('fechaNacimientoNinoError').innerHTML = "";
    } /*else {
        document.getElementById('edadNino').value = "";
        document.getElementById('fechaNacimientoNino').focus();
        document.getElementById('fechaNacimientoNinoError').innerHTML = "El niño debe tener 3 años cumplidos a 31 de Marzo de 2017.";
    }*/
}

function disableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = true;
    }
}

function enableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = false;
    }
}

document.getElementById("bisDireccion").onchange = function () {
    var bis = document.getElementById("bisDireccion").checked;
    if (bis) {
        document.getElementById("az2").disabled = false;
    } else {
        document.getElementById("az2").disabled = true;
    }
};

document.getElementById("hermanoRadio").onchange = function () {
    disableInput("hermanoRadio", "conocidoNino");
};

document.getElementById("primoRadio").onchange = function () {
    disableInput("primoRadio", "conocidoNino");
};

document.getElementById("amigoRadio").onchange = function () {
    disableInput("amigoRadio", "conocidoNino");
};

document.getElementById("otroRadio").onchange = function () {
    enableInput("otroRadio", "conocidoNino");
};

document.getElementById("ningunoRadio").onchange = function () {
    disableInput("ningunoRadio", "conocidoNino");
};

document.getElementById("impedimentoSi").onchange = function () {
    enableInput("impedimentoSi", "nombreImpedimentoNino");
};

document.getElementById("impedimentoNo").onchange = function () {
    disableInput("impedimentoNo", "nombreImpedimentoNino");
};

document.getElementById("tenidoAyudaSi").onchange = function () {
    enableInput("tenidoAyudaSi", "nombreAyudaNino");
};

document.getElementById("tenidoAyudaNo").onchange = function () {
    disableInput("tenidoAyudaNo", "nombreAyudaNino");
};

document.getElementById("enfermedadSi").onchange = function () {
    enableInput("enfermedadSi", "enfermedadNino");
};

document.getElementById("enfermedadNo").onchange = function () {
    disableInput("enfermedadNo", "enfermedadNino");
};

document.getElementById("medicamentoSi").onchange = function () {
    enableInput("medicamentoSi", "medicamentoEspecialNino");
};

document.getElementById("medicamentoNo").onchange = function () {
    disableInput("medicamentoNo", "medicamentoEspecialNino");
};

document.getElementById("transporteEscolarSi").onchange = function () {
    var checked = document.getElementById("transporteEscolarSi").checked;
    if (checked) {
        document.getElementById("mediaRuta").disabled = false;
        document.getElementById("rutaCompleta").disabled = false;
        
        document.getElementById("tipoDireccionRuta").disabled = false;
        document.getElementById("numTipoDireccionRuta").disabled = false;
        document.getElementById("az1Ruta").disabled = false;
        document.getElementById("bisDireccionRuta").disabled = false;
        document.getElementById("se1Ruta").disabled = false;
        document.getElementById("numDireccionRuta").disabled = false;
        document.getElementById("numDireccion2Ruta").disabled = false;
        document.getElementById("az3Ruta").disabled = false;
        document.getElementById("se2Ruta").disabled = false;
        document.getElementById("intDireccionRuta").disabled = false;
        
        document.getElementById("barrioRutaNino").disabled = false;
    }
};

document.getElementById("transporteEscolarNo").onchange = function () {
    var checked = document.getElementById("transporteEscolarNo").checked;
    if (checked) {
        document.getElementById("mediaRuta").disabled = true;
        document.getElementById("rutaCompleta").disabled = true;
        
        document.getElementById("tipoDireccionRuta").disabled = true;
        document.getElementById("numTipoDireccionRuta").disabled = true;
        document.getElementById("az1Ruta").disabled = true;
        document.getElementById("bisDireccionRuta").disabled = true;
        document.getElementById("se1Ruta").disabled = true;
        document.getElementById("numDireccionRuta").disabled = true;
        document.getElementById("numDireccion2Ruta").disabled = true;
        document.getElementById("az3Ruta").disabled = true;
        document.getElementById("se2Ruta").disabled = true;
        document.getElementById("intDireccionRuta").disabled = true;
        
        document.getElementById("barrioRutaNino").disabled = true;
    }
};

document.getElementById("bisDireccionRuta").onchange = function () {
    var bis = document.getElementById("bisDireccionRuta").checked;
    if (bis) {
        document.getElementById("az2Ruta").disabled = false;
    } else {
        document.getElementById("az2Ruta").disabled = true;
    }
};

document.getElementById("usaGafasSi").onchange = function () {
    enableInput("usaGafasSi", "deficienciaVisualNino");
};

document.getElementById("usaGafasNo").onchange = function () {
    disableInput("usaGafasNo", "deficienciaVisualNino");
};

document.getElementById("fracturaSi").onchange = function () {
    enableInput("fracturaSi", "detalleFracturaNino");
};

document.getElementById("fracturaNo").onchange = function () {
    disableInput("fracturaNo", "detalleFracturaNino");
};