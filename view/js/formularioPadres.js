function ageMadre(date) {
    var fecha = new Date(date);
    var hoy = new Date();
    var ed = parseInt((hoy - fecha) / 365 / 24 / 60 / 60 / 1000);
    if (ed >= 18) {
        document.getElementById('edadMadre').value = ed;
        document.getElementById('fechaNacimientoMadreError').innerHTML = "";
    } else {
        document.getElementById('edadMadre').value = "";
        document.getElementById('fechaNacimientoMadre').focus();
        document.getElementById('fechaNacimientoMadreError').innerHTML = "La madre debe ser mayor de edad.";
    }
}

function agePadre(date) {
    var fecha = new Date(date);
    var hoy = new Date();
    var ed = parseInt((hoy - fecha) / 365 / 24 / 60 / 60 / 1000);
    if (ed >= 18) {
        document.getElementById('edadPadre').value = ed;
        document.getElementById('fechaNacimientoPadreError').innerHTML = "";
    } else {
        document.getElementById('edadPadre').value = "";
        document.getElementById('fechaNacimientoPadre').focus();
        document.getElementById('fechaNacimientoPadreError').innerHTML = "El padre debe ser mayor de edad.";
    }
}

function disableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = true;
    }
}

function enableInput(idRadio, idInput) {
    var checked = document.getElementById(idRadio).checked;
    if (checked) {
        document.getElementById(idInput).disabled = false;
    }
}

document.getElementById("independienteSiMadre").onchange = function () {
    disableInput("independienteSiMadre", "empresaMadre");
    enableInput("independienteSiMadre", "celularMadre");
    enableInput("independienteSiMadre", "telefonoMadre");
    enableInput("independienteSiMadre", "extensionMadre");
    disableInput("independienteSiMadre", "cargoMadre");
    enableInput("independienteSiMadre", "ingresosMadre");
    enableInput("independienteSiMadre", "trabajoMadre");
};

document.getElementById("independienteNoMadre").onchange = function () {
    enableInput("independienteNoMadre", "empresaMadre");
    enableInput("independienteNoMadre", "celularMadre");
    enableInput("independienteNoMadre", "telefonoMadre");
    enableInput("independienteNoMadre", "extensionMadre");
    enableInput("independienteNoMadre", "cargoMadre");
    enableInput("independienteNoMadre", "ingresosMadre");
    disableInput("independienteNoMadre", "trabajoMadre");
};

document.getElementById("independienteSiPadre").onchange = function () {
    disableInput("independienteSiPadre", "empresaPadre");
    enableInput("independienteSiPadre", "celularPadre");
    enableInput("independienteSiPadre", "telefonoPadre");
    enableInput("independienteSiPadre", "extensionPadre");
    disableInput("independienteSiPadre", "cargoPadre");
    enableInput("independienteSiPadre", "ingresosPadre");
    enableInput("independienteSiPadre", "trabajoPadre");
};

document.getElementById("independienteNoPadre").onchange = function () {
    enableInput("independienteNoPadre", "empresaPadre");
    enableInput("independienteNoPadre", "celularPadre");
    enableInput("independienteNoPadre", "telefonoPadre");
    enableInput("independienteNoPadre", "extensionPadre");
    enableInput("independienteNoPadre", "cargoPadre");
    enableInput("independienteNoPadre", "ingresosPadre");
    disableInput("independienteNoPadre", "trabajoPadre");
};