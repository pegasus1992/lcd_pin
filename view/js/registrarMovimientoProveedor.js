$(document).ready(function() {
    $("#selectProveedor").bind('change', function() {
        var value = $(this).val();
        if(value != null && value != "") {
            var json = replaceHtml2Json(value);
            var nit = json.nit + "-" + json.digito;
            $("#nitProveedor").val(nit);
            $("#nombreProveedor").val(json.nombre);
            $("#correoProveedor").val(json.email);
            $("#direccionProveedor").val(json.direccion);
        } else {
            $("#nitProveedor").val("");
            $("#nombreProveedor").val("");
            $("#correoProveedor").val("");
            $("#direccionProveedor").val("");
        }
    });
    /*$(".codigoBarras").bind('change', function() {
        var value = $(this).val();
        if(value != null && value != "") {
            var json = replaceHtml2Json(value);
            $(".nombreProducto");
        } else {
            
        }
    });*/
});

function replaceHtml2Json(json) {
    return jQuery.parseJSON(json.replace(/'/g, '"'));
}