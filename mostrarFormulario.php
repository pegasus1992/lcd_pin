<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';
include_once './model/Formulario.php';
include_once './model/Foto.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Mostrar Formulario - Información del Educando</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        if (strlen($pin) == LONGITUD_PIN) {
            $formulario = Formulario::getFormulario($pin);
            if ($formulario != null) {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Mostrar Formulario - Información del Educando</h4>
                    <div>
                        <label>Pin:</label>
                        <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fotografía del Niño:</label>
                        <?php
                        $fotoNino = $formulario->getFoto();
                        if ($fotoNino->getId() != null) {
                            echo imageField("imagenNino", $fotoNino->getId());
                        } else {
                            echo imageField("imagenNino");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido del Niño:</label>
                        <?php echo '<input name="primerApellidoNino" id="primerApellidoNino" type="text" value="' . $formulario->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido del Niño:</label>
                        <?php echo '<input name="segundoApellidoNino" id="segundoApellidoNino" type="text" value="' . $formulario->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres del Niño:</label>
                        <?php echo '<input name="nombresNino" id="nombresNino" type="text" value="' . $formulario->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoNino" id="lugarNacimientoNino" type="text" value="' . $formulario->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoNino" id="fechaNacimientoNino" type="date" value="' . $formulario->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadNino" id="edadNino" type="number" value="' . $formulario->getEdadCumplida() . '" readonly/>' ?>
                    </div>

                    <?php
                    $grupoSanguineo = $formulario->getGrupoSanguineo();
                    $rh = $formulario->getRh();
                    
                    echo '<div>';
                    echo '<label>Grupo Sanguíneo:</label>';
                    echo '<grupoRH>';
                    echo '<input name="grupoSanguineoNino" id="grupoSanguineo" type="text" value="' . $grupoSanguineo . '" readonly/>';
                    
                    if($rh == "P") {
                         echo '<input name="rhNino" id="rh" type="text" value="Positivo" readonly/>';
                    } else {
                         echo '<input name="rhNino" id="rh" type="text" value="Negativo" readonly/>';
                    }
                    echo '</grupoRH>';
                    echo '</div>';
                    ?>
                    
                    <div>
                        <label>Estrato donde vive:</label>
                        <?php echo '<input name="estratoNino" id="estratoNino" type="number" value="' . $formulario->getEstrato() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>EPS a la que está afiliado:</label>
                        <?php echo '<input name="epsNino" id="epsNino" type="text" value="' . $formulario->getEps() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidNino" id="tidNino" type="text" value="' . $formulario->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidNino" id="nidNino" type="text" value="' . $formulario->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de expedición del documento:</label>
                        <?php echo '<input name="lugarExpedicionNino" id="lugarExpedicionNino" type="text" value="' . $formulario->getLugarExpedicion() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nacionalidad:</label>
                        <?php echo '<input name="nacionalidadNino" id="nacionalidadNino" type="text" value="' . $formulario->getNacionalidad() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Dirección de Residencia:</label>
                        <?php echo '<input name="direccionNino" id="direccionNino" type="text" value="' . $formulario->getDireccionResidencia() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Barrio:</label>
                        <?php echo '<input name="barrioNino" id="barrioNino" type="text" value="' . $formulario->getBarrio() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Teléfono:</label>
                        <?php echo '<input name="telefonoNino" id="telefonoNino" type="text" value="' . $formulario->getTelefono() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Grado al que desea Ingresar:</label>
                        <?php echo '<input name="gradoDeseaNino" id="gradoDeseaNino" type="text" value="' . $formulario->getGradoIngresar() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Grado que cursa en el Presente Año:</label>
                        <?php echo '<input name="gradoActualNino" id="gradoActualNino" type="text" value="' . $formulario->getGradoActual() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Estudia en el Liceo Campo David:</label>
                        <?php echo '<input name="conocidoNino" id="conocidoNino" type="text" value="' . $formulario->getConocidoEstudia() . '" readonly/>' ?>
                    </div>

                    <?php
                    $impedimento = $formulario->getImpedimentoFisico();
                    echo '<div>';
                    echo '<label>¿El educando tiene algún tipo de impedimento físico, psicológico o mental?</label>';
                    if ($impedimento == "S") {
                        echo '<input name="impedimentoRadio" id="impedimentoRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="impedimentoRadio" id="impedimentoRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($impedimento == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="nombreImpedimentoNino" id="nombreImpedimentoNino" type="text" value="' . $formulario->getNombreImpedimentoFisico() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $ayuda = $formulario->getTenidoAyuda();
                    echo '<div>';
                    echo '<label>¿Ha tenido el educando aspirante algún tipo de ayuda psicológica, de fonoaudiología y/o terapia ocupacional?</label>';
                    if ($ayuda == "S") {
                        echo '<input name="tenidoAyudaRadio" id="tenidoAyudaRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="tenidoAyudaRadio" id="tenidoAyudaRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($ayuda == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<textarea cols="40" rows="5" name="nombreAyudaNino" id="nombreAyudaNino" type="text" readonly>' . $formulario->getNombreAyuda() . '</textarea>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $enfermedad = $formulario->getEnfermedad();
                    echo '<div>';
                    echo '<label>¿Padece el educando alguna enfermedad de consideración?</label>';
                    if ($enfermedad == "S") {
                        echo '<input name="enfermedadRadio" id="enfermedadRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="enfermedadRadio" id="enfermedadRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($enfermedad == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="enfermedadNino" id="enfermedadNino" type="text" value="' . $formulario->getNombreEnfermedad() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $medicamento = $formulario->getMedicamentoEspecial();
                    echo '<div>';
                    echo '<label>¿Debe ingerir diaria o periódicamente algún medicamento especial?</label>';
                    if ($medicamento == "S") {
                        echo '<input name="medicamentoEspecialRadio" id="medicamentoEspecialRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="medicamentoEspecialRadio" id="medicamentoEspecialRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($medicamento == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="medicamentoEspecialNino" id="medicamentoEspecialNino" type="text" value="' . $formulario->getNombreMedicamentoEspecial() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $transporte = $formulario->getTransporteEscolar();
                    echo '<div>';
                    echo '<label>¿Requiere servicio de Transporte Escolar?</label>';
                    if ($transporte == "S") {
                        echo '<input name="transporteEscolarRadio" id="transporteEscolarRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="transporteEscolarRadio" id="transporteEscolarRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($transporte == "S") {
                        $tipoRuta = $formulario->getTipoRuta();
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        if ($tipoRuta == "M") {
                            echo '<input name="tipoRutaRadio" id="tipoRutaRadio" type="text" value="Media Ruta" readonly/>';
                        } else {
                            echo '<input name="tipoRutaRadio" id="tipoRutaRadio" type="text" value="Ruta Completa" readonly/>';
                        }
                        echo '</div>';

                        echo '<div>';
                        echo '<label>(*) Dirección</label>';
                        echo '<input name="direccionRutaNino" id="direccionRutaNino" type="text" value="' . $formulario->getDireccionRuta() . '" readonly/>';
                        echo '</div>';

                        echo '<div>';
                        echo '<label>(*) Barrio</label>';
                        echo '<input name="barrioRutaNino" id="barrioRutaNino" type="text" value="' . $formulario->getBarrioRuta() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $restaurante = $formulario->getRestauranteEscolar();
                    echo '<div>';
                    echo '<label>¿Requiere servicio de Restaurante Escolar?</label>';
                    if ($restaurante == "S") {
                        echo '<input name="restauranteEscolarRadio" id="restauranteEscolarRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="restauranteEscolarRadio" id="restauranteEscolarRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    ?>

                    <?php
                    $mano = $formulario->getManoDominante();
                    echo '<div>';
                    echo '<label>El Educando es:</label>';
                    if ($mano == "Z") {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Zurdo" readonly/>';
                    } else if ($mano == "D") {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Diestro" readonly/>';
                    } else {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Ambidiestro" readonly/>';
                    }
                    echo '</div>';
                    ?>

                    <?php
                    $usaGafas = $formulario->getUsaGafas();
                    echo '<div>';
                    echo '<label>¿Usa gafas o lentes formulados por un especialista?</label>';
                    if ($usaGafas == "S") {
                        echo '<input name="usaGafasRadio" id="usaGafasRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="usaGafasRadio" id="usaGafasRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($usaGafas == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Qué deficiencia visual presenta?</label>';
                        echo '<input name="deficienciaVisualNino" id="deficienciaVisualNino" type="text" value="' . $formulario->getDeficienciaVisual() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $fractura = $formulario->getFractura();
                    echo '<div>';
                    echo '<label>¿Ha sufrido alguna fractura?</label>';
                    if ($fractura == "S") {
                        echo '<input name="fracturaRadio" id="fracturaRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="fracturaRadio" id="fracturaRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($fractura == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Qué deficiencia visual presenta?</label>';
                        echo '<input name="detalleFracturaNino" id="detalleFracturaNino" type="text" value="' . $formulario->getDetalleFractura() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>
                    <?php
                    $aspectos = $formulario->getAspectosEspeciales();
                    if ($aspectos != null) {
                        echo '<div>';
                        echo '<label>¿Desea señalar algún aspecto especial del estado de su hijo(a) que considere Ud. importante y además, la deba conocer el Liceo?</label>';
                        echo '<textarea cols="40" rows="5" name="aspectosEspecialesNino" id="aspectosEspecialesNino" type="text" readonly>' . $aspectos . '</textarea>';
                        echo '</div>';
                    }
                    ?>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <style type="text/css" media="print">
                        @page { size: letter; }
                    </style>
                    <div align="center">
                        <!--input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/-->
                        <?php echo '<input name="formularioPadres" type="button" value="Ver Información de los Padres" onclick="window.location = \'mostrarFormularioPadres.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <?php echo '<input name="formularioAdicional" type="button" value="Ver Información Adicional" onclick="window.location = \'mostrarFormularioAdicional.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin no encontrado</h4>
                    <?php
                    echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no válido</h4>
                <?php
                echo "<p>Se ha ingresado un Pin no válido.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
    </body>
</html>