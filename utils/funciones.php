<?php

include_once './utils/constantes.php';

/**
 * Permite redirigir a la pagina ingresada.
 * @param type $pagina
 */
function redirigir($pagina) {
    header("location:" . $pagina);
}

/**
 * Permite despliegar en el navegador el mensaje ingresado.
 * @param type $mensaje
 * @return type
 */
function mostrarMensaje($mensaje) {
    return "<script language='javascript'>alert('" . $mensaje . "');</script>";
}

/**
 * Permite cargar los datos de sesion.
 */
function initSession() {
    if (!isset($_SESSION)) {
        session_start();
    }
}

/**
 * Devuelve un campo de imagen en HTML, dado el id de la imagen.
 * @param type $id Id del campo de imagen HTML.
 * @param type $fotoId Id de la imagen.
 * @return type
 */
function imageField($id, $fotoId = null) {
    if ($fotoId == null) {
        return '<img alt="Tamaño Foto 3X4. Tamaño máximo 1MB" id="' . $id . '" style="font-size: 11px;"/>';
    } else {
        return '<img id="' . $id . '" src="imagen.php?id=' . $fotoId . '" style="font-size: 11px;"/>';
    }
}

function replaceJson2Html($json) {
    return str_replace('"', "'", $json);
}

function replaceHtml2Json($json) {
    return str_replace("'", '"', $json);
}

/**
 * Generate a random string, using a cryptographically secure 
 * pseudorandom number generator (random_int).
 * @param type $length <p>How many characters do we want?</p>
 * @return type
 */
function random_str($length) {
    $str = substr(sha1(rand()), 0, $length);
    return letrasHex2Numeros($str);
}

function letrasHex2Numeros($cadena) {
    $caracteres = array('a', 'b', 'c', 'd', 'e', 'f');
    $reemplazos = array(SHA_A, SHA_B, SHA_C, SHA_D, SHA_E, SHA_F);
    return str_replace($caracteres, $reemplazos, $cadena);
}

function quitarTildes($cadena) {
    $caracteres = array('Á', 'É', 'Í', 'Ó', 'Ú', 'á', 'é', 'í', 'ó', 'ú');
    $reemplazos = array('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u');
    return str_replace($caracteres, $reemplazos, $cadena);
}
