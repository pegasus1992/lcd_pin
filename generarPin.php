<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/generarPinController.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Generar Pon</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Generar Pin</h4>
                <div align="center">
                    <?php
                    $pin = Pin::getPinMaximoFormulario();
                    if ($pin->getNumeroFormulario() < MAX_NUM_FORM) {
                        ?>
                        <input name="register" id="submit" type="submit" value="Obtener" class="Button2"/>
                        <?php
                    }
                    ?>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>