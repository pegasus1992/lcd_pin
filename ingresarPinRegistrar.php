<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/ingresarPinRegistrarController.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Ingresar Pin</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <div>
                    <img src="view/images/logo.jpg" alt="" style="width: 15%; height: 15%"/>
                    <h4 align="center">Ingresar Pin</h4>
                </div>
                <div>
                    <label>Ingrese su Pin:</label>
                    <input placeholder="Ingrese el Pin" name="pin" id="pin" type="text" autocomplete="off" maxlength="15" required/>
                </div>

                <!----------------- PANEL DE BOTONES ----------------->
                <div align="center">
                    <br/>
                    <input name="register" id="submit" type="submit" value="Registrar Educando" class="Button2"/>
                    <input name="registerPadres" id="submit" type="submit" value="Registrar Padres" class="Button2"/>
                    <!--br/>
                    <br/-->
                    <input name="registerAdicional" id="submit" type="submit" value="Registrar Info. Adicional" class="Button2"/>
                    <!--input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2"-->
                </div>
            </form>
        </div>
    </body>
</html>