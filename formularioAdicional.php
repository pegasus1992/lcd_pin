<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/formularioAdicionalController.php';

function tipoDireccion($tipo = "") {
    return '<select name="tipoDireccion' . $tipo . '" id="tipoDireccion' . $tipo . '" required>'
            . '<option></option>'
            . '<option value="Cll">Calle</option>'
            . '<option value="Cr">Carrera</option>'
            . '<option value="Dg">Diagonal</option>'
            . '<option value="Tr">Transversal</option>'
            . '<option value="Av. Cll">Avenida Calle</option>'
            . '<option value="Av. Cr">Avenida Carrera</option>'
            . '</select>';
}

function listaAZ($i, $disabled = false, $tipo = "") {
    $dis = $disabled ? 'disabled' : '';
    return '<select name="az' . $i . $tipo . '" id="az' . $i . $tipo . '" ' . $dis . '>'
            . '<option></option>'
            . '<option value="A">A</option><option value="B">B</option><option value="C">C</option>'
            . '<option value="D">D</option><option value="E">E</option><option value="F">F</option>'
            . '<option value="G">G</option><option value="H">H</option><option value="I">I</option>'
            . '<option value="J">J</option><option value="K">K</option><option value="L">L</option>'
            . '<option value="M">M</option><option value="N">N</option><option value="O">O</option>'
            . '<option value="P">P</option><option value="Q">Q</option><option value="R">R</option>'
            . '<option value="S">S</option><option value="T">T</option><option value="U">U</option>'
            . '<option value="V">V</option><option value="W">W</option><option value="X">X</option>'
            . '<option value="Y">Y</option><option value="Z">Z</option>'
            . '</select>';
}

function listaSE($i, $tipo = "") {
    return '<select name="se' . $i . $tipo .'" id="se' . $i . $tipo . '">'
            . '<option></option>'
            . '<option value="N">Norte</option>'
            . '<option value="S">Sur</option>'
            . '<option value="E">Este</option>'
            . '<option value="O">Oeste</option>'
            . '</select>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Diligenciar Formulario - Información Adicional</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        $utilizado = Pin::existePinUtilizado($pin);
        if ($utilizado) {
            $cantidad = FormularioAdicional::obtenerConteoFormulariosAdicionales($pin);
            if ($cantidad == 0) {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro2">
                    <h4 align="center">Diligenciar Formulario - Información Adicional</h4>
                    <div>
                        <label>Pin:</label>
                        <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Actualmente los Padres están:</label>
                        <radio>
                            <input type="radio" name="tipoUnionRadio" id="casadosOption" value="Casados" required/>Casados
                            <br/>
                            <input type="radio" name="tipoUnionRadio" id="separadosOption" value="Separados" required/>Separados
                            <br/>
                            <input type="radio" name="tipoUnionRadio" id="otroOption" value="Otro" required/>Otro
                        </radio>
                    </div>
                    <div>
                        <label>¿Cuál?</label>
                        <input placeholder="Ingrese el estado civil" name="estadoCivil" id="estadoCivil" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>El Educando vive con:</label>
                        <input placeholder="Ingrese con quién vive el educando" name="separados" id="separados" type="text" autocomplete="on" maxlength="100" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>¿El Educando aspirante es Adoptado?</label>
                        <radio>
                            <input type="radio" name="adoptadoRadio" id="adoptadoSi" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="adoptadoRadio" id="adoptadoNo" value="N" required/>No
                            <br/>
                            <br/>
                            <input placeholder="¿Qué edad tenía en el momento de la adopción?" name="edadAdoptado" id="edadAdoptado" type="number" autocomplete="on" min="0" max="999" disabled required/>
                        </radio>
                    </div>
                    <br/>
                    <table id="hermanosTabla" border="0" class="asociados">
                        <tr>
                            <th>Nombres de los hermanos(as)</th>
                            <th>Apellidos de los hermanos(as)</th>
                            <th>Edad</th>
                            <th>Colegio o Institución donde estudia</th>
                            <th>Grado</th>
                            <th><input name="adicionarHermano" id="adicionarHermano" type="button" value="Añadir Hermano" class="Button"></th>
                        </tr>
                    </table>
                    <br/>
                    <div>
                    	<label>En caso de emergencia, señale dirección y número telefónico donde se pueden localizar papá y/o mamá de manera inmediata:</label>
                    </div>
                    <div>
	                    <label>* Dirección de la Madre:</label>
	                    <direccion>
	                        <?php echo tipoDireccion("Madre") ?>
	                        <input name="numTipoDireccionMadre" id="numTipoDireccionMadre" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(1, false, "Madre") ?>
	                        <label><input type="checkbox" name="bisDireccionMadre" id="bisDireccionMadre" value="BIS">Bis</label>
	                        <?php echo listaAZ(2, true, "Madre") ?>
	                        <?php echo listaSE(1, "Madre") ?>
	                        <br/>
	                        <br/>
	                        <label>No.</label>
	                        <input name="numDireccionMadre" id="numDireccionMadre" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(3, false, "Madre") ?>
	                        <label>-</label>
	                        <input name="numDireccion2Madre" id="numDireccion2Madre" type="number" autocomplete="on" min="0"/>
	                        <?php echo listaSE(2, "Madre") ?>
	                        <label>Apto/Int.</label>
	                        <input name="intDireccionMadre" id="intDireccionMadre" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();"/>
	                    </direccion>
                    </div>
                    <div>
	                    <label>* Teléfono de la Madre:</label>
	                    <input placeholder="Ingrese el telefono" name="telefonoMadre" id="telefonoMadre" type="number" autocomplete="on" min="1000000" max="999999999999999" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <br/>
                    <div>
	                    <label>* Dirección del Padre:</label>
	                    <direccion>
	                        <?php echo tipoDireccion("Padre") ?>
	                        <input name="numTipoDireccionPadre" id="numTipoDireccionPadre" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(1, false, "Padre") ?>
	                        <label><input type="checkbox" name="bisDireccionPadre" id="bisDireccionPadre" value="BIS">Bis</label>
	                        <?php echo listaAZ(2, true, "Padre") ?>
	                        <?php echo listaSE(1, "Padre") ?>
	                        <br/>
	                        <br/>
	                        <label>No.</label>
	                        <input name="numDireccionPadre" id="numDireccionPadre" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(3, false, "Padre") ?>
	                        <label>-</label>
	                        <input name="numDireccion2Padre" id="numDireccion2Padre" type="number" autocomplete="on" min="0"/>
	                        <?php echo listaSE(2, "Padre") ?>
	                        <label>Apto/Int.</label>
	                        <input name="intDireccionPadre" id="intDireccionPadre" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();"/>
	                    </direccion>
                    </div>
                    <div>
	                    <label>* Teléfono del Padre:</label>
	                    <input placeholder="Ingrese el teléfono" name="telefonoPadre" id="telefonoPadre" type="number" autocomplete="on" min="1000000" max="999999999999999" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                    	<label>Nombre, Dirección y Teléfono de Familiar(es) cercano(s) que puedan acudir al Liceo de manera inmediata, en caso de emergencia:</label>
                    </div>
                    <div>
	                    <label>* Nombre del Acudiente 1:</label>
	                    <input placeholder="Ingrese el nombre" name="nombreAcudiente1" id="nombreAcudiente1" type="text" autocomplete="on" maxlength="75" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
	                    <label>* Dirección del Acudiente 1:</label>
	                    <direccion>
	                        <?php echo tipoDireccion("Acudiente1") ?>
	                        <input name="numTipoDireccionAcudiente1" id="numTipoDireccionAcudiente1" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(1, false, "Acudiente1") ?>
	                        <label><input type="checkbox" name="bisDireccionAcudiente1" id="bisDireccionAcudiente1" value="BIS">Bis</label>
	                        <?php echo listaAZ(2, true, "Acudiente1") ?>
	                        <?php echo listaSE(1, "Acudiente1") ?>
	                        <br/>
	                        <br/>
	                        <label>No.</label>
	                        <input name="numDireccionAcudiente1" id="numDireccionAcudiente1" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(3, false, "Acudiente1") ?>
	                        <label>-</label>
	                        <input name="numDireccion2Acudiente1" id="numDireccion2Acudiente1" type="number" autocomplete="on" min="0"/>
	                        <?php echo listaSE(2, "Acudiente1") ?>
	                        <label>Apto/Int.</label>
	                        <input name="intDireccionAcudiente1" id="intDireccionAcudiente1" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();"/>
	                    </direccion>
                    </div>
                    <div>
	                    <label>* Teléfono del Acudiente 1:</label>
	                    <input placeholder="Ingrese el telefono" name="telefonoAcudiente1" id="telefonoAcudiente1" type="number" autocomplete="on" min="1000000" max="999999999999999" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <br/>
                    <div>
	                    <label>* Nombre del Acudiente 2:</label>
	                    <input placeholder="Ingrese el nombre" name="nombreAcudiente2" id="nombreAcudiente2" type="text" autocomplete="on" maxlength="75" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
	                    <label>* Dirección del Acudiente 2:</label>
	                    <direccion>
	                        <?php echo tipoDireccion("Acudiente2") ?>
	                        <input name="numTipoDireccionAcudiente2" id="numTipoDireccionAcudiente2" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(1, false, "Acudiente2") ?>
	                        <label><input type="checkbox" name="bisDireccionAcudiente2" id="bisDireccionAcudiente2" value="BIS">Bis</label>
	                        <?php echo listaAZ(2, true, "Acudiente2") ?>
	                        <?php echo listaSE(1, "Acudiente2") ?>
	                        <br/>
	                        <br/>
	                        <label>No.</label>
	                        <input name="numDireccionAcudiente2" id="numDireccionAcudiente2" type="number" autocomplete="on" min="0" required/>
	                        <?php echo listaAZ(3, false, "Acudiente2") ?>
	                        <label>-</label>
	                        <input name="numDireccion2Acudiente2" id="numDireccion2Acudiente2" type="number" autocomplete="on" min="0"/>
	                        <?php echo listaSE(2, "Acudiente2") ?>
	                        <label>Apto/Int.</label>
	                        <input name="intDireccionAcudiente2" id="intDireccionAcudiente2" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();"/>
	                    </direccion>
                    </div>
                    <div>
	                    <label>* Teléfono del Acudiente 2:</label>
	                    <input placeholder="Ingrese el telefono" name="telefonoAcudiente2" id="telefonoAcudiente2" type="number" autocomplete="on" min="1000000" max="999999999999999" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>¿De manera directa, quién responderá con los compromisos formativos, académicos y económicos ante el Liceo Campo David?</label>
                        <radio>
                            <input type="radio" name="personaRespondeRadio" id="mamaResponde" value="Mamá" required/>Mamá
                            <br/>
                            <input type="radio" name="personaRespondeRadio" id="papaResponde" value="Papá" required/>Papá
                            <br/>
                            <input type="radio" name="personaRespondeRadio" id="ambosResponde" value="Ambos" required/>Ambos
                            <br/>
                            <input type="radio" name="personaRespondeRadio" id="otroResponde" value="Otro" required/>Otro
                            <br/>
                            <br/>
                            <input placeholder="¿Quién?" name="personaResponde" id="personaResponde" type="text" autocomplete="on" maxlength="75" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                        </radio>
                    </div>
                    <div>
                        <label>¿Ambos padres firmarán la matrícula y el contrato de Cooperación Educativa?</label>
                        <radio>
                            <input type="radio" name="padresFirmanRadio" id="padresFirmanSi" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="padresFirmanRadio" id="padresFirmanNo" value="N" required/>No
                            <br/>
                            <br/>
                            <input placeholder="¿Quién?" name="nombreFirma" id="nombreFirma" type="text" autocomplete="on" maxlength="75" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                        </radio>
                    </div>
                    <div>
                        <label>¿Autoriza dar información verbal y escrita al Padre o Madre ausente?</label>
                        <radio>
                            <input type="radio" name="autorizaInformacionRadio" id="autorizaSi" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="autorizaInformacionRadio" id="autorizaNo" value="N" required/>No
                            <br/>
                            <br/>
                            <textarea cols="40" rows="5" placeholder="Explique" name="autorizaExplique" id="autorizaExplique" type="text" autocomplete="on" maxlength="200" disabled required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                        </radio>
                    </div>
                    <div>
                        <label>¿Qué miembro de la Comunidad Liceísta puede dar referencia de la familia que solicita admisión?</label>
                        <radio>
                            <input placeholder="Ingrese el nombre del miembro" name="miembroReferencia" id="miembroReferencia" type="text" autocomplete="on" maxlength="100"/>
                            <br/>
                            <textarea cols="40" rows="3" placeholder="Padres del Educando" name="padresReferencia" id="padresReferencia" type="text" maxlength="200" autocomplete="on" disabled required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                            <br/>
                            <input placeholder="Del Grado" name="gradoReferencia" id="gradoReferencia" type="text" autocomplete="on" maxlength="15" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                        </radio>
                    </div>
                    <div>
                        <label>¿Qué características del Liceo Campo David le han motivado para solicitar esta admisión?</label>
                        <textarea cols="40" rows="7" placeholder="Ingrese las características que lo motivaron" name="caracteristicas" id="caracteristicas" type="text" maxlength="300" autocomplete="on" required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                    </div>
                    <br/>
                    <table id="institucionesTabla" border="0" class="asociados">
                        <tr>
                            <th>Nombre de la Institución</th>
                            <th>Grado</th>
                            <th>Año en que lo cursó</th>
                            <th>Teléfono</th>
                            <th><input name="adicionarInstitucion" id="adicionarInstitucion" type="button" value="Añadir Institución" class="Button"></th>
                        </tr>
                    </table>
                    <br/>
                    <div>
                        <label>¿En pocas palabras, cuál es la Visión Educativa ideal para su Hijo(a)?</label>
                        <textarea cols="40" rows="5" placeholder="Ingrese la visión educativa" name="visionEducativa" id="visionEducativa" type="text" maxlength="200" autocomplete="on" required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                    </div>

                    <div>
                        <label>¿Posee escolaridad anterior?</label>
                        <radio>
                            <input type="radio" name="escolaridadRadio" id="escolaridadSi" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="escolaridadRadio" id="escolaridadNo" value="N" required/>No
                        </radio>
                    </div>
                    <div>
                        <label>Paz y Salvo a la fecha:</label>
                        <input placeholder="Ingrese el paz y salvo" name="pazSalvo" id="pazSalvo" type="file" accept="media_type" autocomplete="off" required disabled/>
                    </div>
                    <div>
                        <label>Último Boletín:</label>
                        <input placeholder="Ingrese el último boletín" name="ultimoBoletin" id="ultimoBoletin" type="file" accept="media_type" autocomplete="off" required disabled/>
                    </div>
                    
                    <div>
                        <label>Certificado de Ingresos:</label>
                        <input placeholder="Ingrese el certificado de ingresos" name="certificadoIngresos" id="certificadoIngresos" type="file" accept="media_type" autocomplete="off"/>
                    </div>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <div align="center">
                        <input name="registrar" id="submit" type="submit" value="Registrar y Finalizar" class="Button2"/>
                        <!--input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2"-->
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin utilizado</h4>
                    <?php
                    echo "<p>El Pin <b>$pin</b> ya ha sido utilizado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no encontrado o no utilizado</h4>
                <?php
                echo "<p>El Pin <b>$pin</b> no existe o aún no ha sido utilizado.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
        <!-- FORMULARIO -->
        <script src="view/js/formularioAdicional.js" type="text/javascript"></script>
    </body>
</html>