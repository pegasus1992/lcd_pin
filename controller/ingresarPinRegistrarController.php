<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';

if (isset($_POST['register'])) {
    $pin = $_POST['pin'];
    redirigir("formulario.php?pin=$pin");
} else if (isset($_POST['registerPadres'])) {
    $pin = $_POST['pin'];
    redirigir("formularioPadres.php?pin=$pin");
} else if (isset($_POST['registerAdicional'])) {
    $pin = $_POST['pin'];
    redirigir("formularioAdicional.php?pin=$pin");
}