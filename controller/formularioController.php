<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';

if (isset($_POST['registrar'])) {
    $pin = $_POST['pin'];

    // Tamano maximo paquete BD
    $sqlTam = "show variables like 'max_allowed_packet'";
    $resultTam = getResultSet($sqlTam);
    $filaTam = mysqli_fetch_array($resultTam);
    $tamanio = array(
        "name" => $filaTam['Variable_name'],
        "value" => $filaTam['Value']
    );

    /*     * ******************* DATOS ******************** */

    $fotoNino = $_FILES['fotoNino'];
    $primerApellidoNino = $_POST['primerApellidoNino'];
    $segundoApellidoNino = $_POST['segundoApellidoNino'];
    $nombresNino = $_POST['nombresNino'];
    $lugarNacimientoNino = $_POST['lugarNacimientoNino'];
    $fechaNacimientoNino = $_POST['fechaNacimientoNino'];
    $edadNino = $_POST['edadNino'];
    $tidNino = $_POST['tidNino'];
    $nidNino = $_POST['nidNino'];
    $nacionalidadNino = $_POST['nacionalidadNino'];
    $barrioNino = $_POST['barrioNino'];
    $telefonoNino = $_POST['telefonoNino'];
    $gradoDeseaNino = $_POST['gradoDeseaNino'];
    $gradoActualNino = $_POST['gradoActualNino'];

    $grupoSanguineo = $_POST['grupoSanguineoNino'];
    $rh = $_POST['rhNino'];
    $estrato = $_POST['estratoNino'];
    $eps = $_POST['epsNino'];
    $lugarExpedicion = $_POST['lugarExpedicionNino'];

    // Direccion del Nino
    $tipoDireccion = $_POST['tipoDireccion'] . " ";
    $numTipoDireccion = $_POST['numTipoDireccion'] . " ";
    $az1 = $_POST['az1'] . " ";
    $bisDireccion = null;
    $az2 = null;
    if (isset($_REQUEST['bisDireccion'])) {
        $bisDireccion = $_REQUEST['bisDireccion'] . " ";
        $az2 = $_POST['az1'] . " ";
    }
    $se1 = $_POST['se1'] . " ";
    $numDireccion2 = $_POST['numDireccion2'];
    $adicionDireccion = $numDireccion2 != null && $numDireccion2 != "" ? "- $numDireccion2 " : " ";
    $numDireccion = "NO. " . $_POST['numDireccion'] . " ";
    $az3 = $_POST['az3'] . " ";
    $se2 = $_POST['se2'] . " ";
    $bis = $bisDireccion != null && $az2 != null ? $bisDireccion . $az2 : "";
    $interiorDireccion = $_POST['intDireccion'];
    $adicionIntDireccion = $interiorDireccion != null && $interiorDireccion != "" ? "INT $interiorDireccion " : " ";
    $direccionNino = $tipoDireccion . $numTipoDireccion . $az1 . $bis . $se1 . $numDireccion . $az3 . $adicionDireccion . $se2 . $adicionIntDireccion;

    $conocidoEstudia = null;
    $parienteEstudia = $_POST['conocidoRadio'];
    if ($parienteEstudia == "Otro") {
        $conocidoEstudia = $_POST['conocidoNino'];
    } else {
        $conocidoEstudia = $parienteEstudia;
    }

    $impedimentoFisico = $_POST['impedimentoRadio'];
    $nombreImpedimentoFisico = null;
    if ($impedimentoFisico == "S") {
        $nombreImpedimentoFisico = $_POST['nombreImpedimentoNino'];
    }

    $tenidoAyuda = $_POST['tenidoAyudaRadio'];
    $nombreAyuda = null;
    if ($tenidoAyuda == "S") {
        $nombreAyuda = $_POST['nombreAyudaNino'];
    }

    $enfermedad = $_POST['enfermedadRadio'];
    $nombreEnfermedad = null;
    if ($enfermedad == "S") {
        $nombreEnfermedad = $_POST['enfermedadNino'];
    }

    $medicamentoEspecial = $_POST['medicamentoEspecialRadio'];
    $nombreMedicamentoEspecial = null;
    if ($medicamentoEspecial == "S") {
        $nombreMedicamentoEspecial = $_POST['medicamentoEspecialNino'];
    }

    $transporteEscolar = $_POST['transporteEscolarRadio'];
    $tipoRuta = null;
    $direccionRuta = null;
    $barrioRuta = null;
    if ($transporteEscolar == "S") {
        $tipoRuta = $_POST['tipoRutaRadio'];
        $barrioRuta = $_POST['barrioRutaNino'];

        // Direccion de la ruta
        $tipoDireccionRuta = $_POST['tipoDireccionRuta'] . " ";
        $numTipoDireccionRuta = $_POST['numTipoDireccionRuta'] . " ";
        $az1Ruta = $_POST['az1Ruta'] . " ";
        $bisDireccionRuta = null;
        $az2Ruta = null;
        if (isset($_REQUEST['bisDireccionRuta'])) {
            $bisDireccionRuta = $_REQUEST['bisDireccionRuta'] . " ";
            $az2Ruta = $_POST['az1Ruta'] . " ";
        }
        $se1Ruta = $_POST['se1Ruta'] . " ";
        $numDireccion2Ruta = $_POST['numDireccion2Ruta'];
        $adicionDireccionRuta = $numDireccion2Ruta != null && $numDireccion2Ruta != "" ? "- $numDireccion2Ruta " : " ";
        $numDireccionRuta = "NO. " . $_POST['numDireccionRuta'] . " ";
        $az3Ruta = $_POST['az3Ruta'] . " ";
        $se2Ruta = $_POST['se2Ruta'] . " ";
        $bisRuta = $bisDireccionRuta != null && $az2Ruta != null ? $bisDireccionRuta . $az2Ruta : "";
        $direccionRuta = $tipoDireccionRuta . $numTipoDireccionRuta . $az1Ruta . $bisRuta . $se1Ruta . $numDireccionRuta . $az3Ruta . $adicionDireccionRuta . $se2Ruta;
    }

    $restauranteEscolar = $_POST['restauranteEscolarRadio'];
    $manoDominante = $_POST['manoDominanteRadio'];

    $usaGafas = $_POST['usaGafasRadio'];
    $deficienciaVisual = null;
    if ($usaGafas == "S") {
        $deficienciaVisual = $_POST['deficienciaVisualNino'];
    }

    $fractura = $_POST['fracturaRadio'];
    $detalleFractura = null;
    if ($fractura == "S") {
        $detalleFractura = $_POST['detalleFracturaNino'];
    }

    $aspectosEspeciales = $_POST['aspectosEspecialesNino'];

    $sqls = array();

    /*     * ******************* PROCESANDO DATOS ******************** */

    $idFotoNino = null;

    // Datos Imagen
    {
        $imagePath = $fotoNino['tmp_name'];
        $imageSize = $fotoNino['size'];
        $imageName = $fotoNino['name'];
        $imageType = $fotoNino['type'];

        if ($imageSize < $tamanio["value"]) {
            $fp = fopen($imagePath, 'r');
            $datosImagen = null;
            if ($fp) {
                $datosImagen = fread($fp, $imageSize); // Cargar la imagen
                fclose($fp);
            }
            if ($datosImagen != null) {
                $datosImagen = base64_encode($datosImagen);
                $id = rand();
                $sqls[] = "INSERT INTO Foto (id, imagen, tipo) VALUES ($id, '$datosImagen', '$imageType')";
                $idFotoNino = $id;
            }
        }
    }

    $anio = substr($pin, 0, 4);
    $numero = substr($pin, 4);

    $inserts = array();
    $values = array();

    $inserts[] = "anioPin";
    $values[] = "$anio";

    $inserts[] = "numeroPin";
    $values[] = "'$numero'";

    $inserts[] = "idFoto";
    $values[] = "$idFotoNino";

    $inserts[] = "primerApellido";
    $values[] = "'$primerApellidoNino'";

    $inserts[] = "segundoApellido";
    $values[] = "'$segundoApellidoNino'";

    $inserts[] = "nombres";
    $values[] = "'$nombresNino'";

    $inserts[] = "lugarNacimiento";
    $values[] = "'$lugarNacimientoNino'";

    $inserts[] = "fechaNacimiento";
    $values[] = "'$fechaNacimientoNino'";

    $inserts[] = "edadCumplida";
    $values[] = "$edadNino";

    $inserts[] = "tid";
    $values[] = "'$tidNino'";

    $inserts[] = "nid";
    $values[] = "'$nidNino'";

    $inserts[] = "nacionalidad";
    $values[] = "'$nacionalidadNino'";

    $inserts[] = "direccionResidencia";
    $values[] = "'$direccionNino'";

    $inserts[] = "barrio";
    $values[] = "'$barrioNino'";

    $inserts[] = "telefono";
    $values[] = "'$telefonoNino'";

    $inserts[] = "gradoIngresar";
    $values[] = "'$gradoDeseaNino'";

    $inserts[] = "gradoActual";
    $values[] = "'$gradoActualNino'";

    $inserts[] = "conocidoEstudia";
    $values[] = "'$conocidoEstudia'";

    $inserts[] = "impedimentoFisico";
    $values[] = "'$impedimentoFisico'";

    if ($nombreImpedimentoFisico != null) {
        $inserts[] = "nombreImpedimentoFisico";
        $values[] = "'$nombreImpedimentoFisico'";
    }

    $inserts[] = "tenidoAyuda";
    $values[] = "'$tenidoAyuda'";

    if ($nombreAyuda != null) {
        $inserts[] = "nombreAyuda";
        $values[] = "'$nombreAyuda'";
    }

    $inserts[] = "enfermedad";
    $values[] = "'$enfermedad'";

    if ($nombreEnfermedad != null) {
        $inserts[] = "nombreEnfermedad";
        $values[] = "'$nombreEnfermedad'";
    }

    $inserts[] = "medicamentoEspecial";
    $values[] = "'$medicamentoEspecial'";

    if ($nombreMedicamentoEspecial != null) {
        $inserts[] = "nombreMedicamentoEspecial";
        $values[] = "'$nombreMedicamentoEspecial'";
    }

    $inserts[] = "transporteEscolar";
    $values[] = "'$transporteEscolar'";

    if ($tipoRuta != null) {
        $inserts[] = "tipoRuta";
        $values[] = "'$tipoRuta'";
    }

    if ($direccionRuta != null) {
        $inserts[] = "direccionRuta";
        $values[] = "'$direccionRuta'";
    }

    if ($barrioRuta != null) {
        $inserts[] = "barrioRuta";
        $values[] = "'$barrioRuta'";
    }

    $inserts[] = "restauranteEscolar";
    $values[] = "'$restauranteEscolar'";

    $inserts[] = "manoDominante";
    $values[] = "'$manoDominante'";

    $inserts[] = "usaGafas";
    $values[] = "'$usaGafas'";

    if ($deficienciaVisual != null) {
        $inserts[] = "deficienciaVisual";
        $values[] = "'$deficienciaVisual'";
    }

    $inserts[] = "fractura";
    $values[] = "'$fractura'";

    if ($detalleFractura != null) {
        $inserts[] = "detalleFractura";
        $values[] = "'$detalleFractura'";
    }
    if ($aspectosEspeciales != null) {
        $inserts[] = "aspectosEspeciales";
        $values[] = "'$aspectosEspeciales'";
    }

    $inserts[] = "grupoSanguineo";
    $values[] = "'$grupoSanguineo'";

    $inserts[] = "rh";
    $values[] = "'$rh'";

    $inserts[] = "estrato";
    $values[] = "$estrato";

    $inserts[] = "eps";
    $values[] = "'$eps'";

    $inserts[] = "lugarExpedicionDocumento";
    $values[] = "'$lugarExpedicion'";

    $insertInto = "";
    for ($i = 0; $i < sizeof($inserts); $i++) {
        $insertInto .= $inserts[$i];
        if ($i < sizeof($inserts) - 1) {
            $insertInto .= ", ";
        }
    }
    $insertValues = "";
    for ($i = 0; $i < sizeof($values); $i++) {
        $insertValues .= $values[$i];
        if ($i < sizeof($values) - 1) {
            $insertValues .= ", ";
        }
    }

    /*     * ******************* INSERTANDO DATOS ******************** */

    $sqls[] = "INSERT INTO Formulario ($insertInto) VALUES ($insertValues)";
    $sqls[] = "UPDATE Pin SET utilizado = 'S' WHERE anio = '$anio' AND numero = '$numero' ";

    if ($idFotoNino != null) {
        if (executeMultiQuery($sqls)) {
            mostrarMensaje("Información del Educando registrada correctamente");
            redirigir("formularioPadres.php?pin=$pin");
        }
    }
}