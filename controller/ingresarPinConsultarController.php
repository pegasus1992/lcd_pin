<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';

if (isset($_POST['register'])) {
    $pin = $_POST['pin'];
    redirigir("mostrarFormularioCompleto.php?pin=$pin");
}