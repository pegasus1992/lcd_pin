<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';
include_once './model/FormularioAdicional.php';

if (isset($_POST['registrar'])) {
    $id = rand();
    $pin = $_POST['pin'];

    // Tamano maximo paquete BD
    $sqlTam = "show variables like 'max_allowed_packet'";
    $resultTam = getResultSet($sqlTam);
    $filaTam = mysqli_fetch_array($resultTam);
    $tamanio = array(
        "name" => $filaTam['Variable_name'],
        "value" => $filaTam['Value']
    );

    /*     * ******************* DATOS ******************** */

    $hermanos = null;
    if (isset($_POST['hermanos'])) {
        $hermanosTabla = $_POST['hermanos'];
        $hermanos = table2Array($hermanosTabla, 5);
    }

    $instituciones = null;
    if (isset($_POST['instituciones'])) {
        $institucionesTabla = $_POST['instituciones'];
        $instituciones = table2Array($institucionesTabla, 4);
    }

    $tipoUnion = null;
    $tipoUnionRadio = $_POST['tipoUnionRadio'];
    $separados = null;
    if ($tipoUnionRadio == "Otro") {
        $tipoUnion = $_POST['estadoCivil'];
    } else {
        $tipoUnion = $tipoUnionRadio;
        if ($tipoUnionRadio == "Separados") {
            $separados = $_POST['separados'];
        }
    }

    $adoptado = $_POST['adoptadoRadio'];
    $edadAdoptado = null;
    if ($adoptado == "S") {
        $edadAdoptado = $_POST['edadAdoptado'];
    }

    $telefonoMadre = $_POST['telefonoMadre'];
    // Direccion de la Madre
    $tipoDireccionMadre = $_POST['tipoDireccionMadre'] . " ";
    $numTipoDireccionMadre = $_POST['numTipoDireccionMadre'] . " ";
    $az1Madre = $_POST['az1Madre'] . " ";
    $bisDireccionMadre = null;
    $az2Madre = null;
    if (isset($_REQUEST['bisDireccionMadre'])) {
        $bisDireccionMadre = $_REQUEST['bisDireccionMadre'] . " ";
        $az2Madre = $_POST['az1'] . " ";
    }
    $se1Madre = $_POST['se1Madre'] . " ";
    $numDireccion2Madre = $_POST['numDireccion2Madre'];
    $adicionDireccionMadre = $numDireccion2Madre != null && $numDireccion2Madre != "" ? "- $numDireccion2Madre " : " ";
    $numDireccionMadre = "NO. " . $_POST['numDireccionMadre'] . " ";
    $az3Madre = $_POST['az3Madre'] . " ";
    $se2Madre = $_POST['se2Madre'] . " ";
    $bisMadre = $bisDireccionMadre != null && $az2Madre != null ? $bisDireccionMadre . $az2Madre : "";
    $interiorDireccionMadre = $_POST['intDireccionMadre'];
    $adicionIntDireccionMadre = $interiorDireccionMadre != null && $interiorDireccionMadre != "" ? "INT $interiorDireccionMadre " : " ";
    $direccionMadre = $tipoDireccionMadre . $numTipoDireccionMadre . $az1Madre . $bisMadre . $se1Madre . $numDireccionMadre . $az3Madre . $adicionDireccionMadre . $se2Madre . $adicionIntDireccionMadre;

    $telefonoPadre = $_POST['telefonoPadre'];
    // Direccion del Padre
    $tipoDireccionPadre = $_POST['tipoDireccionPadre'] . " ";
    $numTipoDireccionPadre = $_POST['numTipoDireccionPadre'] . " ";
    $az1Padre = $_POST['az1Padre'] . " ";
    $bisDireccionPadre = null;
    $az2Padre = null;
    if (isset($_REQUEST['bisDireccionPadre'])) {
        $bisDireccionPadre = $_REQUEST['bisDireccionPadre'] . " ";
        $az2Padre = $_POST['az1'] . " ";
    }
    $se1Padre = $_POST['se1Padre'] . " ";
    $numDireccion2Padre = $_POST['numDireccion2Padre'];
    $adicionDireccionPadre = $numDireccion2Padre != null && $numDireccion2Padre != "" ? "- $numDireccion2Padre " : " ";
    $numDireccionPadre = "NO. " . $_POST['numDireccionPadre'] . " ";
    $az3Padre = $_POST['az3Padre'] . " ";
    $se2Padre = $_POST['se2Padre'] . " ";
    $bisPadre = $bisDireccionPadre != null && $az2Padre != null ? $bisDireccionPadre . $az2Padre : "";
    $interiorDireccionPadre = $_POST['intDireccionPadre'];
    $adicionIntDireccionPadre = $interiorDireccionPadre != null && $interiorDireccionPadre != "" ? "INT $interiorDireccionPadre " : " ";
    $direccionPadre = $tipoDireccionPadre . $numTipoDireccionPadre . $az1Padre . $bisPadre . $se1Padre . $numDireccionPadre . $az3Padre . $adicionDireccionPadre . $se2Padre . $adicionIntDireccionPadre;

    $nombreAcudiente1 = $_POST['nombreAcudiente1'];
    $telefonoAcudiente1 = $_POST['telefonoAcudiente1'];
    // Direccion del Acudiente 1
    $tipoDireccionAcudiente1 = $_POST['tipoDireccionAcudiente1'] . " ";
    $numTipoDireccionAcudiente1 = $_POST['numTipoDireccionAcudiente1'] . " ";
    $az1Acudiente1 = $_POST['az1Acudiente1'] . " ";
    $bisDireccionAcudiente1 = null;
    $az2Acudiente1 = null;
    if (isset($_REQUEST['bisDireccionAcudiente1'])) {
        $bisDireccionAcudiente1 = $_REQUEST['bisDireccionAcudiente1'] . " ";
        $az2Acudiente1 = $_POST['az1'] . " ";
    }
    $se1Acudiente1 = $_POST['se1Acudiente1'] . " ";
    $numDireccion2Acudiente1 = $_POST['numDireccion2Acudiente1'];
    $adicionDireccionAcudiente1 = $numDireccion2Acudiente1 != null && $numDireccion2Acudiente1 != "" ? "- $numDireccion2Acudiente1 " : " ";
    $numDireccionAcudiente1 = "NO. " . $_POST['numDireccionAcudiente1'] . " ";
    $az3Acudiente1 = $_POST['az3Acudiente1'] . " ";
    $se2Acudiente1 = $_POST['se2Acudiente1'] . " ";
    $bisAcudiente1 = $bisDireccionAcudiente1 != null && $az2Acudiente1 != null ? $bisDireccionAcudiente1 . $az2Acudiente1 : "";
    $interiorDireccionAcudiente1 = $_POST['intDireccionAcudiente1'];
    $adicionIntDireccionAcudiente1 = $interiorDireccionAcudiente1 != null && $interiorDireccionAcudiente1 != "" ? "INT $interiorDireccionAcudiente1 " : " ";
    $direccionAcudiente1 = $tipoDireccionAcudiente1 . $numTipoDireccionAcudiente1 . $az1Acudiente1 . $bisAcudiente1 . $se1Acudiente1 . $numDireccionAcudiente1 . $az3Acudiente1 . $adicionDireccionAcudiente1 . $se2Acudiente1 . $adicionIntDireccionAcudiente1;

    $nombreAcudiente2 = $_POST['nombreAcudiente2'];
    $telefonoAcudiente2 = $_POST['telefonoAcudiente2'];
    // Direccion del Acudiente 2
    $tipoDireccionAcudiente2 = $_POST['tipoDireccionAcudiente2'] . " ";
    $numTipoDireccionAcudiente2 = $_POST['numTipoDireccionAcudiente2'] . " ";
    $az1Acudiente2 = $_POST['az1Acudiente2'] . " ";
    $bisDireccionAcudiente2 = null;
    $az2Acudiente2 = null;
    if (isset($_REQUEST['bisDireccionAcudiente2'])) {
        $bisDireccionAcudiente2 = $_REQUEST['bisDireccionAcudiente2'] . " ";
        $az2Acudiente2 = $_POST['az1'] . " ";
    }
    $se1Acudiente2 = $_POST['se1Acudiente2'] . " ";
    $numDireccion2Acudiente2 = $_POST['numDireccion2Acudiente2'];
    $adicionDireccionAcudiente2 = $numDireccion2Acudiente2 != null && $numDireccion2Acudiente2 != "" ? "- $numDireccion2Acudiente2 " : " ";
    $numDireccionAcudiente2 = "NO. " . $_POST['numDireccionAcudiente2'] . " ";
    $az3Acudiente2 = $_POST['az3Acudiente2'] . " ";
    $se2Acudiente2 = $_POST['se2Acudiente2'] . " ";
    $bisAcudiente2 = $bisDireccionAcudiente2 != null && $az2Acudiente2 != null ? $bisDireccionAcudiente2 . $az2Acudiente2 : "";
    $interiorDireccionAcudiente2 = $_POST['intDireccionAcudiente2'];
    $adicionIntDireccionAcudiente2 = $interiorDireccionAcudiente2 != null && $interiorDireccionAcudiente2 != "" ? "INT $interiorDireccionAcudiente2 " : " ";
    $direccionAcudiente2 = $tipoDireccionAcudiente2 . $numTipoDireccionAcudiente2 . $az1Acudiente2 . $bisAcudiente2 . $se1Acudiente2 . $numDireccionAcudiente2 . $az3Acudiente2 . $adicionDireccionAcudiente2 . $se2Acudiente2 . $adicionIntDireccionAcudiente2;

    $direccionTelefonoEmergencia = "DIRECCION MADRE: $direccionMadre - TELEFONO MADRE: $telefonoMadre - DIRECCION PADRE: $direccionPadre - TELEFONO PADRE: $telefonoPadre";
    $direccionTelefonoFamiliar = "ACUDIENTE 1: $nombreAcudiente1 - DIRECCION ACUDIENTE 1: $direccionAcudiente1 - TELEFONO ACUDIENTE 1: $telefonoAcudiente1 - ACUDIENTE 2: $nombreAcudiente2 - DIRECCION ACUDIENTE 2: $direccionAcudiente2 - TELEFONO ACUDIENTE 2: $telefonoAcudiente2";

    $personaRespondeRadio = $_POST['personaRespondeRadio'];
    $personaResponde = null;
    if ($personaRespondeRadio == "Otro") {
        $personaResponde = $_POST['personaResponde'];
    } else {
        $personaResponde = $personaRespondeRadio;
    }

    $padresFirman = $_POST['padresFirmanRadio'];
    $nombreFirma = null;
    $autorizaInformacion = null;
    $autorizaExplique = null;
    if ($padresFirman == "N") {
        $nombreFirma = $_POST['nombreFirma'];
        $autorizaInformacion = $_POST['autorizaInformacionRadio'];
        $autorizaExplique = $_POST['autorizaExplique'];
    }

    $miembroReferencia = null;
    $padresReferencia = null;
    $gradoReferencia = null;
    $firmaReferencia = "";
    if ($_POST['miembroReferencia'] != null) {
        $miembroReferencia = $_POST['miembroReferencia'];
        if ($_POST['padresReferencia'] != null) {
            $padresReferencia = $_POST['padresReferencia'];
        }
        if ($_POST['gradoReferencia'] != null) {
            $gradoReferencia = $_POST['gradoReferencia'];
        }
    }

    $caracteristicas = $_POST['caracteristicas'];
    $visionEducativa = $_POST['visionEducativa'];

    $sqls = array();

    $escolaridadAnterior = $_POST['escolaridadRadio'];
    $idPazSalvo = null;
    $idUltimoBoletin = null;
    if ($escolaridadAnterior == "S") {
        $pazSalvo = $_FILES['pazSalvo'];
        // Paz y Salvo
        {
            $pazSalvoPath = $pazSalvo['tmp_name'];
            $pazSalvoSize = $pazSalvo['size'];
            $pazSalvoName = $pazSalvo['name'];
            $pazSalvoType = $pazSalvo['type'];

            if ($pazSalvoSize < $tamanio["value"]) {
                $pazSalvoContent = addslashes(fread(fopen($pazSalvoPath, "rb"), $pazSalvoSize));
                if ($pazSalvoContent != null) {
                    $idPazSalvo = rand();
                    $sqls[] = "INSERT INTO Archivo (id, binario, nombre, peso, tipo) "
                            . "VALUES ($idPazSalvo, '$pazSalvoContent', '$pazSalvoName', '$pazSalvoSize', '$pazSalvoType')";
                }
            }
        }

        $ultimoBoletin = $_FILES['ultimoBoletin'];
        //Ultimo Boletin
        {
            $ultimoBoletinPath = $ultimoBoletin['tmp_name'];
            $ultimoBoletinSize = $ultimoBoletin['size'];
            $ultimoBoletinName = $ultimoBoletin['name'];
            $ultimoBoletinType = $ultimoBoletin['type'];

            if ($ultimoBoletinSize < $tamanio["value"]) {
                $ultimoBoletinContent = addslashes(fread(fopen($ultimoBoletinPath, "rb"), $ultimoBoletinSize));
                if ($ultimoBoletinContent != null) {
                    $idUltimoBoletin = rand();
                    $sqls[] = "INSERT INTO Archivo (id, binario, nombre, peso, tipo) "
                            . "VALUES ($idUltimoBoletin, '$ultimoBoletinContent', '$ultimoBoletinName', '$ultimoBoletinSize', '$ultimoBoletinType')";
                }
            }
        }
    }

    $idCertificadoIngresos = null;
    $certificadoIngresos = $_FILES['certificadoIngresos'];
    // Certificado de Ingresos
    {
        $certificadoPath = $certificadoIngresos['tmp_name'];
        $certificadoSize = $certificadoIngresos['size'];
        $certificadoName = $certificadoIngresos['name'];
        $certificadoType = $certificadoIngresos['type'];

        if ($certificadoContent < $tamanio["value"]) {
            $certificadoContent = addslashes(fread(fopen($certificadoPath, "rb"), $certificadoSize));
            if ($certificadoContent != null) {
                $idCertificadoIngresos = rand();
                $sqls[] = "INSERT INTO Archivo (id, binario, nombre, peso, tipo) "
                        . "VALUES ($idCertificadoIngresos, '$certificadoContent', '$certificadoName', '$certificadoSize', '$certificadoType')";
            }
        }
    }

    /*     * ******************* PROCESANDO DATOS ******************** */

    $anio = substr($pin, 0, 4);
    $numero = substr($pin, 4);

    $inserts = array();
    $values = array();

    $inserts[] = "id";
    $values[] = "$id";

    $inserts[] = "anioPin";
    $values[] = "$anio";

    $inserts[] = "numeroPin";
    $values[] = "'$numero'";

    $inserts[] = "tipoUnion";
    $values[] = "'$tipoUnion'";

    if ($separados != null) {
        $inserts[] = "separados";
        $values[] = "'$separados'";
    }

    $inserts[] = "adoptado";
    $values[] = "'$adoptado'";

    if ($edadAdoptado != null) {
        $inserts[] = "edadAdoptado";
        $values[] = "$edadAdoptado";
    }

    $inserts[] = "direccionTelefonoEmergencia";
    $values[] = "'$direccionTelefonoEmergencia'";

    $inserts[] = "direccionTelefonoFamiliar";
    $values[] = "'$direccionTelefonoFamiliar'";

    $inserts[] = "personaResponde";
    $values[] = "'$personaResponde'";

    $inserts[] = "padresFirman";
    $values[] = "'$padresFirman'";

    if ($nombreFirma != null) {
        $inserts[] = "nombreFirma";
        $values[] = "'$nombreFirma'";
    }

    if ($autorizaInformacion != null) {
        $inserts[] = "autorizaInformacion";
        $values[] = "'$autorizaInformacion'";
    }

    if ($autorizaExplique != null) {
        $inserts[] = "autorizaExplique";
        $values[] = "'$autorizaExplique'";
    }

    if ($miembroReferencia != null) {
        $inserts[] = "miembroReferencia";
        $values[] = "'$miembroReferencia'";
    }

    if ($padresReferencia != null) {
        $inserts[] = "padresReferencia";
        $values[] = "'$padresReferencia'";
    }

    if ($gradoReferencia != null) {
        $inserts[] = "gradoReferencia";
        $values[] = "'$gradoReferencia'";
    }

    if ($firmaReferencia != null) {
        $inserts[] = "firmaReferencia";
        $values[] = "'$firmaReferencia'";
    }

    $inserts[] = "caracteristicas";
    $values[] = "'$caracteristicas'";

    $inserts[] = "visionEducativa";
    $values[] = "'$visionEducativa'";

    $inserts[] = "escolaridadAnterior";
    $values[] = "'$escolaridadAnterior'";

    if ($idPazSalvo != null) {
        $inserts[] = "pazSalvo";
        $values[] = "'$idPazSalvo'";
    }

    if ($idUltimoBoletin != null) {
        $inserts[] = "ultimoBoletin";
        $values[] = "'$idUltimoBoletin'";
    }

    if ($idCertificadoIngresos != null) {
        $inserts[] = "certificadoIngresos";
        $values[] = "'$idCertificadoIngresos'";
    }

    $insertInto = "";
    for ($i = 0; $i < sizeof($inserts); $i++) {
        $insertInto .= $inserts[$i];
        if ($i < sizeof($inserts) - 1) {
            $insertInto .= ", ";
        }
    }
    $insertValues = "";
    for ($i = 0; $i < sizeof($values); $i++) {
        $insertValues .= $values[$i];
        if ($i < sizeof($values) - 1) {
            $insertValues .= ", ";
        }
    }

    /*     * ******************* INSERTANDO DATOS ******************** */
    $sqls[] = "INSERT INTO FormularioAdicional ($insertInto) VALUES ($insertValues)";

    if ($hermanos != null && sizeof($hermanos) > 0) {
        foreach ($hermanos as $hermano) {
            if ($hermano[0] != null && $hermano[0] != "" && $hermano[1] != null && $hermano[1] != "" && $hermano[2] != null && $hermano[2] != "" && $hermano[3] != null && $hermano[3] != "" && $hermano[4] != null && $hermano[4] != "") {
                $sqls[] = "INSERT INTO Hermano (nombres, apellidos, edad, colegio, grado, idFormulario) "
                        . "VALUES ('$hermano[0]', '$hermano[1]', $hermano[2], '$hermano[3]', '$hermano[4]', $id)";
            }
        }
    }

    if ($instituciones != null && sizeof($instituciones) > 0) {
        foreach ($instituciones as $institucion) {
            if ($institucion[0] != null && $institucion[0] != "" && $institucion[1] != null && $institucion[1] != "" && $institucion[2] != null && $institucion[2] != "" && $institucion[3] != null && $institucion[3] != "") {
                $sqls[] = "INSERT INTO Institucion (nombre, grado, anio, telefono, idFormulario) "
                        . "VALUES ('$institucion[0]', '$institucion[1]', $institucion[2], '$institucion[3]', $id)";
            }
        }
    }

    if (($escolaridadAnterior == "N" || ($escolaridadAnterior == "S" && $idPazSalvo != null && $idUltimoBoletin != null)) && $idCertificadoIngresos != null) {
        if (executeMultiQuery($sqls)) {
            mostrarMensaje("Información Adicional registrada correctamente");
            redirigir("formularioRecibo.php?pin=$pin");
        }
    }
}

/**
 * Obtiene los datos de una tabla HTML y los convierte en un Array de PHP.
 * @param type $table <p>Tabla HTML.</p>
 * @param type $numCols <p>Numero de columnas de la tabla.</p>
 * @return type
 */
function table2Array($table, $numCols) {
    $response = array();
    $row = null;
    for ($i = 0; $i < count($table); $i++) {
        $remainder = $i % $numCols;
        if ($remainder == 0) {
            $row = array();
        }
        $row[] = $table[$i];
        if ($remainder == $numCols - 1) {
            $response[] = $row;
        }
    }
    return $response;
}
