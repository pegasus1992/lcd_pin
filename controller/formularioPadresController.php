<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';
include_once './model/FormularioPadres.php';

if (isset($_POST['registrar'])) {
    $pin = $_POST['pin'];
    
    // Tamano maximo paquete BD
    $sqlTam = "show variables like 'max_allowed_packet'";
    $resultTam = getResultSet($sqlTam);
    $filaTam = mysqli_fetch_array($resultTam);
    $tamanio = array(
        "name" => $filaTam['Variable_name'],
        "value" => $filaTam['Value']
    );

    /*     * ******************* DATOS DE LA MADRE ******************** */

    $fotoMadre = $_FILES['fotoMadre'];
    $primerApellidoMadre = $_POST['primerApellidoMadre'];
    $segundoApellidoMadre = $_POST['segundoApellidoMadre'];
    $nombresMadre = $_POST['nombresMadre'];
    $lugarNacimientoMadre = $_POST['lugarNacimientoMadre'];
    $fechaNacimientoMadre = $_POST['fechaNacimientoMadre'];
    $edadMadre = $_POST['edadMadre'];
    $tidMadre = $_POST['tidMadre'];
    $nidMadre = $_POST['nidMadre'];
    $profesionMadre = $_POST['profesionMadre'];

    $esIndependienteMadre = $_POST['independienteRadioMadre'];
    $empresaMadre = null;
    $celularMadre = null;
    $telefonoMadre = null;
    $extensionMadre = null;
    $cargoMadre = null;
    $ingresosMadre = null;
    $trabajoMadre = null;
    if ($esIndependienteMadre == "S") {
        $empresaMadre = "Independiente";
        $trabajoMadre = $_POST['trabajoMadre'];
    } else {
        $empresaMadre = $_POST['empresaMadre'];
        $cargoMadre = $_POST['cargoMadre'];
    }
    if ($_POST['celularMadre'] != null) {
        $celularMadre = $_POST['celularMadre'];
    }
    if ($_POST['telefonoMadre'] != null) {
        $telefonoMadre = $_POST['telefonoMadre'];
    }
    if ($_POST['extensionMadre'] != null) {
        $extensionMadre = $_POST['extensionMadre'];
    }
    $ingresosMadre = $_POST['ingresosMadre'];

    $estudiosPrimariosMadre = null;
    $estudiosSecundariosMadre = null;
    $estudiosUniversitariosMadre = null;
    $estudiosOtrosMadre = null;
    if ($_POST['estudiosPrimariosMadre'] != null) {
        $estudiosPrimariosMadre = $_POST['estudiosPrimariosMadre'];
    }
    if ($_POST['estudiosSecundariosMadre'] != null) {
        $estudiosSecundariosMadre = $_POST['estudiosSecundariosMadre'];
    }
    if ($_POST['estudiosUniversitariosMadre'] != null) {
        $estudiosUniversitariosMadre = $_POST['estudiosUniversitariosMadre'];
    }
    if ($_POST['estudiosOtrosMadre'] != null) {
        $estudiosOtrosMadre = $_POST['estudiosOtrosMadre'];
    }

    $resideEnRadioMadre = $_POST['resideEnRadioMadre'];
    $tipoResidenciaRadioMadre = $_POST['tipoResidenciaRadioMadre'];
    $correoMadre = $_POST['correoMadre'];
    $interesesMadre = $_POST['interesesMadre'];

    /*     * ******************* DATOS DEL PADRE ******************** */

    $fotoPadre = $_FILES['fotoPadre'];
    $primerApellidoPadre = $_POST['primerApellidoPadre'];
    $segundoApellidoPadre = $_POST['segundoApellidoPadre'];
    $nombresPadre = $_POST['nombresPadre'];
    $lugarNacimientoPadre = $_POST['lugarNacimientoPadre'];
    $fechaNacimientoPadre = $_POST['fechaNacimientoPadre'];
    $edadPadre = $_POST['edadPadre'];
    $tidPadre = $_POST['tidPadre'];
    $nidPadre = $_POST['nidPadre'];
    $profesionPadre = $_POST['profesionPadre'];

    $esIndependientePadre = $_POST['independienteRadioPadre'];
    $empresaPadre = null;
    $celularPadre = null;
    $telefonoPadre = null;
    $extensionPadre = null;
    $cargoPadre = null;
    $ingresosPadre = null;
    $trabajoPadre = null;
    if ($esIndependientePadre == "S") {
        $empresaPadre = "Independiente";
        $trabajoPadre = $_POST['trabajoPadre'];
    } else {
        $empresaPadre = $_POST['empresaPadre'];
        $cargoPadre = $_POST['cargoPadre'];
    }
    if ($_POST['celularPadre'] != null) {
        $celularPadre = $_POST['celularPadre'];
    }
    if ($_POST['telefonoPadre'] != null) {
        $telefonoPadre = $_POST['telefonoPadre'];
    }
    if ($_POST['extensionPadre'] != null) {
        $extensionPadre = $_POST['extensionPadre'];
    }
    $ingresosPadre = $_POST['ingresosPadre'];

    $estudiosPrimariosPadre = null;
    $estudiosSecundariosPadre = null;
    $estudiosUniversitariosPadre = null;
    $estudiosOtrosPadre = null;
    if ($_POST['estudiosPrimariosPadre'] != null) {
        $estudiosPrimariosPadre = $_POST['estudiosPrimariosPadre'];
    }
    if ($_POST['estudiosSecundariosPadre'] != null) {
        $estudiosSecundariosPadre = $_POST['estudiosSecundariosPadre'];
    }
    if ($_POST['estudiosUniversitariosPadre'] != null) {
        $estudiosUniversitariosPadre = $_POST['estudiosUniversitariosPadre'];
    }
    if ($_POST['estudiosOtrosPadre'] != null) {
        $estudiosOtrosPadre = $_POST['estudiosOtrosPadre'];
    }

    $resideEnRadioPadre = $_POST['resideEnRadioPadre'];
    $tipoResidenciaRadioPadre = $_POST['tipoResidenciaRadioPadre'];
    $correoPadre = $_POST['correoPadre'];
    $interesesPadre = $_POST['interesesPadre'];

    $sqls = array();

    /*     * ******************* PROCESANDO DATOS DE LA MADRE ******************** */

    $insertsMadre = array();
    $valuesMadre = array();

    $idFotoMadre = null;
    // Datos Imagen
    {
        $imagePathMadre = $fotoMadre['tmp_name'];
        $imageSizeMadre = $fotoMadre['size'];
        $imageNameMadre = $fotoMadre['name'];
        $imageTypeMadre = $fotoMadre['type'];

        if($imageSizeMadre < $tamanio["value"]) {
            $fpMadre = fopen($imagePathMadre, 'r');
            $datosImagenMadre = null;
            if ($fpMadre) {
                $datosImagenMadre = fread($fpMadre, $imageSizeMadre); // Cargar la imagen
                fclose($fpMadre);
            }
            if ($datosImagenMadre != null) {
                $datosImagenMadre = base64_encode($datosImagenMadre);
                $id = rand();
                $sqls[] = "INSERT INTO Foto (id, imagen, tipo) VALUES ($id, '$datosImagenMadre', '$imageTypeMadre')";
                $idFotoMadre = $id;
            }
        } else {
            $idFotoMadre = null;
        }
    }

    $insertsMadre[] = "idFoto";
    $valuesMadre[] = "$idFotoMadre";

    $insertsMadre[] = "primerApellido";
    $valuesMadre[] = "'$primerApellidoMadre'";

    $insertsMadre[] = "segundoApellido";
    $valuesMadre[] = "'$segundoApellidoMadre'";

    $insertsMadre[] = "nombres";
    $valuesMadre[] = "'$nombresMadre'";

    $insertsMadre[] = "lugarNacimiento";
    $valuesMadre[] = "'$lugarNacimientoMadre'";

    $insertsMadre[] = "fechaNacimiento";
    $valuesMadre[] = "'$fechaNacimientoMadre'";

    $insertsMadre[] = "edadCumplida";
    $valuesMadre[] = "$edadMadre";

    $insertsMadre[] = "tid";
    $valuesMadre[] = "'$tidMadre'";

    $insertsMadre[] = "nid";
    $valuesMadre[] = "'$nidMadre'";

    $insertsMadre[] = "profesion";
    $valuesMadre[] = "'$profesionMadre'";

    $insertsMadre[] = "empresa";
    $valuesMadre[] = "'$empresaMadre'";

    if ($celularMadre != null) {
        $insertsMadre[] = "celular";
        $valuesMadre[] = "'$celularMadre'";
    }

    if ($telefonoMadre != null) {
        $insertsMadre[] = "telefono";
        $valuesMadre[] = "'$telefonoMadre'";
    }

    if ($extensionMadre != null) {
        $insertsMadre[] = "extension";
        $valuesMadre[] = "'$extensionMadre'";
    }

    if ($cargoMadre != null) {
        $insertsMadre[] = "cargo";
        $valuesMadre[] = "'$cargoMadre'";
    }

    if ($ingresosMadre != null) {
        $insertsMadre[] = "ingresosMensuales";
        $valuesMadre[] = "'$ingresosMadre'";
    }

    if ($trabajoMadre != null) {
        $insertsMadre[] = "trabajo";
        $valuesMadre[] = "'$trabajoMadre'";
    }

    if ($estudiosPrimariosMadre != null) {
        $insertsMadre[] = "estudiosPrimarios";
        $valuesMadre[] = "'$estudiosPrimariosMadre'";
    }

    if ($estudiosSecundariosMadre != null) {
        $insertsMadre[] = "estudiosSecundarios";
        $valuesMadre[] = "'$estudiosSecundariosMadre'";
    }

    if ($estudiosUniversitariosMadre != null) {
        $insertsMadre[] = "estudiosUniversitarios";
        $valuesMadre[] = "'$estudiosUniversitariosMadre'";
    }

    if ($estudiosOtrosMadre != null) {
        $insertsMadre[] = "estudiosOtros";
        $valuesMadre[] = "'$estudiosOtrosMadre'";
    }

    $insertsMadre[] = "resideEn";
    $valuesMadre[] = "'$resideEnRadioMadre'";

    $insertsMadre[] = "tipoResidencia";
    $valuesMadre[] = "'$tipoResidenciaRadioMadre'";

    $insertsMadre[] = "email";
    $valuesMadre[] = "'$correoMadre'";

    $insertsMadre[] = "intereses";
    $valuesMadre[] = "'$interesesMadre'";

    $insertIntoMadre = "";
    for ($i = 0; $i < sizeof($insertsMadre); $i++) {
        $insertIntoMadre .= $insertsMadre[$i];
        if ($i < sizeof($insertsMadre) - 1) {
            $insertIntoMadre .= ", ";
        }
    }
    $insertValuesMadre = "";
    for ($i = 0; $i < sizeof($valuesMadre); $i++) {
        $insertValuesMadre .= $valuesMadre[$i];
        if ($i < sizeof($valuesMadre) - 1) {
            $insertValuesMadre .= ", ";
        }
    }

    $sqls[] = "INSERT INTO Progenitor ($insertIntoMadre) VALUES ($insertValuesMadre)";

    /*     * ******************* PROCESANDO DATOS DEL PADRE ******************** */

    $insertsPadre = array();
    $valuesPadre = array();

    $idFotoPadre = null;
    // Datos Imagen
    {
        $imagePathPadre = $fotoPadre['tmp_name'];
        $imageSizePadre = $fotoPadre['size'];
        $imageNamePadre = $fotoPadre['name'];
        $imageTypePadre = $fotoPadre['type'];

        if($imageSizePadre < $tamanio["value"]) {
            $fpPadre = fopen($imagePathPadre, 'r');
            $datosImagenPadre = null;
            if ($fpPadre) {
                $datosImagenPadre = fread($fpPadre, $imageSizePadre); // Cargar la imagen
                fclose($fpPadre);
            }
            if ($datosImagenPadre != null) {
                $datosImagenPadre = base64_encode($datosImagenPadre);
                $id = rand();
                $sqls[] = "INSERT INTO Foto (id, imagen, tipo) VALUES ($id, '$datosImagenPadre', '$imageTypePadre')";
                $idFotoPadre = $id;
            }
        } else {
            $idFotoPadre = null;
        }
    }

    $insertsPadre[] = "idFoto";
    $valuesPadre[] = "$idFotoPadre";

    $insertsPadre[] = "primerApellido";
    $valuesPadre[] = "'$primerApellidoPadre'";

    $insertsPadre[] = "segundoApellido";
    $valuesPadre[] = "'$segundoApellidoPadre'";

    $insertsPadre[] = "nombres";
    $valuesPadre[] = "'$nombresPadre'";

    $insertsPadre[] = "lugarNacimiento";
    $valuesPadre[] = "'$lugarNacimientoPadre'";

    $insertsPadre[] = "fechaNacimiento";
    $valuesPadre[] = "'$fechaNacimientoPadre'";

    $insertsPadre[] = "edadCumplida";
    $valuesPadre[] = "$edadPadre";

    $insertsPadre[] = "tid";
    $valuesPadre[] = "'$tidPadre'";

    $insertsPadre[] = "nid";
    $valuesPadre[] = "'$nidPadre'";

    $insertsPadre[] = "profesion";
    $valuesPadre[] = "'$profesionPadre'";

    $insertsPadre[] = "empresa";
    $valuesPadre[] = "'$empresaPadre'";

    if ($celularPadre != null) {
        $insertsPadre[] = "celular";
        $valuesPadre[] = "'$celularPadre'";
    }

    if ($telefonoPadre != null) {
        $insertsPadre[] = "telefono";
        $valuesPadre[] = "'$telefonoPadre'";
    }

    if ($extensionPadre != null) {
        $insertsPadre[] = "extension";
        $valuesPadre[] = "'$extensionPadre'";
    }

    if ($cargoPadre != null) {
        $insertsPadre[] = "cargo";
        $valuesPadre[] = "'$cargoPadre'";
    }

    if ($ingresosPadre != null) {
        $insertsPadre[] = "ingresosMensuales";
        $valuesPadre[] = "'$ingresosPadre'";
    }

    if ($trabajoPadre != null) {
        $insertsPadre[] = "trabajo";
        $valuesPadre[] = "'$trabajoPadre'";
    }

    if ($estudiosPrimariosPadre != null) {
        $insertsPadre[] = "estudiosPrimarios";
        $valuesPadre[] = "'$estudiosPrimariosPadre'";
    }

    if ($estudiosSecundariosPadre != null) {
        $insertsPadre[] = "estudiosSecundarios";
        $valuesPadre[] = "'$estudiosSecundariosPadre'";
    }

    if ($estudiosUniversitariosPadre != null) {
        $insertsPadre[] = "estudiosUniversitarios";
        $valuesPadre[] = "'$estudiosUniversitariosPadre'";
    }

    if ($estudiosOtrosPadre != null) {
        $insertsPadre[] = "estudiosOtros";
        $valuesPadre[] = "'$estudiosOtrosPadre'";
    }

    $insertsPadre[] = "resideEn";
    $valuesPadre[] = "'$resideEnRadioPadre'";

    $insertsPadre[] = "tipoResidencia";
    $valuesPadre[] = "'$tipoResidenciaRadioPadre'";

    $insertsPadre[] = "email";
    $valuesPadre[] = "'$correoPadre'";

    $insertsPadre[] = "intereses";
    $valuesPadre[] = "'$interesesPadre'";

    $insertIntoPadre = "";
    for ($i = 0; $i < sizeof($insertsPadre); $i++) {
        $insertIntoPadre .= $insertsPadre[$i];
        if ($i < sizeof($insertsPadre) - 1) {
            $insertIntoPadre .= ", ";
        }
    }
    $insertValuesPadre = "";
    for ($i = 0; $i < sizeof($valuesPadre); $i++) {
        $insertValuesPadre .= $valuesPadre[$i];
        if ($i < sizeof($valuesPadre) - 1) {
            $insertValuesPadre .= ", ";
        }
    }

    $sqls[] = "INSERT INTO Progenitor ($insertIntoPadre) VALUES ($insertValuesPadre)";

    /*     * ******************* INSERTANDO DATOS ******************** */

    $anio = substr($pin, 0, 4);
    $numero = substr($pin, 4);

    $sqls[] = "INSERT INTO FormularioPadres (anioPin, numeroPin, tidMadre, nidMadre, tidPadre, nidPadre) "
            . "VALUES ($anio, '$numero', '$tidMadre', '$nidMadre', '$tidPadre', '$nidPadre')";

    if ($idFotoMadre != null && $idFotoPadre != null) {
        if (executeMultiQuery($sqls)) {
            mostrarMensaje("Información de los Padres registrada correctamente");
            redirigir("formularioAdicional.php?pin=$pin");
        } else {
            mostrarMensaje("Ha ocurrido un error al intentar guardar las fotos");
        }
    } else {
        if($idFotoMadre != null) {
            mostrarMensaje("Ha ocurrido un error al intentar guardar la foto de la madre");
        } else if($idFotoPadre != null) {
            mostrarMensaje("Ha ocurrido un error al intentar guardar la foto del padre");
        }
    }
}