<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';

if (isset($_POST['register'])) {
    $anio = ANIO_ADMISIONES;
    $numero = random_str(11);

    $insert = "INSERT INTO Pin (anio, numero) VALUES ($anio, '$numero') ";
    if (executeSimpleQuery($insert)) {
        mostrarMensaje("PIN generado correctamente");
        $pin = $anio . $numero;
        redirigir("mensajePin.php?pin=$pin");
    }
}
