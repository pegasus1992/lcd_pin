<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './model/Pin.php';

if (isset($_GET['verFormulario'])) {
    $pin = $_GET['pin'];
    redirigir("mostrarFormulario.php?pin=$pin");
}