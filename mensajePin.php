<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './model/Pin.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Generar Pin</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div style="width:1200px" class="texto2">
            <?php
            $pin = $_GET['pin'];
            if (strlen($pin) == LONGITUD_PIN) {
                $anio = substr($pin, 0, 4);
                $numero = substr($pin, 4);
                $pin = Pin::getPin($anio, $numero);
                if ($pin != null) {
                    ?>
                    <h4 align="center">Pin Generado Satisfactoriamente</h4>
                    <?php
                    $pinAnio = $pin->getAnio();
                    $pinNumero = $pin->getNumero();
                    $pinNumeroFormulario = $pin->getNumeroFormulario();
                    echo "<p>Se ha generado el Pin <b>$pinAnio" . "$pinNumero</b>,</p>";
                    echo "<p>Asociado al formulario N° <b>$pinNumeroFormulario</b>.</p>";
                    ?>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <style type="text/css" media="print">
                        @page { size: landscape; }
                    </style>
                    <div align="center">
                        <!--input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/-->
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'generarPin.php'" class="Button2">
                    </div>
                    <?php
                } else {
                    ?>
                    <h4 align="center">Pin no encontrado</h4>
                    <?php
                    echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                    </div>
                    <?php
                }
            } else {
                ?>
                <h4 align="center">Pin no válido</h4>
                <?php
                echo "<p>Se ha ingresado un Pin no válido.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = './'" class="Button2">
                </div>
                <?php
            }
            ?>
        </div>
    </body>
</html>