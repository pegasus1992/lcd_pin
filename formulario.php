<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/formularioController.php';

function tipoDireccion() {
    return '<select name="tipoDireccion" id="tipoDireccion" required>'
            . '<option></option>'
            . '<option value="Cll">Calle</option>'
            . '<option value="Cr">Carrera</option>'
            . '<option value="Dg">Diagonal</option>'
            . '<option value="Tr">Transversal</option>'
            . '<option value="Av. Cll">Avenida Calle</option>'
            . '<option value="Av. Cr">Avenida Carrera</option>'
            . '</select>';
}

function tipoDireccionRuta() {
    return '<select name="tipoDireccionRuta" id="tipoDireccionRuta" disabled required>'
            . '<option></option>'
            . '<option value="Cll">Calle</option>'
            . '<option value="Cr">Carrera</option>'
            . '<option value="Dg">Diagonal</option>'
            . '<option value="Tr">Transversal</option>'
            . '<option value="Av. Cll">Avenida Calle</option>'
            . '<option value="Av. Cr">Avenida Carrera</option>'
            . '</select>';
}

function listaAZ($i, $disabled = false) {
    $dis = $disabled ? 'disabled' : '';
    return '<select name="az' . $i . '" id="az' . $i . '" ' . $dis . '>'
            . '<option></option>'
            . '<option value="A">A</option><option value="B">B</option><option value="C">C</option>'
            . '<option value="D">D</option><option value="E">E</option><option value="F">F</option>'
            . '<option value="G">G</option><option value="H">H</option><option value="I">I</option>'
            . '<option value="J">J</option><option value="K">K</option><option value="L">L</option>'
            . '<option value="M">M</option><option value="N">N</option><option value="O">O</option>'
            . '<option value="P">P</option><option value="Q">Q</option><option value="R">R</option>'
            . '<option value="S">S</option><option value="T">T</option><option value="U">U</option>'
            . '<option value="V">V</option><option value="W">W</option><option value="X">X</option>'
            . '<option value="Y">Y</option><option value="Z">Z</option>'
            . '</select>';
}

function listaAZRuta($i, $disabled = false) {
    $dis = $disabled ? 'disabled' : '';
    return '<select name="az' . $i . 'Ruta" id="az' . $i . 'Ruta" ' . $dis . ' disabled>'
            . '<option></option>'
            . '<option value="A">A</option><option value="B">B</option><option value="C">C</option>'
            . '<option value="D">D</option><option value="E">E</option><option value="F">F</option>'
            . '<option value="G">G</option><option value="H">H</option><option value="I">I</option>'
            . '<option value="J">J</option><option value="K">K</option><option value="L">L</option>'
            . '<option value="M">M</option><option value="N">N</option><option value="O">O</option>'
            . '<option value="P">P</option><option value="Q">Q</option><option value="R">R</option>'
            . '<option value="S">S</option><option value="T">T</option><option value="U">U</option>'
            . '<option value="V">V</option><option value="W">W</option><option value="X">X</option>'
            . '<option value="Y">Y</option><option value="Z">Z</option>'
            . '</select>';
}

function listaSE($i) {
    return '<select name="se' . $i .'" id="se' . $i . '">'
            . '<option></option>'
            . '<option value="N">Norte</option>'
            . '<option value="S">Sur</option>'
            . '<option value="E">Este</option>'
            . '<option value="O">Oeste</option>'
            . '</select>';
}

function listaSERuta($i) {
    return '<select name="se' . $i .'Ruta" id="se' . $i . 'Ruta" disabled>'
            . '<option></option>'
            . '<option value="N">Norte</option>'
            . '<option value="S">Sur</option>'
            . '<option value="E">Este</option>'
            . '<option value="O">Oeste</option>'
            . '</select>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Diligenciar Formulario - Información del Educando</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        $noUtilizado = Pin::existePinSinUtilizar($pin);
        if ($noUtilizado) {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Diligenciar Formulario - Información del Educando</h4>
                <div>
                    <label>Pin:</label>
                    <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                </div>
                <div>
                    <label>Fotografía del Educando:</label>
                    <input placeholder="Ingrese la foto" name="fotoNino" id="fotoNino" type="file" accept="image/*" autocomplete="off" required/>
                    <br/>
                    <br/>
                    <?php
                    echo imageField("imagenNino");
                    ?>
                </div>
                <div>
                    <label>Primer Apellido del Educando:</label>
                    <input placeholder="Ingrese el primer apellido del educando" name="primerApellidoNino" id="primerApellidoNino" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Segundo Apellido del Educando:</label>
                    <input placeholder="Ingrese el segundo apellido del educando" name="segundoApellidoNino" id="segundoApellidoNino" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Nombres del Educando:</label>
                    <input placeholder="Ingrese los nombres del educando" name="nombresNino" id="nombresNino" type="text" autocomplete="on" maxlength="50" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Lugar de Nacimiento:</label>
                    <input placeholder="Ingrese el lugar de nacimiento" name="lugarNacimientoNino" id="lugarNacimientoNino" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Fecha de Nacimiento:</label>
                    <input type="date" id="fechaNacimientoNino" name="fechaNacimientoNino" class="fecha" placeholder="yyyy-mm-dd" onblur="edad(this.value)" required/>
                </div>
                <div id="fechaNacimientoNinoError" style="color: red; align-content: center"></div>
                <div>
                    <label>Edad cumplida (años):</label>
                    <input placeholder="Ingrese la edad" name="edadNino" id="edadNino" type="number" autocomplete="on" readonly/>
                </div>
                <div>
                    <label>Grupo Sanguíneo y RH:</label>
                    <grupoRH>
                        <select name="grupoSanguineoNino" id="grupoSanguineo" required>
                            <option></option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="O">O</option>
                            <option value="AB">AB</option>
                        </select>
                        <select name="rhNino" id="rh" required>
                            <option></option>
                            <option value="P">Positivo</option>
                            <option value="N">Negativo</option>
                        </select>
                    </grupoRH>
                </div>
                <div>
                    <label>Estrato donde vive:</label>
                    <input placeholder="Ingrese el estrato" name="estratoNino" id="estratoNino" type="number" autocomplete="on" min="1" max="9" required/>
                </div>
                <div>
                    <label>EPS a la que está afiliado:</label>
                    <input placeholder="Ingrese la EPS" name="epsNino" id="epsNino" type="text" autocomplete="on" maxlength="100" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Tipo de Documento:</label>
                    <select name="tidNino" id="tidNino" required>
                        <option></option>
                        <?php
                        $documentos = TipoDocumento::traerDocumentos();
                        foreach ($documentos as $documento) {
                            /*if (trim($documento->getDocumento()) != 'CC')*/ {
                                echo '<option value="' . $documento->getDocumento() . '">' . $documento->getNombre() . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <label>Número de Identificación:</label>
                    <input placeholder="Ingrese el número de identificación" name="nidNino" id="nidNino" type="text" autocomplete="on" maxlength="20" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Lugar de expedición del documento:</label>
                    <input placeholder="Ingrese el lugar de expedición" name="lugarExpedicionNino" id="lugarExpedicionNino" type="text" autocomplete="on" maxlength="75" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Nacionalidad:</label>
                    <input placeholder="Ingrese la nacionalidad" name="nacionalidadNino" id="nacionalidadNino" type="text" autocomplete="on" maxlength="30" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Dirección de Residencia:</label>
                    <direccion>
                        <?php echo tipoDireccion() ?>
                        <input name="numTipoDireccion" id="numTipoDireccion" type="number" autocomplete="on" min="0" required/>
                        <?php echo listaAZ(1) ?>
                        <br/>
                        <br/>
                        <label><input type="checkbox" name="bisDireccion" id="bisDireccion" value="BIS">Bis</label>
                        <?php echo listaAZ(2, true) ?>
                        <?php echo listaSE(1) ?>
                        <br/>
                        <br/>
                        <label>No.</label>
                        <input name="numDireccion" id="numDireccion" type="number" autocomplete="on" min="0" required/>
                        <?php echo listaAZ(3) ?>
                        <br/>
                        <br/>
                        <label>-</label>
                        <input name="numDireccion2" id="numDireccion2" type="number" autocomplete="on" min="0"/>
                        <?php echo listaSE(2) ?>
                        <br/>
                        <br/>
                        <label>Apto/Int.</label>
                        <input name="intDireccion" id="intDireccion" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();"/>
                    </direccion>
                </div>
                <div>
                    <label>Barrio:</label>
                    <input placeholder="Ingrese el barrio" name="barrioNino" id="barrioNino" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Teléfono:</label>
                    <input placeholder="Ingrese el teléfono" name="telefonoNino" id="telefonoNino" type="number" autocomplete="on" min="1000000" max="999999999999999" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Grado al que desea Ingresar:</label>
                    <select name="gradoDeseaNino" id="gradoDeseaNino" required>
                        <option></option>
                        <option value="Prejardín">Prejardín</option>
                        <option value="Jardín">Jardín</option>
                        <option value="Transición">Transición</option>
                        <option value="Primero">Primero</option>
                        <option value="Sexto">Sexto</option>
                    </select>
                </div>
                <div>
                    <label>Grado que cursa en el Presente Año:</label>
                    <input placeholder="Ingrese el grado que actualmente cursa" name="gradoActualNino" id="gradoActualNino" type="text" autocomplete="on" maxlength="20" required onkeyup="this.value = this.value.toUpperCase();"/>
                </div>
                <div>
                    <label>Estudia en el Liceo Campo David:</label>
                    <radio>
                        <input type="radio" name="conocidoRadio" id="hermanoRadio" value="Hermano" required/>Hermano(a)
                        <br/>
                        <input type="radio" name="conocidoRadio" id="primoRadio" value="Primo" required/>Primo(a)
                        <br/>
                        <input type="radio" name="conocidoRadio" id="amigoRadio" value="Amigo" required/>Amigo(a)
                        <br/>
                        <input type="radio" name="conocidoRadio" id="otroRadio" value="Otro" required/>Otro
                        <br/>
                        <input type="radio" name="conocidoRadio" id="ningunoRadio" value="Ninguno" required/>Ninguno
                        <br/>
                        <br/>
                        <input placeholder="¿Quién?" name="conocidoNino" id="conocidoNino" type="text" autocomplete="on" maxlength="30" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿El educando tiene algún tipo de impedimento físico, psicológico o mental?</label>
                    <radio>
                        <input type="radio" name="impedimentoRadio" id="impedimentoSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="impedimentoRadio" id="impedimentoNo" value="N" required/>No
                        <br/>
                        <br/>
                        <input placeholder="¿Cuál?" name="nombreImpedimentoNino" id="nombreImpedimentoNino" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Ha tenido el educando aspirante algún tipo de ayuda psicológica, de fonoaudiología y/o terapia ocupacional?</label>
                    <radio>
                        <input type="radio" name="tenidoAyudaRadio" id="tenidoAyudaSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="tenidoAyudaRadio" id="tenidoAyudaNo" value="N" required/>No
                        <br/>
                        <br/>
                        <textarea cols="40" rows="5" placeholder="¿Cuál?" name="nombreAyudaNino" id="nombreAyudaNino" type="text" maxlength="100" autocomplete="on" disabled required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                    </radio>
                </div>
                <div>
                    <label>¿Padece el educando alguna enfermedad de consideración?</label>
                    <radio>
                        <input type="radio" name="enfermedadRadio" id="enfermedadSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="enfermedadRadio" id="enfermedadNo" value="N" required/>No
                        <br/>
                        <br/>
                        <input placeholder="¿Cuál?" name="enfermedadNino" id="enfermedadNino" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Debe ingerir diaria o periódicamente algún medicamento especial?</label>
                    <radio>
                        <input type="radio" name="medicamentoEspecialRadio" id="medicamentoSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="medicamentoEspecialRadio" id="medicamentoNo" value="N" required/>No
                        <br/>
                        <br/>
                        <input placeholder="¿Cuál?" name="medicamentoEspecialNino" id="medicamentoEspecialNino" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Requiere servicio de Transporte Escolar?</label>
                    <radio>
                        <input type="radio" name="transporteEscolarRadio" id="transporteEscolarSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="transporteEscolarRadio" id="transporteEscolarNo" value="N" required/>No
                    </radio>
                </div>
                <div>
                    <label>¿Cuál?</label>
                    <radio>
                        <input type="radio" name="tipoRutaRadio" id="mediaRuta" value="M" disabled required/>Media Ruta
                        <br/>
                        <input type="radio" name="tipoRutaRadio" id="rutaCompleta" value="C" disabled required/>Ruta Completa
                        <br/>
                        <br/>
                        <label>Dirección:</label>
                        <direccion>
                            <?php echo tipoDireccionRuta() ?>
                            <input name="numTipoDireccionRuta" id="numTipoDireccionRuta" type="number" autocomplete="on" min="0" required disabled/>
                            <?php echo listaAZRuta(1) ?>
                            <br/>
                            <br/>
                            <label><input type="checkbox" name="bisDireccionRuta" id="bisDireccionRuta" value="BIS" disabled>Bis</label>
                            <?php echo listaAZRuta(2, true) ?>
                            <?php echo listaSERuta(1) ?>
                            <br/>
                            <br/>
                            <label>No.</label>
                            <input name="numDireccionRuta" id="numDireccionRuta" type="number" autocomplete="on" min="0" required disabled/>
                            <?php echo listaAZRuta(3) ?>
                            <br/>
                            <br/>
                            <label>-</label>
                            <input name="numDireccion2Ruta" id="numDireccion2Ruta" type="number" autocomplete="on" min="0" disabled/>
                            <?php echo listaSERuta(2) ?>
                            <br/>
                            <br/>
                            <label>Apto/Int.</label>
                            <input name="intDireccionRuta" id="intDireccionRuta" type="text" autocomplete="on" maxlength="5" onkeyup="this.value = this.value.toUpperCase();" disabled/>
                        </direccion>
                        <br/>
                        <br/>
                        <label>Barrio:</label>
                        <input placeholder="Escriba el barrio" name="barrioRutaNino" id="barrioRutaNino" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Requiere servicio de Restaurante Escolar?</label>
                    <radio>
                        <input type="radio" name="restauranteEscolarRadio" id="restauranteEscolarSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="restauranteEscolarRadio" id="restauranteEscolarNo" value="N" required/>No
                    </radio>
                </div>
                <div>
                    <label>El Educando es:</label>
                    <radio>
                        <input type="radio" name="manoDominanteRadio" id="zurdo" value="Z" required/>Zurdo
                        <br/>
                        <input type="radio" name="manoDominanteRadio" id="diestro" value="D" required/>Diestro
                        <br/>
                        <input type="radio" name="manoDominanteRadio" id="ambidiestro" value="A" required/>Ambidiestro
                    </radio>
                </div>
                <div>
                    <label>¿Usa gafas o lentes formulados por un especialista?</label>
                    <radio>
                        <input type="radio" name="usaGafasRadio" id="usaGafasSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="usaGafasRadio" id="usaGafasNo" value="N" required/>No
                        <br/>
                        <br/>
                        <input placeholder="¿Qué deficiencia visual presenta?" name="deficienciaVisualNino" id="deficienciaVisualNino" type="text" autocomplete="on" maxlength="25" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Ha sufrido alguna fractura?</label>
                    <radio>
                        <input type="radio" name="fracturaRadio" id="fracturaSi" value="S" required/>Sí
                        <br/>
                        <input type="radio" name="fracturaRadio" id="fracturaNo" value="N" required/>No
                        <br/>
                        <br/>
                        <input placeholder="Precise, ¿cuál?" name="detalleFracturaNino" id="detalleFracturaNino" type="text" autocomplete="on" maxlength="100" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </radio>
                </div>
                <div>
                    <label>¿Desea señalar algún aspecto especial del estado de su hijo(a) que considere Ud. importante y además, la deba conocer el Liceo?</label>
                    <textarea cols="40" rows="5" placeholder="Si no desea añadir algún aspecto adicional, coloque Ninguno o No aplica." name="aspectosEspecialesNino" id="aspectosEspecialesNino" type="text" maxlength="300" autocomplete="on" required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                </div>

                <!----------------- PANEL DE BOTONES ----------------->
                <div align="center">
                    <input name="registrar" id="submit" type="submit" value="Registrar e Ir a Información de los Padres" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                </div>
            </form>
            <?php
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no encontrado o ya utilizado</h4>
                <?php
                echo "<p>El Pin <b>$pin</b> no existe o ya ha sido utilizado.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
        <!-- CARGAR IMAGEN -->
        <script src="view/js/cargadorFotos.js" type="text/javascript"></script>
        <!-- FORMULARIO -->
        <script src="view/js/formulario.js" type="text/javascript"></script>
    </body>
</html>