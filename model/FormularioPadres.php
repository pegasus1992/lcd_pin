<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once 'Pin.php';
include_once 'Progenitor.php';

class FormularioPadres {

    private $id, $pin;
    private $madre, $padre;

    public function FormularioPadres($id, $pin, $madre, $padre) {
        $this->setId($id);
        $this->setPin($pin);
        $this->setMadre($madre);
        $this->setPadre($padre);
    }

    function getPin() {
        return $this->pin;
    }

    function setPin($pin) {
        $this->pin = $pin;
    }

    function getId() {
        return $this->id;
    }

    function getMadre() {
        return $this->madre;
    }

    function getPadre() {
        return $this->padre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMadre($madre) {
        $this->madre = $madre;
    }

    function setPadre($padre) {
        $this->padre = $padre;
    }

    /**
     * Obtiene el formulario de padres de la BD, dado el PIN.
     * @param type $pin <p>PIN del formulario.</p>
     * @return \FormularioPadres
     */
    public static function obtenerFormularioPadres($pin) {
        $anio = substr($pin, 0, 4);
        $numero = substr($pin, 4);
        $sql = "SELECT FormularioPadres.*, Pin.pagado, Pin.utilizado, Pin.numeroFormulario "
                . "FROM FormularioPadres LEFT JOIN Pin ON (Pin.anio = FormularioPadres.anioPin AND Pin.numero = FormularioPadres.numeroPin) "
                . "WHERE anioPin = '$anio' AND numeroPin = '$numero'";
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $madre = Progenitor::obtenerProgenitor($fila['tidMadre'], $fila['nidMadre']);
            $padre = Progenitor::obtenerProgenitor($fila['tidPadre'], $fila['nidPadre']);
            $pin = new Pin($fila['anioPin'], $fila['numeroPin'], $fila['pagado'], $fila['utilizado'], $fila['numeroFormulario']);
            $response = new FormularioPadres($fila['id'], $pin, $madre, $padre);
        }
        return $response;
    }

    /**
     * Obtiene la cantidad de formularios de padres registrados en la BD, que coincidan con el pin ingresado.
     * @param type $pin <p>PIN del formulario.</p>
     * @return type
     */
    public static function obtenerConteoFormulariosPadres($pin) {
        $anio = substr($pin, 0, 4);
        $numero = substr($pin, 4);
        $sql = "SELECT * FROM FormularioPadres WHERE anioPin = '$anio' AND numeroPin = '$numero'";
        $result = getResultSet($sql);
        $response = $result->num_rows;
        return $response;
    }

}
