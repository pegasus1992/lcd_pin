<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';

class Hermano {

    private $nombres, $apellidos, $edad, $colegio, $grado;

    public function Hermano($nombres, $apellidos, $edad, $colegio, $grado) {
        $this->setNombres($nombres);
        $this->setApellidos($apellidos);
        $this->setEdad($edad);
        $this->setColegio($colegio);
        $this->setGrado($grado);
    }

    function getNombres() {
        return $this->nombres;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function getEdad() {
        return $this->edad;
    }

    function getColegio() {
        return $this->colegio;
    }

    function getGrado() {
        return $this->grado;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setColegio($colegio) {
        $this->colegio = $colegio;
    }

    function setGrado($grado) {
        $this->grado = $grado;
    }

    /**
     * Obtiene los hermanos registrados en el formulario adicional de ld BD.
     * @param type $idFormulario
     * @return \Hermano
     */
    public static function obtenerHermanos($idFormulario) {
        $sql = "SELECT * FROM Hermano "
                . "LEFT JOIN FormularioAdicional ON (FormularioAdicional.id = Hermano.idFormulario) "
                . "WHERE idFormulario = $idFormulario";
        $result = getResultSet($sql);
        $hermanos = null;
        if ($result->num_rows > 0) {
            $hermanos = array();
            while ($fila = mysqli_fetch_array($result)) {
                $hermano = new Hermano($fila['nombres'], $fila['apellidos'], $fila['edad'], $fila['colegio'], $fila['grado']);
                $hermanos[] = $hermano;
            }
        }
        return $hermanos;
    }

}
