<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once 'Pin.php';
include_once 'Hermano.php';
include_once 'Institucion.php';
include_once 'Archivo.php';

class FormularioAdicional {

    private $id, $pin;
    private $tipoUnion, $separados;
    private $adoptado, $edadAdoptado;
    private $direccionTelefonoEmergencia, $direccionTelefonoFamiliar;
    private $personaResponde, $padresFirman, $nombreFirma, $autorizaInformacion, $autorizaExplique;
    private $miembroReferencia, $padresReferencia, $gradoReferencia, $firmaReferencia;
    private $caracteristicas, $visionEducativa;
    private $escolaridadAnterior, $pazSalvo, $ultimoBoletin, $certificadoIngresos;
    private $hermanos, $instituciones;

    public function FormularioAdicional($id, $pin, $tipoUnion, $separados, $adoptado, $edadAdoptado, $hermanos, $direccionTelefonoEmergencia, $direccionTelefonoFamiliar, $personaResponde, $padresFirman, $nombreFirma, $autorizaInformacion, $autorizaExplique, $miembroReferencia, $padresReferencia, $gradoReferencia, $firmaReferencia, $caracteristicas, $instituciones, $visionEducativa, $escolaridadAnterior, $pazSalvo, $ultimoBoletin, $certificadoIngresos) {
        $this->setId($id);
        $this->setPin($pin);
        $this->setTipoUnion($tipoUnion);
        $this->setSeparados($separados);
        $this->setAdoptado($adoptado);
        $this->setEdadAdoptado($edadAdoptado);
        $this->setHermanos($hermanos);
        $this->setDireccionTelefonoEmergencia($direccionTelefonoEmergencia);
        $this->setDireccionTelefonoFamiliar($direccionTelefonoFamiliar);
        $this->setPersonaResponde($personaResponde);
        $this->setPadresFirman($padresFirman);
        $this->setNombreFirma($nombreFirma);
        $this->setAutorizaInformacion($autorizaInformacion);
        $this->setAutorizaExplique($autorizaExplique);
        $this->setMiembroReferencia($miembroReferencia);
        $this->setPadresReferencia($padresReferencia);
        $this->setGradoReferencia($gradoReferencia);
        $this->setFirmaReferencia($firmaReferencia);
        $this->setCaracteristicas($caracteristicas);
        $this->setInstituciones($instituciones);
        $this->setVisionEducativa($visionEducativa);
        $this->setEscolaridadAnterior($escolaridadAnterior);
        $this->setPazSalvo($pazSalvo);
        $this->setUltimoBoletin($ultimoBoletin);
        $this->setCertificadoIngresos($certificadoIngresos);
    }

    function getCertificadoIngresos() {
        return $this->certificadoIngresos;
    }

    function setCertificadoIngresos($certificadoIngresos) {
        $this->certificadoIngresos = $certificadoIngresos;
    }

    function getEscolaridadAnterior() {
        return $this->escolaridadAnterior;
    }

    function getPazSalvo() {
        return $this->pazSalvo;
    }

    function getUltimoBoletin() {
        return $this->ultimoBoletin;
    }

    function setEscolaridadAnterior($escolaridadAnterior) {
        $this->escolaridadAnterior = $escolaridadAnterior;
    }

    function setPazSalvo($pazSalvo) {
        $this->pazSalvo = $pazSalvo;
    }

    function setUltimoBoletin($ultimoBoletin) {
        $this->ultimoBoletin = $ultimoBoletin;
    }

    function getPin() {
        return $this->pin;
    }

    function setPin($pin) {
        $this->pin = $pin;
    }

    function getId() {
        return $this->id;
    }

    function getTipoUnion() {
        return $this->tipoUnion;
    }

    function getSeparados() {
        return $this->separados;
    }

    function getAdoptado() {
        return $this->adoptado;
    }

    function getEdadAdoptado() {
        return $this->edadAdoptado;
    }

    function getDireccionTelefonoEmergencia() {
        return $this->direccionTelefonoEmergencia;
    }

    function getDireccionTelefonoFamiliar() {
        return $this->direccionTelefonoFamiliar;
    }

    function getPersonaResponde() {
        return $this->personaResponde;
    }

    function getPadresFirman() {
        return $this->padresFirman;
    }

    function getNombreFirma() {
        return $this->nombreFirma;
    }

    function getAutorizaInformacion() {
        return $this->autorizaInformacion;
    }

    function getAutorizaExplique() {
        return $this->autorizaExplique;
    }

    function getMiembroReferencia() {
        return $this->miembroReferencia;
    }

    function getPadresReferencia() {
        return $this->padresReferencia;
    }

    function getGradoReferencia() {
        return $this->gradoReferencia;
    }

    function getFirmaReferencia() {
        return $this->firmaReferencia;
    }

    function getCaracteristicas() {
        return $this->caracteristicas;
    }

    function getVisionEducativa() {
        return $this->visionEducativa;
    }

    function getHermanos() {
        return $this->hermanos;
    }

    function getInstituciones() {
        return $this->instituciones;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipoUnion($tipoUnion) {
        $this->tipoUnion = $tipoUnion;
    }

    function setSeparados($separados) {
        $this->separados = $separados;
    }

    function setAdoptado($adoptado) {
        $this->adoptado = $adoptado;
    }

    function setEdadAdoptado($edadAdoptado) {
        $this->edadAdoptado = $edadAdoptado;
    }

    function setDireccionTelefonoEmergencia($direccionTelefonoEmergencia) {
        $this->direccionTelefonoEmergencia = $direccionTelefonoEmergencia;
    }

    function setDireccionTelefonoFamiliar($direccionTelefonoFamiliar) {
        $this->direccionTelefonoFamiliar = $direccionTelefonoFamiliar;
    }

    function setPersonaResponde($personaResponde) {
        $this->personaResponde = $personaResponde;
    }

    function setPadresFirman($padresFirman) {
        $this->padresFirman = $padresFirman;
    }

    function setNombreFirma($nombreFirma) {
        $this->nombreFirma = $nombreFirma;
    }

    function setAutorizaInformacion($autorizaInformacion) {
        $this->autorizaInformacion = $autorizaInformacion;
    }

    function setAutorizaExplique($autorizaExplique) {
        $this->autorizaExplique = $autorizaExplique;
    }

    function setMiembroReferencia($miembroReferencia) {
        $this->miembroReferencia = $miembroReferencia;
    }

    function setPadresReferencia($padresReferencia) {
        $this->padresReferencia = $padresReferencia;
    }

    function setGradoReferencia($gradoReferencia) {
        $this->gradoReferencia = $gradoReferencia;
    }

    function setFirmaReferencia($firmaReferencia) {
        $this->firmaReferencia = $firmaReferencia;
    }

    function setCaracteristicas($caracteristicas) {
        $this->caracteristicas = $caracteristicas;
    }

    function setVisionEducativa($visionEducativa) {
        $this->visionEducativa = $visionEducativa;
    }

    function setHermanos($hermanos) {
        $this->hermanos = $hermanos;
    }

    function setInstituciones($instituciones) {
        $this->instituciones = $instituciones;
    }

    /**
     * Obtiene el formulario adicional de la BD, dado el PIN.
     * @param type $pin <p>PIN del formulario.</p>
     * @return \FormularioAdicional
     */
    public static function obtenerFormularioAdicional($pin) {
        $anio = substr($pin, 0, 4);
        $numero = substr($pin, 4);
        $sql = "SELECT FormularioAdicional.*, Pin.pagado, Pin.utilizado, Pin.numeroFormulario "
                . "FROM FormularioAdicional LEFT JOIN Pin ON (Pin.anio = FormularioAdicional.anioPin AND Pin.numero = FormularioAdicional.numeroPin) "
                . "WHERE anioPin = '$anio' AND numeroPin = '$numero'";
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $pin = new Pin($fila['anioPin'], $fila['numeroPin'], $fila['pagado'], $fila['utilizado'], $fila['numeroFormulario']);
            $idFormulario = $fila['id'];

            $idPazSalvo = $fila['pazSalvo'];
            $pazSalvo = null;
            if ($idPazSalvo != null) {
                $pazSalvo = Archivo::getArchivo($idPazSalvo);
            }

            $idUltimoBoletin = $fila['ultimoBoletin'];
            $ultimoBoletin = null;
            if ($idUltimoBoletin != null) {
                $ultimoBoletin = Archivo::getArchivo($idUltimoBoletin);
            }
            
            $idCertificadoIngresos = $fila['certificadoIngresos'];
            $certificadoIngresos = Archivo::getArchivo($idCertificadoIngresos);

            $hermanos = Hermano::obtenerHermanos($idFormulario);
            $instituciones = Institucion::obtenerInstituciones($idFormulario);
            $response = new FormularioAdicional($idFormulario, $pin, $fila['tipoUnion'], $fila['separados'], $fila['adoptado'], $fila['edadAdoptado'], $hermanos, $fila['direccionTelefonoEmergencia'], $fila['direccionTelefonoFamiliar'], $fila['personaResponde'], $fila['padresFirman'], $fila['nombreFirma'], $fila['autorizaInformacion'], $fila['autorizaExplique'], $fila['miembroReferencia'], $fila['padresReferencia'], $fila['gradoReferencia'], $fila['firmaReferencia'], $fila['caracteristicas'], $instituciones, $fila['visionEducativa'], $fila['escolaridadAnterior'], $pazSalvo, $ultimoBoletin, $certificadoIngresos);
        }
        return $response;
    }

    /**
     * Obtiene la cantidad de formularios adicionales registrados en la BD, que coincidan con el pin ingresado.
     * @param type $pin <p>PIN del formulario.</p>
     * @return type
     */
    public static function obtenerConteoFormulariosAdicionales($pin) {
        $anio = substr($pin, 0, 4);
        $numero = substr($pin, 4);
        $sql = "SELECT * FROM FormularioAdicional WHERE anioPin = '$anio' AND numeroPin = '$numero'";
        $result = getResultSet($sql);
        $response = $result->num_rows;
        return $response;
    }

}
