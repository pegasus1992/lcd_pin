<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once 'Pin.php';
include_once 'Foto.php';
include_once 'TipoDocumento.php';

class Formulario {

    private $id, $pin;
    private $primerApellido, $segundoApellido, $nombres, $tid, $nid, $foto;
    private $lugarNacimiento, $fechaNacimiento, $edadCumplida;
    private $nacionalidad, $direccionResidencia, $barrio, $telefono;
    private $gradoIngresar, $gradoActual, $conocidoEstudia;
    private $impedimentoFisico, $nombreImpedimentoFisico;
    private $tenidoAyuda, $nombreAyuda;
    private $enfermedad, $nombreEnfermedad;
    private $medicamentoEspecial, $nombreMedicamentoEspecial;
    private $transporteEscolar, $tipoRuta, $direccionRuta, $barrioRuta;
    private $restauranteEscolar, $manoDominante;
    private $usaGafas, $deficienciaVisual;
    private $fractura, $detalleFractura;
    private $aspectosEspeciales;
    private $grupoSanguineo, $rh, $estrato, $eps, $lugarExpedicion;

    public function Formulario($id, $pin, $primerApellido, $segundoApellido, $nombres, $tid, $nid, $foto, $lugarNacimiento, $fechaNacimiento, $edadCumplida, $nacionalidad, $direccionResidencia, $barrio, $telefono, $gradoIngresar, $gradoActual, $conocidoEstudia, $impedimentoFisico, $nombreImpedimentoFisico, $tenidoAyuda, $nombreAyuda, $enfermedad, $nombreEnfermedad, $medicamentoEspecial, $nombreMedicamentoEspecial, $transporteEscolar, $tipoRuta, $direccionRuta, $barrioRuta, $restauranteEscolar, $manoDominante, $usaGafas, $deficienciaVisual, $fractura, $detalleFractura, $aspectosEspeciales, $grupoSanguineo, $rh, $estrato, $eps, $lugarExpedicion) {
        $this->setId($id);
        $this->setPin($pin);
        $this->setPrimerApellido($primerApellido);
        $this->setSegundoApellido($segundoApellido);
        $this->setNombres($nombres);
        $this->setTid($tid);
        $this->setNid($nid);
        $this->setFoto($foto);
        $this->setLugarNacimiento($lugarNacimiento);
        $this->setFechaNacimiento($fechaNacimiento);
        $this->setEdadCumplida($edadCumplida);
        $this->setNacionalidad($nacionalidad);
        $this->setDireccionResidencia($direccionResidencia);
        $this->setBarrio($barrio);
        $this->setTelefono($telefono);
        $this->setGradoIngresar($gradoIngresar);
        $this->setGradoActual($gradoActual);
        $this->setConocidoEstudia($conocidoEstudia);
        $this->setImpedimentoFisico($impedimentoFisico);
        $this->setNombreImpedimentoFisico($nombreImpedimentoFisico);
        $this->setTenidoAyuda($tenidoAyuda);
        $this->setNombreAyuda($nombreAyuda);
        $this->setEnfermedad($enfermedad);
        $this->setNombreEnfermedad($nombreEnfermedad);
        $this->setMedicamentoEspecial($medicamentoEspecial);
        $this->setNombreMedicamentoEspecial($nombreMedicamentoEspecial);
        $this->setTransporteEscolar($transporteEscolar);
        $this->setTipoRuta($tipoRuta);
        $this->setDireccionRuta($direccionRuta);
        $this->setBarrioRuta($barrioRuta);
        $this->setRestauranteEscolar($restauranteEscolar);
        $this->setManoDominante($manoDominante);
        $this->setUsaGafas($usaGafas);
        $this->setDeficienciaVisual($deficienciaVisual);
        $this->setFractura($fractura);
        $this->setDetalleFractura($detalleFractura);
        $this->setAspectosEspeciales($aspectosEspeciales);
        $this->setGrupoSanguineo($grupoSanguineo);
        $this->setRh($rh);
        $this->setEstrato($estrato);
        $this->setEps($eps);
        $this->setLugarExpedicion($lugarExpedicion);
    }

    function getGrupoSanguineo() {
        return $this->grupoSanguineo;
    }

    function getRh() {
        return $this->rh;
    }

    function getEstrato() {
        return $this->estrato;
    }

    function getEps() {
        return $this->eps;
    }

    function getLugarExpedicion() {
        return $this->lugarExpedicion;
    }

    function setGrupoSanguineo($grupoSanguineo) {
        $this->grupoSanguineo = $grupoSanguineo;
    }

    function setRh($rh) {
        $this->rh = $rh;
    }

    function setEstrato($estrato) {
        $this->estrato = $estrato;
    }

    function setEps($eps) {
        $this->eps = $eps;
    }

    function setLugarExpedicion($lugarExpedicion) {
        $this->lugarExpedicion = $lugarExpedicion;
    }

    function getPin() {
        return $this->pin;
    }

    function setPin($pin) {
        $this->pin = $pin;
    }

    function getId() {
        return $this->id;
    }

    function getPrimerApellido() {
        return $this->primerApellido;
    }

    function getSegundoApellido() {
        return $this->segundoApellido;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getTid() {
        return $this->tid;
    }

    function getNid() {
        return $this->nid;
    }

    function getFoto() {
        return $this->foto;
    }

    function getLugarNacimiento() {
        return $this->lugarNacimiento;
    }

    function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    function getEdadCumplida() {
        return $this->edadCumplida;
    }

    function getNacionalidad() {
        return $this->nacionalidad;
    }

    function getDireccionResidencia() {
        return $this->direccionResidencia;
    }

    function getBarrio() {
        return $this->barrio;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getGradoIngresar() {
        return $this->gradoIngresar;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPrimerApellido($primerApellido) {
        $this->primerApellido = $primerApellido;
    }

    function setSegundoApellido($segundoApellido) {
        $this->segundoApellido = $segundoApellido;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setTid($tid) {
        $this->tid = $tid;
    }

    function setNid($nid) {
        $this->nid = $nid;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setLugarNacimiento($lugarNacimiento) {
        $this->lugarNacimiento = $lugarNacimiento;
    }

    function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = substr($fechaNacimiento, 0, 10);
    }

    function setEdadCumplida($edadCumplida) {
        $this->edadCumplida = $edadCumplida;
    }

    function setNacionalidad($nacionalidad) {
        $this->nacionalidad = $nacionalidad;
    }

    function setDireccionResidencia($direccionResidencia) {
        $this->direccionResidencia = $direccionResidencia;
    }

    function setBarrio($barrio) {
        $this->barrio = $barrio;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setGradoIngresar($gradoIngresar) {
        $this->gradoIngresar = $gradoIngresar;
    }

    function getGradoActual() {
        return $this->gradoActual;
    }

    function getConocidoEstudia() {
        return $this->conocidoEstudia;
    }

    function setConocidoEstudia($conocidoEstudia) {
        $this->conocidoEstudia = $conocidoEstudia;
    }

    function getImpedimentoFisico() {
        return $this->impedimentoFisico;
    }

    function getNombreImpedimentoFisico() {
        return $this->nombreImpedimentoFisico;
    }

    function getTenidoAyuda() {
        return $this->tenidoAyuda;
    }

    function getNombreAyuda() {
        return $this->nombreAyuda;
    }

    function getEnfermedad() {
        return $this->enfermedad;
    }

    function getNombreEnfermedad() {
        return $this->nombreEnfermedad;
    }

    function getMedicamentoEspecial() {
        return $this->medicamentoEspecial;
    }

    function getNombreMedicamentoEspecial() {
        return $this->nombreMedicamentoEspecial;
    }

    function getTransporteEscolar() {
        return $this->transporteEscolar;
    }

    function getTipoRuta() {
        return $this->tipoRuta;
    }

    function getDireccionRuta() {
        return $this->direccionRuta;
    }

    function getBarrioRuta() {
        return $this->barrioRuta;
    }

    function getRestauranteEscolar() {
        return $this->restauranteEscolar;
    }

    function getManoDominante() {
        return $this->manoDominante;
    }

    function getUsaGafas() {
        return $this->usaGafas;
    }

    function getDeficienciaVisual() {
        return $this->deficienciaVisual;
    }

    function getFractura() {
        return $this->fractura;
    }

    function getDetalleFractura() {
        return $this->detalleFractura;
    }

    function getAspectosEspeciales() {
        return $this->aspectosEspeciales;
    }

    function setGradoActual($gradoActual) {
        $this->gradoActual = $gradoActual;
    }

    function setImpedimentoFisico($impedimentoFisico) {
        $this->impedimentoFisico = $impedimentoFisico;
    }

    function setNombreImpedimentoFisico($nombreImpedimentoFisico) {
        $this->nombreImpedimentoFisico = $nombreImpedimentoFisico;
    }

    function setTenidoAyuda($tenidoAyuda) {
        $this->tenidoAyuda = $tenidoAyuda;
    }

    function setNombreAyuda($nombreAyuda) {
        $this->nombreAyuda = $nombreAyuda;
    }

    function setEnfermedad($enfermedad) {
        $this->enfermedad = $enfermedad;
    }

    function setNombreEnfermedad($nombreEnfermedad) {
        $this->nombreEnfermedad = $nombreEnfermedad;
    }

    function setMedicamentoEspecial($medicamentoEspecial) {
        $this->medicamentoEspecial = $medicamentoEspecial;
    }

    function setNombreMedicamentoEspecial($nombreMedicamentoEspecial) {
        $this->nombreMedicamentoEspecial = $nombreMedicamentoEspecial;
    }

    function setTransporteEscolar($transporteEscolar) {
        $this->transporteEscolar = $transporteEscolar;
    }

    function setTipoRuta($tipoRuta) {
        $this->tipoRuta = $tipoRuta;
    }

    function setDireccionRuta($direccionRuta) {
        $this->direccionRuta = $direccionRuta;
    }

    function setBarrioRuta($barrioRuta) {
        $this->barrioRuta = $barrioRuta;
    }

    function setRestauranteEscolar($restauranteEscolar) {
        $this->restauranteEscolar = $restauranteEscolar;
    }

    function setManoDominante($manoDominante) {
        $this->manoDominante = $manoDominante;
    }

    function setUsaGafas($usaGafas) {
        $this->usaGafas = $usaGafas;
    }

    function setDeficienciaVisual($deficienciaVisual) {
        $this->deficienciaVisual = $deficienciaVisual;
    }

    function setFractura($fractura) {
        $this->fractura = $fractura;
    }

    function setDetalleFractura($detalleFractura) {
        $this->detalleFractura = $detalleFractura;
    }

    function setAspectosEspeciales($aspectosEspeciales) {
        $this->aspectosEspeciales = $aspectosEspeciales;
    }

    /**
     * Obtiene el formulario de la BD, dado el PIN.
     * @param Pin $pin <p>PIN del formulario.</p>
     * @return \Formulario
     */
    public static function getFormulario($pin) {
        $anio = substr($pin, 0, 4);
        $numero = substr($pin, 4);
        $sql = "SELECT DISTINCT Formulario.*, Foto.imagen, Foto.tipo, Pin.pagado, Pin.utilizado, Pin.numeroFormulario, TiposDocumento.nombre nombreDocumento "
                . "FROM Foto RIGHT JOIN Formulario ON Formulario.idFoto = Foto.id "
                . "LEFT JOIN Pin ON (Formulario.anioPin = Pin.anio AND Formulario.numeroPin = Pin.numero) "
                . "LEFT JOIN TiposDocumento ON (TiposDocumento.documento = Formulario.tid) "
                . "WHERE Pin.anio = '$anio' AND Pin.numero = '$numero' AND Pin.utilizado = 'S' ";
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $foto = new Foto($fila['idFoto'], $fila['imagen'], $fila['tipo']);
            $pin = new Pin($fila['anioPin'], $fila['numeroPin'], $fila['pagado'], $fila['utilizado'], $fila['numeroFormulario']);
            $tipoDocumento = new TipoDocumento($fila['tid'], $fila['nombreDocumento']);
            $response = new Formulario($fila['id'], $pin, $fila['primerApellido'], $fila['segundoApellido'], $fila['nombres'], $tipoDocumento, $fila['nid'], $foto, $fila['lugarNacimiento'], $fila['fechaNacimiento'], $fila['edadCumplida'], $fila['nacionalidad'], $fila['direccionResidencia'], $fila['barrio'], $fila['telefono'], $fila['gradoIngresar'], $fila['gradoActual'], $fila['conocidoEstudia'], $fila['impedimentoFisico'], $fila['nombreImpedimentoFisico'], $fila['tenidoAyuda'], $fila['nombreAyuda'], $fila['enfermedad'], $fila['nombreEnfermedad'], $fila['medicamentoEspecial'], $fila['nombreMedicamentoEspecial'], $fila['transporteEscolar'], $fila['tipoRuta'], $fila['direccionRuta'], $fila['barrioRuta'], $fila['restauranteEscolar'], $fila['manoDominante'], $fila['usaGafas'], $fila['deficienciaVisual'], $fila['fractura'], $fila['detalleFractura'], $fila['aspectosEspeciales'], $fila['grupoSanguineo'], $fila['rh'], $fila['estrato'], $fila['eps'], $fila['lugarExpedicionDocumento']);
        }
        return $response;
    }

}
