<?php

include_once 'persistance/database.php';

class TipoDocumento {

    private $documento, $nombre;

    public function TipoDocumento($documento, $nombre) {
        $this->setDocumento($documento);
        $this->setNombre($nombre);
    }

    function getDocumento() {
        return $this->documento;
    }

    function setDocumento($documento) {
        $this->documento = $documento;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    /**
     * Trae de la BD los documentos registrados.
     * @return type
     */
    public static function traerDocumentos() {
        return TipoDocumento::getDocumentos();
    }

    /**
     * Trae de la BD el documento ingresado.
     * @param type $doc
     * @return type
     */
    public static function traerDocumento($doc) {
        $filtro = "WHERE documento = '$doc'";
        return TipoDocumento::getDocumentos($filtro)[0];
    }

    /**
     * Trae los documentos registrados en la BD.
     * @param type $filtro
     * @return \TipoDocumento
     */
    private static function getDocumentos($filtro = "") {
        $sql = "SELECT * FROM TiposDocumento ";
        $result = getResultSet($sql . $filtro);
        $documentos = array();
        if ($result->num_rows > 0) {
            while ($fila = mysqli_fetch_array($result)) {
                $documento = new TipoDocumento($fila['documento'], $fila['nombre']);
                $documentos[] = $documento;
            }
        }
        return $documentos;
    }

}
