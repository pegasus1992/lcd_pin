<?php

class Foto {

    private $id, $nombre, $tipo;

    public function Foto($id, $nombre, $tipo) {
        $this->setId($id);
        $this->setNombre($nombre);
        $this->setTipo($tipo);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

}
