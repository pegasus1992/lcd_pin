<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once 'Foto.php';
include_once 'TipoDocumento.php';

class Progenitor {

    private $primerApellido, $segundoApellido, $nombres, $tid, $nid, $foto;
    private $lugarNacimiento, $fechaNacimiento, $edadCumplida;
    private $profesion, $empresa, $celular, $telefono, $extension, $cargo, $ingresosMensuales, $trabajo;
    private $estudiosPrimarios, $estudiosSecundarios, $estudiosUniversitarios, $estudiosOtros;
    private $resideEn, $tipoResidencia, $email, $intereses;

    public function Progenitor($primerApellido, $segundoApellido, $nombres, $tid, $nid, $foto, $lugarNacimiento, $fechaNacimiento, $edadCumplida, $profesion, $empresa, $celular, $telefono, $extension, $cargo, $ingresosMensuales, $trabajo, $estudiosPrimarios, $estudiosSecundarios, $estudiosUniversitarios, $estudiosOtros, $resideEn, $tipoResidencia, $email, $intereses) {
        $this->setPrimerApellido($primerApellido);
        $this->setSegundoApellido($segundoApellido);
        $this->setNombres($nombres);
        $this->setTid($tid);
        $this->setNid($nid);
        $this->setFoto($foto);
        $this->setLugarNacimiento($lugarNacimiento);
        $this->setFechaNacimiento($fechaNacimiento);
        $this->setEdadCumplida($edadCumplida);
        $this->setProfesion($profesion);
        $this->setEmpresa($empresa);
        $this->setCelular($celular);
        $this->setTelefono($telefono);
        $this->setExtension($extension);
        $this->setCargo($cargo);
        $this->setIngresosMensuales($ingresosMensuales);
        $this->setTrabajo($trabajo);
        $this->setEstudiosPrimarios($estudiosPrimarios);
        $this->setEstudiosSecundarios($estudiosSecundarios);
        $this->setEstudiosUniversitarios($estudiosUniversitarios);
        $this->setEstudiosOtros($estudiosOtros);
        $this->setResideEn($resideEn);
        $this->setTipoResidencia($tipoResidencia);
        $this->setEmail($email);
        $this->setIntereses($intereses);
    }

    function getPrimerApellido() {
        return $this->primerApellido;
    }

    function getSegundoApellido() {
        return $this->segundoApellido;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getTid() {
        return $this->tid;
    }

    function getNid() {
        return $this->nid;
    }

    function getFoto() {
        return $this->foto;
    }

    function getLugarNacimiento() {
        return $this->lugarNacimiento;
    }

    function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    function getEdadCumplida() {
        return $this->edadCumplida;
    }

    function getProfesion() {
        return $this->profesion;
    }

    function getEmpresa() {
        return $this->empresa;
    }

    function getCelular() {
        return $this->celular;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getExtension() {
        return $this->extension;
    }

    function getCargo() {
        return $this->cargo;
    }

    function getIngresosMensuales() {
        return $this->ingresosMensuales;
    }

    function getTrabajo() {
        return $this->trabajo;
    }

    function getEstudiosPrimarios() {
        return $this->estudiosPrimarios;
    }

    function getEstudiosSecundarios() {
        return $this->estudiosSecundarios;
    }

    function getEstudiosUniversitarios() {
        return $this->estudiosUniversitarios;
    }

    function getEstudiosOtros() {
        return $this->estudiosOtros;
    }

    function getResideEn() {
        return $this->resideEn;
    }

    function getTipoResidencia() {
        return $this->tipoResidencia;
    }

    function getEmail() {
        return $this->email;
    }

    function getIntereses() {
        return $this->intereses;
    }

    function setPrimerApellido($primerApellido) {
        $this->primerApellido = $primerApellido;
    }

    function setSegundoApellido($segundoApellido) {
        $this->segundoApellido = $segundoApellido;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setTid($tid) {
        $this->tid = $tid;
    }

    function setNid($nid) {
        $this->nid = $nid;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setLugarNacimiento($lugarNacimiento) {
        $this->lugarNacimiento = $lugarNacimiento;
    }

    function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = substr($fechaNacimiento, 0, 10);
    }

    function setEdadCumplida($edadCumplida) {
        $this->edadCumplida = $edadCumplida;
    }

    function setProfesion($profesion) {
        $this->profesion = $profesion;
    }

    function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setExtension($extension) {
        $this->extension = $extension;
    }

    function setCargo($cargo) {
        $this->cargo = $cargo;
    }

    function setIngresosMensuales($ingresosMensuales) {
        $this->ingresosMensuales = $ingresosMensuales;
    }

    function setTrabajo($trabajo) {
        $this->trabajo = $trabajo;
    }

    function setEstudiosPrimarios($estudiosPrimarios) {
        $this->estudiosPrimarios = $estudiosPrimarios;
    }

    function setEstudiosSecundarios($estudiosSecundarios) {
        $this->estudiosSecundarios = $estudiosSecundarios;
    }

    function setEstudiosUniversitarios($estudiosUniversitarios) {
        $this->estudiosUniversitarios = $estudiosUniversitarios;
    }

    function setEstudiosOtros($estudiosOtros) {
        $this->estudiosOtros = $estudiosOtros;
    }

    function setResideEn($resideEn) {
        $this->resideEn = $resideEn;
    }

    function setTipoResidencia($tipoResidencia) {
        $this->tipoResidencia = $tipoResidencia;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setIntereses($intereses) {
        $this->intereses = $intereses;
    }

    /**
     * Obtiene el progenitor de la BD, dado su tid y nid.
     * @param type $tid <p>Tid del progenitor.</p>
     * @param type $nid <p>Nid del Progenitor.</p>
     * @return \Progenitor
     */
    public static function obtenerProgenitor($tid, $nid) {
        $sql = "SELECT DISTINCT Progenitor.*, Foto.imagen, Foto.tipo, TiposDocumento.nombre nombreDocumento "
                . "FROM Progenitor LEFT JOIN Foto ON (Progenitor.idFoto = Foto.id) "
                . "LEFT JOIN TiposDocumento ON (TiposDocumento.documento = Progenitor.tid) "
                . "WHERE tid = '$tid' AND nid = '$nid'";
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $foto = new Foto($fila['idFoto'], $fila['imagen'], $fila['tipo']);
            $tipoDocumento = new TipoDocumento($fila['tid'], $fila['nombreDocumento']);
            $response = new Progenitor($fila['primerApellido'], $fila['segundoApellido'], $fila['nombres'], $tipoDocumento, $fila['nid'], $foto, $fila['lugarNacimiento'], $fila['fechaNacimiento'], $fila['edadCumplida'], $fila['profesion'], $fila['empresa'], $fila['celular'], $fila['telefono'], $fila['extension'], $fila['cargo'], $fila['ingresosMensuales'], $fila['trabajo'], $fila['estudiosPrimarios'], $fila['estudiosSecundarios'], $fila['estudiosUniversitarios'], $fila['estudiosOtros'], $fila['resideEn'], $fila['tipoResidencia'], $fila['email'], $fila['intereses']);
        }
        return $response;
    }

}
