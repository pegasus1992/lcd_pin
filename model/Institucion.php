<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';

class Institucion {

    private $nombre, $grado, $anio, $telefono;

    public function Institucion($nombre, $grado, $anio, $telefono) {
        $this->setNombre($nombre);
        $this->setGrado($grado);
        $this->setAnio($anio);
        $this->setTelefono($telefono);
    }

    function getNombre() {
        return $this->nombre;
    }

    function getGrado() {
        return $this->grado;
    }

    function getAnio() {
        return $this->anio;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setGrado($grado) {
        $this->grado = $grado;
    }

    function setAnio($anio) {
        $this->anio = $anio;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    /**
     * Obtiene las instituciones registradas en el formulario adicional de ld BD.
     * @param type $idFormulario
     * @return \Institucion
     */
    public static function obtenerInstituciones($idFormulario) {
        $sql = "SELECT * FROM Institucion "
                . "LEFT JOIN FormularioAdicional ON (FormularioAdicional.id = Institucion.idFormulario) "
                . "WHERE idFormulario = $idFormulario";
        $result = getResultSet($sql);
        $instituciones = null;
        if ($result->num_rows > 0) {
            $instituciones = array();
            while ($fila = mysqli_fetch_array($result)) {
                $institucion = new Institucion($fila['nombre'], $fila['grado'], $fila['anio'], $fila['telefono']);
                $instituciones[] = $institucion;
            }
        }
        return $instituciones;
    }

}
