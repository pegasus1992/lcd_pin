<?php

class Archivo {

    private $id, $binario, $nombre, $peso, $tipo;

    public function Archivo($id, $binario, $nombre, $peso, $tipo) {
        $this->setId($id);
        $this->setBinario($binario);
        $this->setNombre($nombre);
        $this->setPeso($peso);
        $this->setTipo($tipo);
    }

    function getId() {
        return $this->id;
    }

    function getBinario() {
        return $this->binario;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getPeso() {
        return $this->peso;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBinario($binario) {
        $this->binario = $binario;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    /**
     * Obtiene de la BD el archivo dado su id.
     * @param type $id <p>Id del archivo.</p>
     * @return \Archivo
     */
    public static function getArchivo($id) {
        $sql = "SELECT * FROM Archivo WHERE id = $id";
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $response = new Archivo($fila['id'], $fila['binario'], $fila['nombre'], $fila['peso'], $fila['tipo']);
        }
        return $response;
    }

}
