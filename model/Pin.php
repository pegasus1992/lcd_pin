<?php

include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';

class Pin {

    private $anio, $numero, $pagado, $utilizado, $numeroFormulario;

    public function Pin($anio, $numero, $pagado, $utilizado, $numeroFormulario) {
        $this->setAnio($anio);
        $this->setNumero($numero);
        $this->setPagado($pagado);
        $this->setUtilizado($utilizado);
        $this->setNumeroFormulario($numeroFormulario);
    }

    function getNumeroFormulario() {
        return $this->numeroFormulario;
    }

    function setNumeroFormulario($numeroFormulario) {
        $this->numeroFormulario = $numeroFormulario;
    }

    function getAnio() {
        return $this->anio;
    }

    function getNumero() {
        return $this->numero;
    }

    function getPagado() {
        return $this->pagado;
    }

    function getUtilizado() {
        return $this->utilizado;
    }

    function setAnio($anio) {
        $this->anio = $anio;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setPagado($pagado) {
        $this->pagado = $pagado;
    }

    function setUtilizado($utilizado) {
        $this->utilizado = $utilizado;
    }

    private static function getSqlPines($filtro = "") {
        return "SELECT DISTINCT anio, numero, pagado, utilizado, numeroFormulario FROM Pin " . $filtro;
    }

    private static function getPinBD($fila) {
        $pin = new Pin($fila['anio'], $fila['numero'], $fila['pagado'], $fila['utilizado'], $fila['numeroFormulario']);
        return $pin;
    }

    /**
     * Obtiene de la BD el pin y su numero de formulario asociado (el maximo).
     * @return type
     */
    public static function getPinMaximoFormulario() {
        $filtro = "WHERE numeroFormulario = (SELECT MAX(numeroFormulario) FROM Pin) ";
        $sql = Pin::getSqlPines($filtro);
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $response = Pin::getPinBD($fila);
        }
        return $response;
    }

    /**
     * Obtiene de la BD los pines y sus correspondientes numeros de formulario asociados.
     * @return type
     */
    public static function getPines() {
        $sql = Pin::getSqlPines();
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $response = array();
            while ($fila = mysqli_fetch_array($result)) {
                $pin = Pin::getPinBD($fila);
                $response[] = $pin;
            }
        }
        return $response;
    }

    /**
     * Obtiene de la BD el pin y su numero de formulario asociado.
     * @param type $anio <p>Anio de admisiones.</p>
     * @param type $numero <p>Numero aleatorio del pin.</p>
     * @return \Pin
     */
    public static function getPin($anio, $numero) {
        $filtro = "WHERE anio = '$anio' AND numero = '$numero' ";
        $sql = Pin::getSqlPines($filtro);
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $fila = mysqli_fetch_array($result);
            $response = Pin::getPinBD($fila);
        }
        return $response;
    }

    /**
     * Obtiene de la BD los pines sin utilizar y sus correspondientes numeros de formulario asociados.
     * @return type
     */
    public static function getPinesSinUtilizar() {
        $filtro = "WHERE utilizado = 'N' ";
        $sql = Pin::getSqlPines($filtro);
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $response = array();
            while ($fila = mysqli_fetch_array($result)) {
                $pin = Pin::getPinBD($fila);
                $response[] = $pin;
            }
        }
        return $response;
    }

    /**
     * Obtiene de la BD los pines utilizados y sus correspondientes numeros de formulario asociados.
     * @return type
     */
    public static function getPinesUtilizados() {
        $filtro = "WHERE utilizado = 'S' ";
        $sql = Pin::getSqlPines($filtro);
        $result = getResultSet($sql);
        $response = null;
        if ($result->num_rows > 0) {
            $response = array();
            while ($fila = mysqli_fetch_array($result)) {
                $pin = Pin::getPinBD($fila);
                $response[] = $pin;
            }
        }
        return $response;
    }

    /**
     * Verifica si existe el pin ingresado y si no esta utilizado.
     * @param type $pin
     * @return boolean
     */
    public static function existePinSinUtilizar($pin) {
        $response = false;
        if (strlen($pin) == LONGITUD_PIN) {
            $anio = substr($pin, 0, 4);
            $numero = substr($pin, 4);
            $pines = Pin::getPinesSinUtilizar();

            foreach ($pines as $pin) {
                if ($anio == $pin->getAnio() && $numero == $pin->getNumero()) {
                    $response = true;
                    break;
                }
            }
        }
        return $response;
    }

    /**
     * Verifica si existe el pin ingresado y si no esta utilizado.
     * @param type $pin
     * @return boolean
     */
    public static function existePinUtilizado($pin) {
        $response = false;
        if (strlen($pin) == LONGITUD_PIN) {
            $anio = substr($pin, 0, 4);
            $numero = substr($pin, 4);
            $pines = Pin::getPinesUtilizados();

            foreach ($pines as $pin) {
                if ($anio == $pin->getAnio() && $numero == $pin->getNumero()) {
                    $response = true;
                    break;
                }
            }
        }
        return $response;
    }
}
