<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/FormularioAdicional.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Mostrar Formulario - Información Adicional</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        if (strlen($pin) == LONGITUD_PIN) {
            $formulario = FormularioAdicional::obtenerFormularioAdicional($pin);
            if ($formulario != null) {
                $hermanos = $formulario->getHermanos();
                $instituciones = $formulario->getInstituciones();
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro2">
                    <h4 align="center">Mostrar Formulario - Información Adicional</h4>
                    <div>
                        <label>Pin:</label>
                        <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                    </div>

                    <?php
                    $tipoUnion = $formulario->getTipoUnion();
                    echo '<div>';
                    echo '<label>Actualmente los Padres están:</label>';
                    echo '<input name="estadoCivil" id="estadoCivil" type="text" value="' . $tipoUnion . '" readonly/>';
                    echo '</div>';
                    if ($tipoUnion == "Separados") {
                        echo '<div>';
                        echo '<label>El Educando vive con:</label>';
                        echo '<input name="separados" id="separados" type="text" value="' . $formulario->getSeparados() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $adoptado = $formulario->getAdoptado();
                    echo '<div>';
                    echo '<label>¿El Educando aspirante es Adoptado?</label>';
                    if ($adoptado == "S") {
                        echo '<input name="adoptadoRadio" id="adoptadoRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="adoptadoRadio" id="adoptadoRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($adoptado == "S") {
                        echo '<div>';
                        echo '<label>¿Qué edad tenía en el momento de la adopción?</label>';
                        echo '<input name="edadAdoptado" id="edadAdoptado" type="number" value="' . $formulario->getEdadAdoptado() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    if ($hermanos != null) {
                        echo '<br/>'
                        . '<table id="hermanosTabla" border="0" class="asociados">'
                        . '<tr>'
                        . '<th>Nombres de los hermanos(as)</th>'
                        . '<th>Apellidos de los hermanos(as)</th>'
                        . '<th>Edad</th>'
                        . '<th>Colegio o Institución donde estudia</th>'
                        . '<th>Grado</th>'
                        . '</tr>';
                        foreach ($hermanos as $hermano) {
                            echo '<tr>'
                            . '<td><input id=nombresHermano type=text name=hermanos[] value="' . $hermano->getNombres() . '" readonly/></td>'
                            . '<td><input id=apellidosHermano type=text name=hermanos[] value="' . $hermano->getApellidos() . '" readonly/></td>'
                            . '<td><input id=edadHermano type=number name=hermanos[] value="' . $hermano->getEdad() . '" readonly/></td>'
                            . '<td><input id=colegioHermano type=text name=hermanos[] value="' . $hermano->getColegio() . '" readonly/></td>'
                            . '<td><input id=gradoHermano type=text name=hermanos[] value="' . $hermano->getGrado() . '" readonly/></td>'
                            . '</tr>';
                        }
                        echo '</table><br/>';
                    }
                    ?>

                    <div>
                        <label>En caso de emergencia, señale dirección y número telefónico donde se pueden localizar papá y/o mamá de manera inmediata:</label>
                        <?php echo '<textarea cols="40" rows="5" name="direccionTelefonoEmergencia" id="direccionTelefonoEmergencia" type="text" readonly>' . $formulario->getDireccionTelefonoEmergencia() . '</textarea>' ?>
                    </div>
                    <div>
                        <label>Nombre, Dirección y Teléfono de Familiar(es) cercano(s) que puedan acudir al Liceo de manera inmediata, en caso de emergencia:</label>
                        <?php echo '<textarea cols="40" rows="7" name="direccionTelefonoFamiliar" id="direccionTelefonoFamiliar" type="text" readonly>' . $formulario->getDireccionTelefonoFamiliar() . '</textarea>' ?>
                    </div>

                    <div>
                        <label>¿De manera directa, quién responderá con los compromisos formativos, académicos y económicos ante el Liceo Campo David?</label>
                        <?php echo '<input name="personaResponde" id="personaResponde" type="text" value="' . $formulario->getPersonaResponde() . '" readonly/>' ?>
                    </div>

                    <?php
                    $padresFirman = $formulario->getPadresFirman();
                    echo '<div>';
                    echo '<label>¿Ambos padres firmarán la matrícula y el contrato de Cooperación Educativa?</label>';
                    if ($padresFirman == "S") {
                        echo '<input name="padresFirmanRadio" id="padresFirmanRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="padresFirmanRadio" id="padresFirmanRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($padresFirman == "N") {
                        echo '<div>';
                        echo '<label>¿Quién?</label>';
                        echo '<input name="nombreFirma" id="nombreFirma" type="text" value="' . $formulario->getNombreFirma() . '" readonly/>';
                        echo '</div>';

                        $autorizaInformacion = $formulario->getAutorizaInformacion();
                        echo '<div>';
                        echo '<label>¿Autoriza dar información verbal y escrita al Padre o Madre ausente?</label>';
                        if ($autorizaInformacion == "S") {
                            echo '<input name="autorizaInformacionRadio" id="autorizaInformacionRadio" type="text" value="Sí" readonly/>';
                        } else {
                            echo '<input name="autorizaInformacionRadio" id="autorizaInformacionRadio" type="text" value="No" readonly/>';
                        }
                        echo '</div>';

                        echo '<div>';
                        echo '<label>Explique:</label>';
                        echo '<input name="autorizaExplique" id="autorizaExplique" type="text" value="' . $formulario->getAutorizaExplique() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $miembroReferencia = $formulario->getMiembroReferencia();
                    if ($miembroReferencia != null) {
                        echo '<div>';
                        echo '<label>¿Qué miembro de la Comunidad Liceísta puede dar referencia de la familia que solicita admisión?</label>';
                        echo '<input name="miembroReferencia" id="miembroReferencia" type="text" value="' . $miembroReferencia . '" readonly/>';
                        echo '</div>';

                        echo '<div>';
                        echo '<label>Padres del Educando:</label>';
                        echo '<textarea cols="40" rows="3" name="padresReferencia" id="padresReferencia" type="text" readonly>' . $formulario->getPadresReferencia() . '</textarea>';
                        echo '</div>';
                        echo '<div>';
                        echo '<label>Del Grado:</label>';
                        echo '<input name="gradoReferencia" id="gradoReferencia" type="text" value="' . $formulario->getGradoReferencia() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <div>
                        <label>¿Qué características del Liceo Campo David le han motivado para solicitar esta admisión?</label>
                        <?php echo '<textarea cols="40" rows="7" name="caracteristicas" id="caracteristicas" type="text" readonly>' . $formulario->getCaracteristicas() . '</textarea>' ?>
                    </div>

                    <?php
                    if ($instituciones != null) {
                        echo '<br/>'
                        . '<table id="hermanosTabla" border="0" class="asociados">'
                        . '<tr>'
                        . '<th>Nombre de la Institución</th>'
                        . '<th>Grado</th>'
                        . '<th>Año en que lo cursó</th>'
                        . '<th>Teléfono</th>'
                        . '</tr>';
                        foreach ($instituciones as $institucion) {
                            echo '<tr>'
                            . '<td><input id=nombreInstitucion type=text name=instituciones[] value="' . $institucion->getNombre() . '" readonly/></td>'
                            . '<td><input id=gradoInstitucion type=text name=instituciones[] value="' . $institucion->getGrado() . '" readonly/></td>'
                            . '<td><input id=anioInstitucion type=number name=instituciones[] value="' . $institucion->getAnio() . '" readonly/></td>'
                            . '<td><input id=telefonoInstitucion type=text name=instituciones[] value="' . $institucion->getTelefono() . '" readonly/></td>'
                            . '</tr>';
                        }
                        echo '</table><br/>';
                    }
                    ?>

                    <div>
                        <label>¿En pocas palabras, cuál es la Visión Educativa ideal para su Hijo(a)?</label>
                        <?php echo '<textarea cols="40" rows="5" name="visionEducativa" id="visionEducativa" type="text" readonly>' . $formulario->getVisionEducativa() . '</textarea>' ?>
                    </div>

                    <?php
                    $escolaridadAnterior = $formulario->getEscolaridadAnterior();
                    echo '<div>';
                    echo '<label>¿Posee escolaridad anterior?</label>';
                    if ($escolaridadAnterior == "S") {
                        echo '<input name="escolaridadRadio" id="escolaridadRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="escolaridadRadio" id="escolaridadRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($escolaridadAnterior == "S") {
                        $pazSalvo = $formulario->getPazSalvo();
                        if ($pazSalvo != null) {
                            echo '<div>';
                            $idPazSalvo = $pazSalvo->getId();
                            echo '<input name="pazSalvo" id="pazSalvoMostrar" type="button" value="Descargar Paz y Salvo" class="Button2" '
                            . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idPazSalvo" . '\'"/>';
                            echo '</div>';
                        }

                        $ultimoBoletin = $formulario->getUltimoBoletin();
                        if ($ultimoBoletin != null) {
                            echo '<div>';
                            $idUltimoBoletin = $ultimoBoletin->getId();
                            echo '<input name="ultimoBoletin" id="ultimoBoletinMostrar" type="button" value="Descargar Último Boletin" class="Button2" '
                            . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idUltimoBoletin" . '\'"/>';
                            echo '</div>';
                        }
                    }
                    ?>

                    <?php
                    $certificadoIngresos = $formulario->getCertificadoIngresos();
                    if ($certificadoIngresos != null) {
                        echo '<div>';
                        $idCertificadoIngresos = $certificadoIngresos->getId();
                        echo '<input name="certificadoIngresos" id="certificadoIngresosMostrar" type="button" value="Descargar Certificado de Ingresos" class="Button2" '
                        . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idCertificadoIngresos" . '\'"/>';
                        echo '</div>';
                    }
                    ?>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <style type="text/css" media="print">
                        @page { size: letter; }
                    </style>
                    <div align="center">
                        <!--input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/-->
                        <?php echo '<input name="formulario" type="button" value="Ver Información del Educando" onclick="window.location = \'mostrarFormulario.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <?php echo '<input name="formulario" type="button" value="Ver Información de los Padres" onclick="window.location = \'mostrarFormularioPadres.php?pin=' . $pin . '\'" class="Button2">' ?>
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin no encontrado</h4>
                    <?php
                    echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no válido</h4>
                <?php
                echo "<p>Se ha ingresado un Pin no válido.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'consultarPines.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
    </body>
</html>