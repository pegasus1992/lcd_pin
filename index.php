<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './model/Pin.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Página de Admisiones</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div style="width:1200px" class="texto2">
            <h1 class="Titulo">Liceo Campo David</h1>
            <h3 class="Subtitulo">Página de Admisiones</h3>
        </div>
        <br/>
        <br/>
        <div style="width:500px" class="texto2">
            <h4>Gestión de Pines</h4>
            <?php
            $pin = Pin::getPinMaximoFormulario();
            if ($pin->getNumeroFormulario() < MAX_NUM_FORM) {
                ?>
                <input type="button" class="Button2" value="Generar Pin" onclick="window.location = 'generarPin.php'">
                <?php
            }
            ?>
            <input type="button" class="Button2" value="Consultar Pines" onclick="window.location = 'consultarPines.php'">
            <input type="button" class="Button2" value="Imprimir Pines" onclick="window.location = 'imprimirPines.php'">
            <br/>
            <br/>
        </div>
        <div style="width:500px" class="texto2">
            <h4>Gestión de Formularios</h4>
            <input type="button" class="Button2" value="Diligenciar Formulario" onclick="window.location = 'ingresarPinRegistrar.php'">
            <input type="button" class="Button2" value="Consultar Formulario Completo" onclick="window.location = 'ingresarPinConsultar.php'">
            <br/>
            <br/>
        </div>
    </body>
</html>
