<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './persistance/database.php';
include_once './model/TipoDocumento.php';
include_once './model/Pin.php';
include_once './model/Formulario.php';
include_once './model/Foto.php';
include_once './model/FormularioPadres.php';
include_once './model/FormularioAdicional.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language" content="es" />
        <?php
        echo '<title>' . PROJECT_NAME . ' - Mostrar Formulario</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        if (strlen($pin) == LONGITUD_PIN) {
            $formulario = Formulario::getFormulario($pin);
            $formularioPadres = FormularioPadres::obtenerFormularioPadres($pin);
            $formularioAdicional = FormularioAdicional::obtenerFormularioAdicional($pin);
            if ($formulario != null && $formularioPadres != null && $formularioAdicional != null) {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro2">
                    <?php echo '<h3 align="center">Formulario N° ' . $formulario->getPin()->getNumeroFormulario() . '</h3>'; ?>
                    <?php echo '<h5 align="center">PIN ' . $pin . '</h5>'; ?>
                    <br>

                    <!-- INFORMACION DEL EDUCANDO -->
                    <h4 align="center">Información del Educando</h4>
                    <div class="espaciado">
                        <label>Fotografía del Niño:</label>
                        <?php
                        $fotoNino = $formulario->getFoto();
                        if ($fotoNino->getId() != null) {
                            echo imageField("imagenNino", $fotoNino->getId());
                        } else {
                            echo imageField("imagenNino");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido del Niño:</label>
                        <?php echo '<input name="primerApellidoNino" id="primerApellidoNino" type="text" value="' . $formulario->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido del Niño:</label>
                        <?php echo '<input name="segundoApellidoNino" id="segundoApellidoNino" type="text" value="' . $formulario->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres del Niño:</label>
                        <?php echo '<input name="nombresNino" id="nombresNino" type="text" value="' . $formulario->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoNino" id="lugarNacimientoNino" type="text" value="' . $formulario->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoNino" id="fechaNacimientoNino" type="date" value="' . $formulario->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadNino" id="edadNino" type="number" value="' . $formulario->getEdadCumplida() . '" readonly/>' ?>
                    </div>

                    <?php
                    $grupoSanguineo = $formulario->getGrupoSanguineo();
                    $rh = $formulario->getRh();

                    echo '<div>';
                    echo '<label>Grupo Sanguíneo:</label>';
                    echo '<grupoRH>';
                    echo '<input name="grupoSanguineoNino" id="grupoSanguineo" type="text" value="' . $grupoSanguineo . '" readonly/>';

                    if ($rh == "P") {
                        echo '<input name="rhNino" id="rh" type="text" value="Positivo" readonly/>';
                    } else {
                        echo '<input name="rhNino" id="rh" type="text" value="Negativo" readonly/>';
                    }
                    echo '</grupoRH>';
                    echo '</div>';
                    ?>

                    <div>
                        <label>Estrato donde vive:</label>
                        <?php echo '<input name="estratoNino" id="estratoNino" type="number" value="' . $formulario->getEstrato() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>EPS a la que está afiliado:</label>
                        <?php echo '<input name="epsNino" id="epsNino" type="text" value="' . $formulario->getEps() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidNino" id="tidNino" type="text" value="' . $formulario->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidNino" id="nidNino" type="text" value="' . $formulario->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de expedición del documento:</label>
                        <?php echo '<input name="lugarExpedicionNino" id="lugarExpedicionNino" type="text" value="' . $formulario->getLugarExpedicion() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nacionalidad:</label>
                        <?php echo '<input name="nacionalidadNino" id="nacionalidadNino" type="text" value="' . $formulario->getNacionalidad() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Dirección de Residencia:</label>
                        <?php echo '<input name="direccionNino" id="direccionNino" type="text" value="' . $formulario->getDireccionResidencia() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Barrio:</label>
                        <?php echo '<input name="barrioNino" id="barrioNino" type="text" value="' . $formulario->getBarrio() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Teléfono:</label>
                        <?php echo '<input name="telefonoNino" id="telefonoNino" type="text" value="' . $formulario->getTelefono() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Grado al que desea Ingresar:</label>
                        <?php echo '<input name="gradoDeseaNino" id="gradoDeseaNino" type="text" value="' . $formulario->getGradoIngresar() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Grado que cursa en el Presente Año:</label>
                        <?php echo '<input name="gradoActualNino" id="gradoActualNino" type="text" value="' . $formulario->getGradoActual() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Estudia en el Liceo Campo David:</label>
                        <?php echo '<input name="conocidoNino" id="conocidoNino" type="text" value="' . $formulario->getConocidoEstudia() . '" readonly/>' ?>
                    </div>

                    <?php
                    $impedimento = $formulario->getImpedimentoFisico();
                    echo '<div>';
                    echo '<label>¿El educando tiene algún tipo de impedimento físico, psicológico o mental?</label>';
                    if ($impedimento == "S") {
                        echo '<input name="impedimentoRadio" id="impedimentoRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="impedimentoRadio" id="impedimentoRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($impedimento == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="nombreImpedimentoNino" id="nombreImpedimentoNino" type="text" value="' . $formulario->getNombreImpedimentoFisico() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $ayuda = $formulario->getTenidoAyuda();
                    echo '<div>';
                    echo '<label>¿Ha tenido el educando aspirante algún tipo de ayuda psicológica, de fonoaudiología y/o terapia ocupacional?</label>';
                    if ($ayuda == "S") {
                        echo '<input name="tenidoAyudaRadio" id="tenidoAyudaRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="tenidoAyudaRadio" id="tenidoAyudaRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($ayuda == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<textarea cols="40" rows="5" name="nombreAyudaNino" id="nombreAyudaNino" type="text" readonly>' . $formulario->getNombreAyuda() . '</textarea>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $enfermedad = $formulario->getEnfermedad();
                    echo '<div>';
                    echo '<label>¿Padece el educando alguna enfermedad de consideración?</label>';
                    if ($enfermedad == "S") {
                        echo '<input name="enfermedadRadio" id="enfermedadRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="enfermedadRadio" id="enfermedadRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($enfermedad == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="enfermedadNino" id="enfermedadNino" type="text" value="' . $formulario->getNombreEnfermedad() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $medicamento = $formulario->getMedicamentoEspecial();
                    echo '<div>';
                    echo '<label>¿Debe ingerir diaria o periódicamente algún medicamento especial?</label>';
                    if ($medicamento == "S") {
                        echo '<input name="medicamentoEspecialRadio" id="medicamentoEspecialRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="medicamentoEspecialRadio" id="medicamentoEspecialRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($medicamento == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        echo '<input name="medicamentoEspecialNino" id="medicamentoEspecialNino" type="text" value="' . $formulario->getNombreMedicamentoEspecial() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $transporte = $formulario->getTransporteEscolar();
                    echo '<div>';
                    echo '<label>¿Requiere servicio de Transporte Escolar?</label>';
                    if ($transporte == "S") {
                        echo '<input name="transporteEscolarRadio" id="transporteEscolarRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="transporteEscolarRadio" id="transporteEscolarRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($transporte == "S") {
                        $tipoRuta = $formulario->getTipoRuta();
                        echo '<div>';
                        echo '<label>(*) ¿Cuál?</label>';
                        if ($tipoRuta == "M") {
                            echo '<input name="tipoRutaRadio" id="tipoRutaRadio" type="text" value="Media Ruta" readonly/>';
                        } else {
                            echo '<input name="tipoRutaRadio" id="tipoRutaRadio" type="text" value="Ruta Completa" readonly/>';
                        }
                        echo '</div>';

                        echo '<div>';
                        echo '<label>(*) Dirección</label>';
                        echo '<input name="direccionRutaNino" id="direccionRutaNino" type="text" value="' . $formulario->getDireccionRuta() . '" readonly/>';
                        echo '</div>';

                        echo '<div>';
                        echo '<label>(*) Barrio</label>';
                        echo '<input name="barrioRutaNino" id="barrioRutaNino" type="text" value="' . $formulario->getBarrioRuta() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $restaurante = $formulario->getRestauranteEscolar();
                    echo '<div>';
                    echo '<label>¿Requiere servicio de Restaurante Escolar?</label>';
                    if ($restaurante == "S") {
                        echo '<input name="restauranteEscolarRadio" id="restauranteEscolarRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="restauranteEscolarRadio" id="restauranteEscolarRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    ?>

                    <?php
                    $mano = $formulario->getManoDominante();
                    echo '<div>';
                    echo '<label>El Educando es:</label>';
                    if ($mano == "Z") {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Zurdo" readonly/>';
                    } else if ($mano == "D") {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Diestro" readonly/>';
                    } else {
                        echo '<input name="manoDominanteRadio" id="manoDominanteRadio" type="text" value="Ambidiestro" readonly/>';
                    }
                    echo '</div>';
                    ?>

                    <?php
                    $usaGafas = $formulario->getUsaGafas();
                    echo '<div>';
                    echo '<label>¿Usa gafas o lentes formulados por un especialista?</label>';
                    if ($usaGafas == "S") {
                        echo '<input name="usaGafasRadio" id="usaGafasRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="usaGafasRadio" id="usaGafasRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($usaGafas == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Qué deficiencia visual presenta?</label>';
                        echo '<input name="deficienciaVisualNino" id="deficienciaVisualNino" type="text" value="' . $formulario->getDeficienciaVisual() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $fractura = $formulario->getFractura();
                    echo '<div>';
                    echo '<label>¿Ha sufrido alguna fractura?</label>';
                    if ($fractura == "S") {
                        echo '<input name="fracturaRadio" id="fracturaRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="fracturaRadio" id="fracturaRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($fractura == "S") {
                        echo '<div>';
                        echo '<label>(*) ¿Qué deficiencia visual presenta?</label>';
                        echo '<input name="detalleFracturaNino" id="detalleFracturaNino" type="text" value="' . $formulario->getDetalleFractura() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>
                    <?php
                    $aspectos = $formulario->getAspectosEspeciales();
                    if ($aspectos != null) {
                        echo '<div>';
                        echo '<label>¿Desea señalar algún aspecto especial del estado de su hijo(a) que considere Ud. importante y además, la deba conocer el Liceo?</label>';
                        echo '<textarea cols="40" rows="5" name="aspectosEspecialesNino" id="aspectosEspecialesNino" type="text" readonly>' . $aspectos . '</textarea>';
                        echo '</div>';
                    }
                    ?>

                    <!-- INFORMACION DE LOS PADRES -->
                    <hr>
                    <?php
                    $madre = $formularioPadres->getMadre();
                    $padre = $formularioPadres->getPadre();
                    ?>
                    <!----------------- DATOS DE LA MADRE ----------------->
                    <h4 align="center">Información de la Madre</h4>
                    <div class="espaciado">
                        <label>Fotografía de la Madre:</label>
                        <?php
                        $fotoMadre = $madre->getFoto();
                        if ($fotoMadre->getId() != null) {
                            echo imageField("imagenMadre", $fotoMadre->getId());
                        } else {
                            echo imageField("imagenMadre");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Madre:</label>
                        <?php echo '<input name="primerApellidoMadre" id="primerApellidoMadre" type="text" value="' . $madre->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Madre:</label>
                        <?php echo '<input name="segundoApellidoMadre" id="segundoApellidoMadre" type="text" value="' . $madre->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres de la Madre:</label>
                        <?php echo '<input name="nombresMadre" id="nombresMadre" type="text" value="' . $madre->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoMadre" id="lugarNacimientoMadre" type="text" value="' . $madre->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoMadre" id="fechaNacimientoMadre" type="date" value="' . $madre->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadMadre" id="edadMadre" type="number" value="' . $madre->getEdadCumplida() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidMadre" id="tidMadre" type="text" value="' . $madre->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidMadre" id="nidMadre" type="text" value="' . $madre->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <?php echo '<input name="profesionMadre" id="profesionMadre" type="text" value="' . $madre->getProfesion() . '" readonly/>' ?>
                    </div>

                    <?php
                    $empresaMadre = $madre->getEmpresa();
                    echo '<div>';
                    echo '<label>Empresa donde Trabaja:</label>';
                    echo '<input name="empresaMadre" id="empresaMadre" type="text" value="' . $empresaMadre . '" readonly/>';
                    echo '</div>';

                    if ($empresaMadre == "Independiente") {
                        echo '<div>';
                        echo '<label>Trabajo que Realiza:</label>';
                        echo '<input name="trabajoMadre" id="trabajoMadre" type="text" value="' . $madre->getTrabajo() . '" readonly/>';
                        echo '</div>';
                    } else {

                        echo '<div>';
                        echo '<label>Cargo que Desempeña:</label>';
                        echo '<input name="cargoMadre" id="cargoMadre" type="text" value="' . $madre->getCargo() . '" readonly/>';
                        echo '</div>';
                    }

                    $celularMadre = $madre->getCelular();
                    $telefonoMadre = $madre->getTelefono();
                    $extensionMadre = $madre->getExtension();
                    if ($celularMadre != null) {
                        echo '<div>';
                        echo '<label>Celular:</label>';
                        echo '<input name="celularMadre" id="celularMadre" type="text" value="' . $celularMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($telefonoMadre != null) {
                        echo '<div>';
                        echo '<label>Telefono:</label>';
                        echo '<input name="telefonoMadre" id="telefonoMadre" type="text" value="' . $telefonoMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($extensionMadre != null) {
                        echo '<div>';
                        echo '<label>Extensión:</label>';
                        echo '<input name="extensionMadre" id="extensionMadre" type="text" value="' . $extensionMadre . '" readonly/>';
                        echo '</div>';
                    }

                    echo '<div>';
                    echo '<label>Ingresos Mensuales:</label>';
                    echo '<input name="ingresosMadre" id="ingresosMadre" type="text" value="' . $madre->getIngresosMensuales() . '" readonly/>';
                    echo '</div>';
                    ?>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <?php
                    $estudiosPrimariosMadre = $madre->getEstudiosPrimarios();
                    $estudiosSecundariosMadre = $madre->getEstudiosSecundarios();
                    $estudiosUniversitariosMadre = $madre->getEstudiosUniversitarios();
                    $estudiosOtrosMadre = $madre->getEstudiosOtros();
                    if ($estudiosPrimariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Primarios:</label>';
                        echo '<input name="estudiosPrimariosMadre" id="estudiosPrimariosMadre" type="text" value="' . $estudiosPrimariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosSecundariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Secundarios:</label>';
                        echo '<input name="estudiosSecundariosMadre" id="estudiosSecundariosMadre" type="text" value="' . $estudiosSecundariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosUniversitariosMadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Universitarios:</label>';
                        echo '<input name="estudiosUniversitariosMadre" id="estudiosUniversitariosMadre" type="text" value="' . $estudiosUniversitariosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosOtrosMadre != null) {
                        echo '<div>';
                        echo '<label>* Otros Estudios:</label>';
                        echo '<input name="estudiosOtrosMadre" id="estudiosOtrosMadre" type="text" value="' . $estudiosOtrosMadre . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $resideEnMadre = $madre->getResideEn();
                    $tipoResidenciaMadre = $madre->gettipoResidencia();
                    echo '<div>';
                    echo '<label>Reside en:</label>';
                    echo '<radio>';
                    if ($resideEnMadre == "C") {
                        echo '<input name="resideEnRadioMadre" id="resideEnRadioMadre" type="text" value="Casa" readonly/>';
                    } else {
                        echo '<input name="resideEnRadioMadre" id="resideEnRadioMadre" type="text" value="Apartamento" readonly/>';
                    }
                    echo '<br/><br/>';
                    if ($tipoResidenciaMadre == "P") {
                        echo '<input name="tipoResidenciaRadioMadre" id="tipoResidenciaRadioMadre" type="text" value="Propia" readonly/>';
                    } else {
                        echo '<input name="tipoResidenciaRadioMadre" id="tipoResidenciaRadioMadre" type="text" value="Arriendo" readonly/>';
                    }
                    echo '</radio></div>';
                    ?>

                    <div>
                        <label>E-mail:</label>
                        <?php echo '<input name="correoMadre" id="correoMadre" type="email" value="' . $madre->getEmail() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <?php echo '<textarea cols="40" rows="5" name="interesesMadre" id="interesesMadre" type="text" readonly>' . $madre->getIntereses() . '</textarea>' ?>
                    </div>

                    <!----------------- DATOS DEL PADRE ----------------->
                    <hr>
                    <h4 align="center">Información del Padre</h4>
                    <div class="espaciado">
                        <label>Fotografía de la Padre:</label>
                        <?php
                        $fotoPadre = $padre->getFoto();
                        if ($fotoPadre->getId() != null) {
                            echo imageField("imagenPadre", $fotoPadre->getId());
                        } else {
                            echo imageField("imagenPadre");
                        }
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Padre:</label>
                        <?php echo '<input name="primerApellidoPadre" id="primerApellidoPadre" type="text" value="' . $padre->getPrimerApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Padre:</label>
                        <?php echo '<input name="segundoApellidoPadre" id="segundoApellidoPadre" type="text" value="' . $padre->getSegundoApellido() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Nombres de la Padre:</label>
                        <?php echo '<input name="nombresPadre" id="nombresPadre" type="text" value="' . $padre->getNombres() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <?php echo '<input name="lugarNacimientoPadre" id="lugarNacimientoPadre" type="text" value="' . $padre->getLugarNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <?php echo '<input name="fechaNacimientoPadre" id="fechaNacimientoPadre" type="date" value="' . $padre->getFechaNacimiento() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <?php echo '<input name="edadPadre" id="edadPadre" type="number" value="' . $padre->getEdadCumplida() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <?php echo '<input name="tidPadre" id="tidPadre" type="text" value="' . $padre->getTid()->getNombre() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <?php echo '<input name="nidPadre" id="nidPadre" type="text" value="' . $padre->getNid() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <?php echo '<input name="profesionPadre" id="profesionPadre" type="text" value="' . $padre->getProfesion() . '" readonly/>' ?>
                    </div>

                    <?php
                    $empresaPadre = $padre->getEmpresa();
                    echo '<div>';
                    echo '<label>Empresa donde Trabaja:</label>';
                    echo '<input name="empresaPadre" id="empresaPadre" type="text" value="' . $empresaPadre . '" readonly/>';
                    echo '</div>';

                    if ($empresaPadre == "Independiente") {
                        echo '<div>';
                        echo '<label>Trabajo que Realiza:</label>';
                        echo '<input name="trabajoPadre" id="trabajoPadre" type="text" value="' . $padre->getTrabajo() . '" readonly/>';
                        echo '</div>';
                    } else {
                        echo '<div>';
                        echo '<label>Cargo que Desempeña:</label>';
                        echo '<input name="cargoPadre" id="cargoPadre" type="text" value="' . $padre->getCargo() . '" readonly/>';
                        echo '</div>';
                    }

                    $celularPadre = $padre->getCelular();
                    $telefonoPadre = $padre->getTelefono();
                    $extensionPadre = $padre->getExtension();
                    if ($celularPadre != null) {
                        echo '<div>';
                        echo '<label>Celular:</label>';
                        echo '<input name="celularPadre" id="celularPadre" type="text" value="' . $celularPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($telefonoPadre != null) {
                        echo '<div>';
                        echo '<label>Telefono:</label>';
                        echo '<input name="telefonoPadre" id="telefonoPadre" type="text" value="' . $telefonoPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($extensionPadre != null) {
                        echo '<div>';
                        echo '<label>Extensión:</label>';
                        echo '<input name="extensionPadre" id="extensionPadre" type="text" value="' . $extensionPadre . '" readonly/>';
                        echo '</div>';
                    }

                    echo '<div>';
                    echo '<label>Ingresos Mensuales:</label>';
                    echo '<input name="ingresosPadre" id="ingresosPadre" type="text" value="' . $padre->getIngresosMensuales() . '" readonly/>';
                    echo '</div>';
                    ?>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <?php
                    $estudiosPrimariosPadre = $padre->getEstudiosPrimarios();
                    $estudiosSecundariosPadre = $padre->getEstudiosSecundarios();
                    $estudiosUniversitariosPadre = $padre->getEstudiosUniversitarios();
                    $estudiosOtrosPadre = $padre->getEstudiosOtros();
                    if ($estudiosPrimariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Primarios:</label>';
                        echo '<input name="estudiosPrimariosPadre" id="estudiosPrimariosPadre" type="text" value="' . $estudiosPrimariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosSecundariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Secundarios:</label>';
                        echo '<input name="estudiosSecundariosPadre" id="estudiosSecundariosPadre" type="text" value="' . $estudiosSecundariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosUniversitariosPadre != null) {
                        echo '<div>';
                        echo '<label>* Estudios Universitarios:</label>';
                        echo '<input name="estudiosUniversitariosPadre" id="estudiosUniversitariosPadre" type="text" value="' . $estudiosUniversitariosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    if ($estudiosOtrosPadre != null) {
                        echo '<div>';
                        echo '<label>* Otros Estudios:</label>';
                        echo '<input name="estudiosOtrosPadre" id="estudiosOtrosPadre" type="text" value="' . $estudiosOtrosPadre . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $resideEnPadre = $padre->getResideEn();
                    $tipoResidenciaPadre = $padre->gettipoResidencia();
                    echo '<div>';
                    echo '<label>Reside en:</label>';
                    echo '<radio>';
                    if ($resideEnPadre == "C") {
                        echo '<input name="resideEnRadioPadre" id="resideEnRadioPadre" type="text" value="Casa" readonly/>';
                    } else {
                        echo '<input name="resideEnRadioPadre" id="resideEnRadioPadre" type="text" value="Apartamento" readonly/>';
                    }
                    echo '<br/><br/>';
                    if ($tipoResidenciaPadre == "P") {
                        echo '<input name="tipoResidenciaRadioPadre" id="tipoResidenciaRadioPadre" type="text" value="Propia" readonly/>';
                    } else {
                        echo '<input name="tipoResidenciaRadioPadre" id="tipoResidenciaRadioPadre" type="text" value="Arriendo" readonly/>';
                    }
                    echo '</radio></div>';
                    ?>

                    <div>
                        <label>E-mail:</label>
                        <?php echo '<input name="correoPadre" id="correoPadre" type="email" value="' . $padre->getEmail() . '" readonly/>' ?>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <?php echo '<textarea cols="40" rows="5" name="interesesPadre" id="interesesPadre" type="text" readonly>' . $padre->getIntereses() . '</textarea>' ?>
                    </div>

                    <!-- INFORMACION ADICIONAL -->
                    <hr>
                    <h4 align="center">Información Adicional</h4>

                    <?php
                    $hermanos = $formularioAdicional->getHermanos();
                    $instituciones = $formularioAdicional->getInstituciones();

                    $tipoUnion = $formularioAdicional->getTipoUnion();
                    echo '<div>';
                    echo '<label>Actualmente los Padres están:</label>';
                    echo '<input name="estadoCivil" id="estadoCivil" type="text" value="' . $tipoUnion . '" readonly/>';
                    echo '</div>';
                    if ($tipoUnion == "Separados") {
                        echo '<div>';
                        echo '<label>El Educando vive con:</label>';
                        echo '<input name="separados" id="separados" type="text" value="' . $formularioAdicional->getSeparados() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $adoptado = $formularioAdicional->getAdoptado();
                    echo '<div>';
                    echo '<label>¿El Educando aspirante es Adoptado?</label>';
                    if ($adoptado == "S") {
                        echo '<input name="adoptadoRadio" id="adoptadoRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="adoptadoRadio" id="adoptadoRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($adoptado == "S") {
                        echo '<div>';
                        echo '<label>¿Qué edad tenía en el momento de la adopción?</label>';
                        echo '<input name="edadAdoptado" id="edadAdoptado" type="number" value="' . $formularioAdicional->getEdadAdoptado() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    if ($hermanos != null) {
                        echo '<br/>'
                        . '<table id="hermanosTabla" border="0" class="asociados">'
                        . '<tr>'
                        . '<th>Nombres de los hermanos(as)</th>'
                        . '<th>Apellidos de los hermanos(as)</th>'
                        . '<th>Edad</th>'
                        . '<th>Colegio o Institución donde estudia</th>'
                        . '<th>Grado</th>'
                        . '</tr>';
                        foreach ($hermanos as $hermano) {
                            echo '<tr>'
                            . '<td><input id=nombresHermano type=text name=hermanos[] value="' . $hermano->getNombres() . '" readonly/></td>'
                            . '<td><input id=apellidosHermano type=text name=hermanos[] value="' . $hermano->getApellidos() . '" readonly/></td>'
                            . '<td><input id=edadHermano type=number name=hermanos[] value="' . $hermano->getEdad() . '" readonly/></td>'
                            . '<td><input id=colegioHermano type=text name=hermanos[] value="' . $hermano->getColegio() . '" readonly/></td>'
                            . '<td><input id=gradoHermano type=text name=hermanos[] value="' . $hermano->getGrado() . '" readonly/></td>'
                            . '</tr>';
                        }
                        echo '</table><br/>';
                    }
                    ?>

                    <div>
                        <label>En caso de emergencia, señale dirección y número telefónico donde se pueden localizar papá y/o mamá de manera inmediata:</label>
                        <?php echo '<textarea cols="40" rows="5" name="direccionTelefonoEmergencia" id="direccionTelefonoEmergencia" type="text" readonly>' . $formularioAdicional->getDireccionTelefonoEmergencia() . '</textarea>' ?>
                    </div>
                    <div>
                        <label>Nombre, Dirección y Teléfono de Familiar(es) cercano(s) que puedan acudir al Liceo de manera inmediata, en caso de emergencia:</label>
                        <?php echo '<textarea cols="40" rows="7" name="direccionTelefonoFamiliar" id="direccionTelefonoFamiliar" type="text" readonly>' . $formularioAdicional->getDireccionTelefonoFamiliar() . '</textarea>' ?>
                    </div>

                    <div>
                        <label>¿De manera directa, quién responderá con los compromisos formativos, académicos y económicos ante el Liceo Campo David?</label>
                        <?php echo '<input name="personaResponde" id="personaResponde" type="text" value="' . $formularioAdicional->getPersonaResponde() . '" readonly/>' ?>
                    </div>

                    <?php
                    $padresFirman = $formularioAdicional->getPadresFirman();
                    echo '<div>';
                    echo '<label>¿Ambos padres firmarán la matrícula y el contrato de Cooperación Educativa?</label>';
                    if ($padresFirman == "S") {
                        echo '<input name="padresFirmanRadio" id="padresFirmanRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="padresFirmanRadio" id="padresFirmanRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($padresFirman == "N") {
                        echo '<div>';
                        echo '<label>¿Quién?</label>';
                        echo '<input name="nombreFirma" id="nombreFirma" type="text" value="' . $formularioAdicional->getNombreFirma() . '" readonly/>';
                        echo '</div>';

                        $autorizaInformacion = $formularioAdicional->getAutorizaInformacion();
                        echo '<div>';
                        echo '<label>¿Autoriza dar información verbal y escrita al Padre o Madre ausente?</label>';
                        if ($autorizaInformacion == "S") {
                            echo '<input name="autorizaInformacionRadio" id="autorizaInformacionRadio" type="text" value="Sí" readonly/>';
                        } else {
                            echo '<input name="autorizaInformacionRadio" id="autorizaInformacionRadio" type="text" value="No" readonly/>';
                        }
                        echo '</div>';

                        echo '<div>';
                        echo '<label>Explique:</label>';
                        echo '<input name="autorizaExplique" id="autorizaExplique" type="text" value="' . $formularioAdicional->getAutorizaExplique() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <?php
                    $miembroReferencia = $formularioAdicional->getMiembroReferencia();
                    if ($miembroReferencia != null) {
                        echo '<div>';
                        echo '<label>¿Qué miembro de la Comunidad Liceísta puede dar referencia de la familia que solicita admisión?</label>';
                        echo '<input name="miembroReferencia" id="miembroReferencia" type="text" value="' . $miembroReferencia . '" readonly/>';
                        echo '</div>';

                        echo '<div>';
                        echo '<label>Padres del Educando:</label>';
                        echo '<textarea cols="40" rows="3" name="padresReferencia" id="padresReferencia" type="text" readonly>' . $formularioAdicional->getPadresReferencia() . '</textarea>';
                        echo '</div>';
                        echo '<div>';
                        echo '<label>Del Grado:</label>';
                        echo '<input name="gradoReferencia" id="gradoReferencia" type="text" value="' . $formularioAdicional->getGradoReferencia() . '" readonly/>';
                        echo '</div>';
                    }
                    ?>

                    <div>
                        <label>¿Qué características del Liceo Campo David le han motivado para solicitar esta admisión?</label>
                        <?php echo '<textarea cols="40" rows="7" name="caracteristicas" id="caracteristicas" type="text" readonly>' . $formularioAdicional->getCaracteristicas() . '</textarea>' ?>
                    </div>

                    <?php
                    if ($instituciones != null) {
                        echo '<br/>'
                        . '<table id="hermanosTabla" border="0" class="asociados">'
                        . '<tr>'
                        . '<th>Nombre de la Institución</th>'
                        . '<th>Grado</th>'
                        . '<th>Año en que lo cursó</th>'
                        . '<th>Teléfono</th>'
                        . '</tr>';
                        foreach ($instituciones as $institucion) {
                            echo '<tr>'
                            . '<td><input id=nombreInstitucion type=text name=instituciones[] value="' . $institucion->getNombre() . '" readonly/></td>'
                            . '<td><input id=gradoInstitucion type=text name=instituciones[] value="' . $institucion->getGrado() . '" readonly/></td>'
                            . '<td><input id=anioInstitucion type=number name=instituciones[] value="' . $institucion->getAnio() . '" readonly/></td>'
                            . '<td><input id=telefonoInstitucion type=text name=instituciones[] value="' . $institucion->getTelefono() . '" readonly/></td>'
                            . '</tr>';
                        }
                        echo '</table><br/>';
                    }
                    ?>

                    <div>
                        <label>¿En pocas palabras, cuál es la Visión Educativa ideal para su Hijo(a)?</label>
                        <?php echo '<textarea cols="40" rows="5" name="visionEducativa" id="visionEducativa" type="text" readonly>' . $formularioAdicional->getVisionEducativa() . '</textarea>' ?>
                    </div>

                    <?php
                    $escolaridadAnterior = $formularioAdicional->getEscolaridadAnterior();
                    echo '<div>';
                    echo '<label>¿Posee escolaridad anterior?</label>';
                    if ($escolaridadAnterior == "S") {
                        echo '<input name="escolaridadRadio" id="escolaridadRadio" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="escolaridadRadio" id="escolaridadRadio" type="text" value="No" readonly/>';
                    }
                    echo '</div>';
                    if ($escolaridadAnterior == "S") {
                        $pazSalvo = $formularioAdicional->getPazSalvo();
                        if ($pazSalvo != null) {
                            echo '<div>';
                            $idPazSalvo = $pazSalvo->getId();
                            echo '<input name="pazSalvo" id="pazSalvoMostrar" type="button" value="Descargar Paz y Salvo" class="Button2" '
                            . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idPazSalvo" . '\'"/>';
                            echo '</div>';
                        }

                        $ultimoBoletin = $formularioAdicional->getUltimoBoletin();
                        if ($ultimoBoletin != null) {
                            echo '<div>';
                            $idUltimoBoletin = $ultimoBoletin->getId();
                            echo '<input name="ultimoBoletin" id="ultimoBoletinMostrar" type="button" value="Descargar Último Boletin" class="Button2" '
                            . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idUltimoBoletin" . '\'"/>';
                            echo '</div>';
                        }
                    }
                    ?>

                    <?php
                    $certificadoIngresos = $formularioAdicional->getCertificadoIngresos();
                    if ($certificadoIngresos != null) {
                        echo '<div>';
                        $idCertificadoIngresos = $certificadoIngresos->getId();
                        echo '<input name="certificadoIngresos" id="certificadoIngresosMostrar" type="button" value="Descargar Certificado de Ingresos" class="Button2" '
                        . 'onclick="window.location = \'' . "descargarArchivo.php?id=$idCertificadoIngresos" . '\'"/>';
                        echo '</div>';
                    }
                    ?>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <style type="text/css" media="print">
                        @page { size: letter; }
                    </style>
                    <div align="center">
                        <input name="imprimir" id="submit" type="submit" value="Imprimir" onclick="window.print();" class="Button2"/>
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinConsultar.php'" class="Button2">
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin no encontrado</h4>
                    <?php
                    echo "<p>No se han encontrado coincidencias para el Pin <b>$pin</b> ingresado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinConsultar.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no válido</h4>
                <?php
                echo "<p>Se ha ingresado un Pin no válido.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinConsultar.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
    </body>
</html>