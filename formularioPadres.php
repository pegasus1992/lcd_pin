<?php
include_once './utils/funciones.php';
include_once './utils/constantes.php';
include_once './controller/formularioPadresController.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>' . PROJECT_NAME . ' - Diligenciar Formulario - Información de los Padres</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        $pin = $_GET['pin'];
        $utilizado = Pin::existePinUtilizado($pin);
        if ($utilizado) {
            $cantidad = FormularioPadres::obtenerConteoFormulariosPadres($pin);
            if ($cantidad == 0) {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Diligenciar Formulario - Información de los Padres</h4>
                    <div>
                        <label>Pin:</label>
                        <?php echo '<input name="pin" id="pin" type="text" value="' . $pin . '" readonly/>' ?>
                    </div>

                    <!----------------- DATOS DE LA MADRE ----------------->
                    <h6 align="center">Datos de la Madre</h6>
                    <div>
                        <label>Fotografía de la Madre:</label>
                        <input placeholder="Ingrese la foto" name="fotoMadre" id="fotoMadre" type="file" accept="image/*" autocomplete="off" required/>
                        <br/>
                        <br/>
                        <?php
                        echo imageField("imagenMadre");
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Madre:</label>
                        <input placeholder="Ingrese el primer apellido de la Madre" name="primerApellidoMadre" id="primerApellidoMadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Madre:</label>
                        <input placeholder="Ingrese el segundo apellido de la Madre" name="segundoApellidoMadre" id="segundoApellidoMadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Nombres de la Madre:</label>
                        <input placeholder="Ingrese los nombres de la Madre" name="nombresMadre" id="nombresMadre" type="text" autocomplete="on" maxlength="50" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <input placeholder="Ingrese el lugar de nacimiento" name="lugarNacimientoMadre" id="lugarNacimientoMadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <input type="date" id="fechaNacimientoMadre" name="fechaNacimientoMadre" class="fecha" placeholder="yyyy-mm-dd" onblur="ageMadre(this.value)" required/>
                    </div>
                    <div id="fechaNacimientoMadreError" style="color: red; align-content: center"></div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <input placeholder="Ingrese la edad" name="edadMadre" id="edadMadre" type="number" autocomplete="on" readonly/>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <select name="tidMadre" id="tidMadre" required>
                            <option></option>
                            <?php
                            $documentos = TipoDocumento::traerDocumentos();
                            foreach ($documentos as $documento) {
                                if (trim($documento->getDocumento()) != 'TI') {
                                    echo '<option value="' . $documento->getDocumento() . '">' . $documento->getNombre() . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <input placeholder="Ingrese el número de identificación" name="nidMadre" id="nidMadre" type="text" autocomplete="on" maxlength="20" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <input placeholder="Ingrese la profesión" name="profesionMadre" id="profesionMadre" type="text" autocomplete="on" maxlength="30" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>¿Es Independiente?</label>
                        <radio>
                            <input type="radio" name="independienteRadioMadre" id="independienteSiMadre" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="independienteRadioMadre" id="independienteNoMadre" value="N" required/>No
                        </radio>
                    </div>
                    <div>
                        <label>Empresa donde Trabaja:</label>
                        <input placeholder="Ingrese la empresa" name="empresaMadre" id="empresaMadre" type="text" autocomplete="on" maxlength="50" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Celular:</label>
                        <input placeholder="Ingrese el celular" name="celularMadre" id="celularMadre" type="number" autocomplete="on" min="1000000000" max="99999999999999999999" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Telefono:</label>
                        <input placeholder="Ingrese el teléfono" name="telefonoMadre" id="telefonoMadre" type="number" autocomplete="on" min="1000000" max="999999999999999" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Extensión:</label>
                        <input placeholder="Si tiene extensión, ingrésela" name="extensionMadre" id="extensionMadre" type="number" autocomplete="on" min="0" max="99999" disabled onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Cargo que Desempeña:</label>
                        <input placeholder="Ingrese el cargo" name="cargoMadre" id="cargoMadre" type="text" autocomplete="on" maxlength="30" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Ingresos Mensuales:</label>
                        <input placeholder="Ingrese los ingresos" name="ingresosMadre" id="ingresosMadre" type="number" autocomplete="on" min="0" max="99999999999" disabled required/>
                    </div>
                    <div>
                        <label>Trabajo que Realiza:</label>
                        <input placeholder="Ingrese el trabajo" name="trabajoMadre" id="trabajoMadre" type="text" autocomplete="on" maxlength="50" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <div>
                        <label>* Estudios Primarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosPrimariosMadre" id="estudiosPrimariosMadre" type="text" autocomplete="on" maxlength="50" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Estudios Secundarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosSecundariosMadre" id="estudiosSecundariosMadre" type="text" autocomplete="on" maxlength="50" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Estudios Universitarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosUniversitariosMadre" id="estudiosUniversitariosMadre" type="text" autocomplete="on" maxlength="75" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Otros Estudios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosOtrosMadre" id="estudiosOtrosMadre" type="text" autocomplete="on" maxlength="100" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Reside en:</label>
                        <radio>
                            <input type="radio" name="resideEnRadioMadre" id="casaMadre" value="C" required/>Casa
                            <br/>
                            <input type="radio" name="resideEnRadioMadre" id="apartamentoMadre" value="A" required/>Apartamento
                            <br/>
                            <br/>
                            <input type="radio" name="tipoResidenciaRadioMadre" id="propiaMadre" value="P" required/>Propia
                            <br/>
                            <input type="radio" name="tipoResidenciaRadioMadre" id="arriendoMadre" value="A" required/>Arriendo
                        </radio>
                    </div>
                    <div>
                        <label>E-mail:</label>
                        <input placeholder="Ingrese el correo" type="email" name="correoMadre" id="correoMadre" autocomplete="on" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <textarea cols="40" rows="5" placeholder="Ingrese los intereses personales" name="interesesMadre" id="interesesMadre" type="text" maxlength="300" autocomplete="on" required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                    </div>

                    <!----------------- DATOS DEL PADRE ----------------->
                    <h6 align="center">Datos del Padre</h6>
                    <div>
                        <label>Fotografía del Padre:</label>
                        <input placeholder="Ingrese la foto" name="fotoPadre" id="fotoPadre" type="file" accept="image/*" autocomplete="off" required/>
                        <br/>
                        <br/>
                        <?php
                        echo imageField("imagenPadre");
                        ?>
                    </div>
                    <div>
                        <label>Primer Apellido de la Padre:</label>
                        <input placeholder="Ingrese el primer apellido del Padre" name="primerApellidoPadre" id="primerApellidoPadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Segundo Apellido de la Padre:</label>
                        <input placeholder="Ingrese el segundo apellido del Padre" name="segundoApellidoPadre" id="segundoApellidoPadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Nombres de la Padre:</label>
                        <input placeholder="Ingrese los nombres del Padre" name="nombresPadre" id="nombresPadre" type="text" autocomplete="on" maxlength="50" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Lugar de Nacimiento:</label>
                        <input placeholder="Ingrese el lugar de nacimiento" name="lugarNacimientoPadre" id="lugarNacimientoPadre" type="text" autocomplete="on" maxlength="25" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Fecha de Nacimiento:</label>
                        <input type="date" id="fechaNacimientoPadre" name="fechaNacimientoPadre" class="fecha" placeholder="yyyy-mm-dd" onblur="agePadre(this.value)" required/>
                    </div>
                    <div id="fechaNacimientoPadreError" style="color: red; align-content: center"></div>
                    <div>
                        <label>Edad cumplida (años):</label>
                        <input placeholder="Ingrese la edad" name="edadPadre" id="edadPadre" type="number" autocomplete="on" readonly/>
                    </div>
                    <div>
                        <label>Tipo de Documento:</label>
                        <select name="tidPadre" id="tidPadre" required>
                            <option></option>
                            <?php
                            foreach ($documentos as $documento) {
                                if (trim($documento->getDocumento()) != 'TI') {
                                    echo '<option value="' . $documento->getDocumento() . '">' . $documento->getNombre() . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label>Número de Identificación:</label>
                        <input placeholder="Ingrese el número de identificación" name="nidPadre" id="nidPadre" type="text" autocomplete="on" maxlength="20" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Profesión:</label>
                        <input placeholder="Ingrese la profesión" name="profesionPadre" id="profesionPadre" type="text" autocomplete="on" maxlength="30" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>¿Es Independiente?</label>
                        <radio>
                            <input type="radio" name="independienteRadioPadre" id="independienteSiPadre" value="S" required/>Sí
                            <br/>
                            <input type="radio" name="independienteRadioPadre" id="independienteNoPadre" value="N" required/>No
                        </radio>
                    </div>
                    <div>
                        <label>Empresa donde Trabaja:</label>
                        <input placeholder="Ingrese la empresa" name="empresaPadre" id="empresaPadre" type="text" autocomplete="on" maxlength="50" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Celular:</label>
                        <input placeholder="Ingrese el celular" name="celularPadre" id="celularPadre" type="number" autocomplete="on" min="1000000000" max="99999999999999999999" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Telefono:</label>
                        <input placeholder="Ingrese el teléfono" name="telefonoPadre" id="telefonoPadre" type="number" autocomplete="on" min="1000000" max="999999999999999" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Extensión:</label>
                        <input placeholder="Si tiene extensión, ingrésela" name="extensionPadre" id="extensionPadre" type="number" autocomplete="on" min="0" max="99999" disabled onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Cargo que Desempeña:</label>
                        <input placeholder="Ingrese el cargo" name="cargoPadre" id="cargoPadre" type="text" autocomplete="on" maxlength="30" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Ingresos Mensuales:</label>
                        <input placeholder="Ingrese los ingresos" name="ingresosPadre" id="ingresosPadre" type="number" autocomplete="on" min="0" max="99999999999" disabled required/>
                    </div>
                    <div>
                        <label>Trabajo que Realiza:</label>
                        <input placeholder="Ingrese el trabajo" name="trabajoPadre" id="trabajoPadre" type="text" autocomplete="on" maxlength="50" disabled required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Institución donde realizó sus:</label>
                    </div>
                    <div>
                        <label>* Estudios Primarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosPrimariosPadre" id="estudiosPrimariosPadre" type="text" autocomplete="on" maxlength="50" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Estudios Secundarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosSecundariosPadre" id="estudiosSecundariosPadre" type="text" autocomplete="on" maxlength="50" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Estudios Universitarios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosUniversitariosPadre" id="estudiosUniversitariosPadre" type="text" autocomplete="on" maxlength="75" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>* Otros Estudios:</label>
                        <input placeholder="Institución o Vacío" name="estudiosOtrosPadre" id="estudiosOtrosPadre" type="text" autocomplete="on" maxlength="100" onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Reside en:</label>
                        <radio>
                            <input type="radio" name="resideEnRadioPadre" id="casaPadre" value="C" required/>Casa
                            <br/>
                            <input type="radio" name="resideEnRadioPadre" id="apartamentoPadre" value="A" required/>Apartamento
                            <br/>
                            <br/>
                            <input type="radio" name="tipoResidenciaRadioPadre" id="propiaPadre" value="P" required/>Propia
                            <br/>
                            <input type="radio" name="tipoResidenciaRadioPadre" id="arriendoPadre" value="A" required/>Arriendo
                        </radio>
                    </div>
                    <div>
                        <label>E-mail:</label>
                        <input placeholder="Ingrese el correo" type="email" name="correoPadre" id="correoPadre" autocomplete="on" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" required onkeyup="this.value = this.value.toUpperCase();"/>
                    </div>
                    <div>
                        <label>Intereses Personales diferentes al Trabajo:</label>
                        <textarea cols="40" rows="5" placeholder="Ingrese los intereses personales" name="interesesPadre" id="interesesPadre" type="text" maxlength="300" autocomplete="on" required onkeyup="this.value = this.value.toUpperCase();"></textarea>
                    </div>

                    <!----------------- PANEL DE BOTONES ----------------->
                    <div align="center">
                        <input name="registrar" id="submit" type="submit" value="Registrar e Ir a Información Adicional" class="Button2"/>
                        <!--input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2"-->
                    </div>
                </form>
                <?php
            } else {
                ?>
                <form action="" method="post" enctype="multipart/form-data" class="registro">
                    <h4 align="center">Pin utilizado</h4>
                    <?php
                    echo "<p>El Pin <b>$pin</b> ya ha sido utilizado.</p>";
                    ?>
                    <div align="center">
                        <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                    </div>
                </form>
                <?php
            }
        } else {
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Pin no encontrado o no utilizado</h4>
                <?php
                echo "<p>El Pin <b>$pin</b> no existe o aún no ha sido utilizado.</p>";
                ?>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location = 'ingresarPinRegistrar.php'" class="Button2">
                </div>
            </form>
            <?php
        }
        ?>
        <!-- CARGAR IMAGEN -->
        <script src="view/js/cargadorFotosPadres.js" type="text/javascript"></script>
        <!-- FORMULARIO -->
        <script src="view/js/formularioPadres.js" type="text/javascript"></script>
    </body>
</html>